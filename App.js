import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import AppContainer from './src/app/router';
import configureStore from './src/app/store'

const { store, persistor } = configureStore();
console.disableYellowBox = true;
export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                        <AppContainer />
                </PersistGate>
            </Provider>
        )
    }
}
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

const MyHeadlessTask = async () => {
    console.log('Receiving Location service!')
};
// const FirebaseTask = async (message) => {
//     console.log('FirebaseTask')
//     const notification = new firebase.notifications.Notification()
//     .setNotificationId('1')
//     .setTitle(message.data.title)
//     .setBody(message.data.body);
//     if (Platform.OS === 'android') {
//         notification.android.setChannelId('visual_sat_channel_id');
//         notification.android.setSmallIcon('ic_launcher_round')
//         notification.android.setBadgeIconType(1)
//       }
//       firebase.notifications().displayNotification(notification);
// };

AppRegistry.registerHeadlessTask('Location', () => MyHeadlessTask)
AppRegistry.registerComponent(appName, () => App);
//AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => FirebaseTask)
let BASE_URL = "https://www.visualsatpe.com:9595/api/" //prod
//let BASE_URL = "https://192.168.1.62:95/api/" //local

export const sw_base = (url, params, methodType, token = null) => {

    console.log('url: ' + url)
    console.log('token: ' + token)

    method = methodType == 0 ? "POST" : (methodType == 1 ? "GET" : (methodType == 2 ? "PUT" : "DELETE"))
    requestInfo = { method: method, headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' } }
    if (methodType != 1) {
        const json = JSON.stringify(params)

        requestInfo = { ...requestInfo, body: json }
    }
    if (token != null) {
        requestInfo = { ...requestInfo, headers: { ...requestInfo.headers, 'Authorization': token } }
    }
    console.log('requestInfo',requestInfo)
    
    return fetch(url, requestInfo).then((res) => {
        if (res.ok || res.status == 400 || res.status == 401) {
            try {
                return res.json()
            } catch{
                throw new Error('*Hubo un error en el servidor')
            }
        } else {
            throw new Error('*Hubo un error en el servidor')
        }
    }).catch((e) => {

        let message = e.message
        console.log('error',message)
        if (message == null) {
            message = "Hubo un error en la conexión"
        } else {
            if (message.charAt(0) == "*") {
                message = message.slice(1)
            } else {
                message = "Hubo un error en la conexión"
            }
        }
        throw new Error(message)
    });
}

export const sw_login = (params) => {
    return sw_base(`${BASE_URL}login`, params, 0).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_sendLocation = (params, token) => {
    return sw_base(`${BASE_URL}sendData`, params, 0, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_myServices = (token, status) => {
    return sw_base(`${BASE_URL}myservice?status=${status}`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_startService = (params, token) => {
    return sw_base(`${BASE_URL}startservice`, params, 2, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_activityList = (params, token) => {
    return sw_base(`${BASE_URL}activitylist?RequestId=${params.RequestId}`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_checkActivity = (params, token) => {
    return sw_base(`${BASE_URL}activitycheck`, params, 2, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_sendToken = (params, token) => {
    return sw_base(`${BASE_URL}tokenpush`, params, 0, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_cancelTravel = (params, token) => {
    return sw_base(`${BASE_URL}cancelservice`, params, 3, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_cancelReasons = (params, token) => {
    return sw_base(`${BASE_URL}cancellationreasons`, params, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_notificationList = (params, token) => {
    return sw_base(`${BASE_URL}driverevent`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}


export const sw_qualityNotificationList = (params, token) => {
    return sw_base(`${BASE_URL}qualitylistnotification`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}

export const sw_certifierNotificationList = (params, token) => {
    return sw_base(`${BASE_URL}certifierlistnotification`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}

export const sw_platesList = (params, token) => {
    return sw_base(`${BASE_URL}vehiclereception`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_lotsList = (params, token) => {
    return sw_base(`${BASE_URL}lotereception?Plate=${params.Plate}`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
//BEGIN CERTIFIER
export const sw_sackLots = (token) => {
    return sw_base(`${BASE_URL}lote`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_plants = (token) => {
    return sw_base(`${BASE_URL}plant`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_saveLot = (token, params) => {
    return sw_base(`${BASE_URL}lote`, params, 0, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_updateLot = (token, params) => {
    return sw_base(`${BASE_URL}lote`, params, 2, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_deleteLot = (token, params) => {
    return sw_base(`${BASE_URL}lote`, params, 3, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_unitInWay = (token) => {
    return sw_base(`${BASE_URL}unitinway`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_saveSack = (token, params) => {
    return sw_base(`${BASE_URL}shipment`, params, 0, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_getResampling = (token) => {
    return sw_base(`${BASE_URL}resamplinglist`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_saveResampling = (token, params) => {
    return sw_base(`${BASE_URL}resampling`, params, 0, token).then((r) => { return r }).catch((e) => { throw e })
}
//END CERTIFIER
export const sw_nichosList = (params, token) => {
    return sw_base(`${BASE_URL}lockeravailable?Sector=${params.Sector}&Zone=${params.Zone}`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_differenceSackList = (params, token) => {
    return sw_base(`${BASE_URL}differencessacklote`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_saveReception = (params, token) => {
    return sw_base(`${BASE_URL}savelotereception`, params, 0, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_listNotificationAlmacen = (params, token) => {
    return sw_base(`${BASE_URL}listnotification`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}

export const sw_listBaseTravel = (params, token) => {
    return sw_base(`${BASE_URL}travellist`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}


export const sw_listFollow = (params, token) => {
    return sw_base(`${BASE_URL}qualitylist`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_saveFollow = (params, token) => {
    return sw_base(`${BASE_URL}savelotequality`, params, 0, token).then((r) => { return r }).catch((e) => { throw e })
}

export const sw_listContracts = (params, token) => {
    return sw_base(`${BASE_URL}contract`, params, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_saveShipmentLote = (params , token) =>{
    return sw_base(`${BASE_URL}shipmentlote`, params, 0, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_listContractLote = (params , token) =>{
    return sw_base(`${BASE_URL}contractlote?ContractId=${params.ContractId}`, params, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_recoveryPassword = (params) => {
    return sw_base(`${BASE_URL}forgotpassword`, params, 0).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_logOut = (params, token) => {
    return sw_base(`${BASE_URL}logout`, params, 0, token).then((r) => { return r }).catch((e) => { throw e })


}

export const sw_checkNotification = (params, token) => {
    return sw_base(`${BASE_URL}checknotification`, params, 2, token).then((r) => { return r }).catch((e) => { throw e })
}

//APIS PARA NUEVO PERFIL

export const sw_bases = (params, token) => {
    return sw_base(`${BASE_URL}bases`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}

export const sw_typeDoc = (params, token) => {
    return sw_base(`${BASE_URL}typedoc`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_product = (params, token) => {
    return sw_base(`${BASE_URL}product`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_serviceType = (params, token) => {
    return sw_base(`${BASE_URL}servicetype`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_provider = (params, token) => {
    return sw_base(`${BASE_URL}providerapp`, null, 1, token).then((r) => { return r }).catch((e) => { throw e })
}
export const sw_RegisterRequest = (params, token)=>{
    return sw_base(`${BASE_URL}serviceregister`, params, 0, token).then((r) => { return r }).catch((e) => { throw e })
}

export const sw_ListBaseTravelTransport = (params, token)=>{
    return sw_base(`${BASE_URL}travellistapp`, params, 1, token).then((r) => { return r }).catch((e) => { throw e })
}

export const sw_cancelRequest  = (params, token)=>{
    return sw_base(`${BASE_URL}cancelservice`, params, 3, token).then((r) => { return r }).catch((e) => { throw e })
}

export const sw_updateRegisterRequest = (params, token)=>{
    return sw_base(`${BASE_URL}serviceregister`, params, 2, token).then((r) => { return r }).catch((e) => { throw e })
}
import React from 'react'
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { createStackNavigator } from 'react-navigation-stack'

import {
    SplashScreen, LogInScreen, MapScreen, TravelsScreen, NotificationsScreen, DetailTravelScreen, SummaryTravelScreen,
    JourneyScreen, CheckListScreen, CancelTravelScreen, RecoveryScreen, BaseTravelsScreen, ReceptionScreen, TrackingScreen,
    CertifierMenuScreen, DetailReceptionScreen, SackLotListScreen, SackLotInfoScreen, ListContractsScreen, ContractScreen,
    TemperatureScreen, SackScreen, ResamplingLotListScreen, ResamplingLotScreen, RequestScreen
} from '../ui/screen'
import { spaceBlack } from '../resources/color'
import TabBarItem from '../ui/component/tabBarItem'








const LoginStack = createStackNavigator({
    Login: LogInScreen,
    Recovery: RecoveryScreen
}, {
    initialRouteName: 'Login',
    headerMode: 'none'
})

const TravelStack = createStackNavigator({
    Travels: TravelsScreen,
    DetailTravel: DetailTravelScreen,
    CancelTravel: CancelTravelScreen,
    SummaryTravel: SummaryTravelScreen,
    Journey: JourneyScreen,
    CheckList: CheckListScreen
}, {
    initialRouteName: 'Travels',
    headerMode: 'none'
})
TravelStack.navigationOptions = ({ navigation }) => {
    return { tabBarVisible: navigation.state.index == 0 }
}
const DriverBottomTab = createBottomTabNavigator({
    Map: {
        screen: MapScreen,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={0} />
            }
        }
    },
    Travels: {
        screen: TravelStack,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={1} />
            }
        }
    },
    Notifications: {
        screen: NotificationsScreen,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={2} />
            }
        }
    }
}, {
    initialRouteName: 'Map',
    headerMode: 'none',
    tabBarOptions: {
        inactiveBackgroundColor: spaceBlack,
        activeBackgroundColor: spaceBlack,
        safeAreaInset: { bottom: 'always', top: 'never' },
        showLabel: false,
        style: {
            backgroundColor: spaceBlack,
        },
    },
    lazy: true
})

const StorageStack = createStackNavigator({
    Reception: ReceptionScreen,
    DetailReception: DetailReceptionScreen,
}, {
    initialRouteName: 'Reception',
    headerMode: 'none'
})
StorageStack.navigationOptions = ({ navigation }) => {
    return { tabBarVisible: navigation.state.index == 0 }
}
const StorageBottomTab = createBottomTabNavigator({
    BaseTravels: {
        screen: BaseTravelsScreen,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={1} />
            }
        }
    },
    Storage: {
        screen: StorageStack,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={3} />
            }
        }
    },
    Notifications: {
        screen: NotificationsScreen,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={2} />
            }
        }
    }
}, {
    initialRouteName: 'BaseTravels',
    headerMode: 'none',
    tabBarOptions: {
        inactiveBackgroundColor: spaceBlack,
        activeBackgroundColor: spaceBlack,
        safeAreaInset: { bottom: 'always', top: 'never' },
        showLabel: false,
        style: {
            backgroundColor: spaceBlack,
        },
    },
    lazy: true
})

const QualityStack = createStackNavigator({
    Tracking: TrackingScreen,
    Temperature: TemperatureScreen
}, {
    initialRouteName: 'Tracking',
    headerMode: 'none'
})
QualityStack.navigationOptions = ({ navigation }) => {
    return { tabBarVisible: navigation.state.index == 0 }
}
const QualityBottomTab = createBottomTabNavigator({
    Quality: {
        screen: QualityStack,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={3} />
            }
        }
    },
    Notifications: {
        screen: NotificationsScreen,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={2} />
            }
        }
    }
}, {
    initialRouteName: 'Quality',
    headerMode: 'none',
    tabBarOptions: {
        inactiveBackgroundColor: spaceBlack,
        activeBackgroundColor: spaceBlack,
        safeAreaInset: { bottom: 'always', top: 'never' },
        showLabel: false,
        style: {
            backgroundColor: spaceBlack,
        },
    },
    lazy: true
})

const CertifierStack = createStackNavigator({
    Menu: CertifierMenuScreen,
    SackLotList: SackLotListScreen,
    SackLotInfo: SackLotInfoScreen,
    Sack: SackScreen,
    ResamplingLotList: ResamplingLotListScreen,
    ResamplingLot: ResamplingLotScreen,
    ListContracts: ListContractsScreen,
    Contract: ContractScreen
}, {
    initialRouteName: 'Menu',
    headerMode: 'none'
})
CertifierStack.navigationOptions = ({ navigation }) => {
    return { tabBarVisible: navigation.state.index == 0 }
}
const CertifierBottomTab = createBottomTabNavigator({
    BaseTravels: {
        screen: BaseTravelsScreen,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={1} />
            }
        }
    },
    Certifier: {
        screen: CertifierStack,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={3} />
            }
        }
    },
    Notifications: {
        screen: NotificationsScreen,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={2} />
            }
        }
    }
}, {
    initialRouteName: 'BaseTravels',
    headerMode: 'none',
    tabBarOptions: {
        inactiveBackgroundColor: spaceBlack,
        activeBackgroundColor: spaceBlack,
        safeAreaInset: { bottom: 'always', top: 'never' },
        showLabel: false,
        style: {
            backgroundColor: spaceBlack,
        },
    },
    lazy: true
})

//Nuevo Perfil Transporte

//Pantalla Solicitar Servicio
const RequestStack = createStackNavigator({
    Request: RequestScreen,
}, {
    initialRouteName: 'Request',
    headerMode: 'none'
})


const TransportBottomTab = createBottomTabNavigator({
    Request: {
        screen: RequestStack,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={3} />
            }
        }
    },
    
    BaseTravels: {
        screen: BaseTravelsScreen,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={1} />
            }
        }
    },
    Notifications: {
        screen: NotificationsScreen,
        navigationOptions: {
            tabBarIcon: props => {
                return <TabBarItem focused={props.focused} type={2} />
            }
        }
    }
}, {
    initialRouteName: 'Request',
    headerMode: 'none',
    tabBarOptions: {
        inactiveBackgroundColor: spaceBlack,
        activeBackgroundColor: spaceBlack,
        safeAreaInset: { bottom: 'always', top: 'never' },
        showLabel: false,
        style: {
            backgroundColor: spaceBlack,
        },
    },
    lazy: true
})

const mainNav = createSwitchNavigator({
    Splash: SplashScreen,
    LogIn: LoginStack,
    Driver: DriverBottomTab,
    Storage: StorageBottomTab,
    Quality: QualityBottomTab,
    Certifier: CertifierBottomTab,
    Transport: TransportBottomTab
}, {
    headerMode: 'none'
})

const AppContainer = createAppContainer(mainNav)
export default AppContainer;
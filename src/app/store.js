import { createStore, combineReducers, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import ReduxThunk from 'redux-thunk';
import AsyncStorage from '@react-native-community/async-storage'

import logIn from './thunk/auth/reducer'
import travel from './thunk/travel/reducer'
import notifications from './thunk/notifcications/reducer'
import reception from './thunk/reception/reducer'
import certifier from './thunk/certifier/reducer'
import baseTravel from './thunk/baseTravel/reducer'
import seguimiento from './thunk/seguimiento/reducer'
import contract from './thunk/contract/reducer'
import request from './thunk/request/reducer'


const reducers = combineReducers({ logIn, travel, notifications, reception, certifier, baseTravel, seguimiento,contract,request })

const persistConfig = {
    key: 'root',
    storage: AsyncStorage
}

const persistedReducer = persistReducer(persistConfig, reducers)

export default () => {
    let store = createStore(persistedReducer, applyMiddleware(ReduxThunk))
    let persistor = persistStore(store)
    return {store, persistor}
}
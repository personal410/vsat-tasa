import { sw_login, sw_sendToken, sw_recoveryPassword, sw_logOut } from '../../api'
import { SAVE_USER, CLEAR_USER, SET_CURRENT_PAGE, SET_LOCATION_USER } from './types'
import { DRIVER_ROLE_ID, GUARD_ROLE_ID, STORAGE_ROLE_ID, QUALITY_ROLE_ID, CERTIFIER_ROLE_ID, TRANSPORT_ROLE_ID } from '../../../resources/consts'
import { showAlert } from '../../util'

export const logIn = (params) => {
    return (dispatch) => {
        const { Login, Password, callback } = params
        sw_login({ Login, Password }).then((r) => {
            if (r.Success == null) {
                const { RoleId } = r
                if (RoleId == DRIVER_ROLE_ID || RoleId == GUARD_ROLE_ID || RoleId == STORAGE_ROLE_ID || RoleId == QUALITY_ROLE_ID || RoleId == CERTIFIER_ROLE_ID || RoleId == TRANSPORT_ROLE_ID) {
                    dispatch({
                        type: SAVE_USER,
                        payload: r
                    })
                    callback(null)
                } else {
                    callback("Perfil no permitido")
                }
            } else {
                callback(r.Mesage)
            }
        }).catch((e) => {
            callback(e.message)
        })
    }
}
export const sendToken = (params) => {
    return dispatch => {
        const { fcmToken, token, callback } = params
        sw_sendToken({ token: fcmToken }, token).then(r => {
            if (r.Success == 1) {
                callback(null)
            } else {
                callback("Hubo un error registrar el token de las notificaciones")
            }
        }).catch(e => {
            callback(e.message)
        })
    }
}
export const setCurrentPage = (params) => {
    return dispatch => {
        dispatch({
            type: SET_CURRENT_PAGE,
            payload: params
        })
    }
}
export const logOut = (info) => {
    const { token } = info
    return dispatch => {
        sw_logOut({}, token).then(r => {
            dispatch({ type: CLEAR_USER })
        }).catch(e => {
        })
    }
}
export const setLocationUser = (info) => {
    return dispatch => {
        dispatch({
            type: SET_LOCATION_USER,
            payload: info
        })
    }
}
export const recoveryPassword = (info) => {
    const { Login, callback } = info
    return dispatch => {
        sw_recoveryPassword({ Login }).then(r => {
            if (r.Success == 1) {
                showAlert({ msg: r.Message, type: 2 })
                callback();
            } else {
                showAlert({ msg: r.Message, type: 2 })
            }
        }).catch(e => {})
    }
}
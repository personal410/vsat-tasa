import { SAVE_USER, CLEAR_USER, SET_CURRENT_PAGE,SET_LOCATION_USER } from './types'

const initialState = {
    user: null,
    currentPage: '',
    latitude: 0.0,
    longitude: 0.0
}

export default (state = initialState, action) => {
    switch(action.type){
        case SAVE_USER:
            return {...state, user: action.payload}
        case SET_CURRENT_PAGE:
            return {...state, currentPage: action.payload}
        case CLEAR_USER:
            return { initialState }
        case SET_LOCATION_USER:
            const { latitude, longitude } = action.payload
            return{ ...state, latitude, longitude }
        default:
            return state
    }
}
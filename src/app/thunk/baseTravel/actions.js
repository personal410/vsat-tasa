
import {
    WAITING_BASE_TRAVEL,
    GOT_BASE_TRAVEL
} from './types'

import { TRANSPORT_ROLE_ID} from '../../../resources/consts'

import { sw_listBaseTravel, sw_ListBaseTravelTransport } from '../../api'

export const listBaseTravel = (info) => {
    const { token,callback, RoleId } = info

    return (dispatch) => {
        dispatch({
            type: WAITING_BASE_TRAVEL
        })

        switch (RoleId) {
            case TRANSPORT_ROLE_ID:

                sw_ListBaseTravelTransport({}, token).then(r => {
                    if (r.Success == 1) {
        
                        dispatch({
                            type: GOT_BASE_TRAVEL,
                            payload: r.Data
                        })
                        callback();
                    }
                }).catch(e => {
                    dispatch({
                        type: ERROR_GETTING_NOTIFICATIONS,
                    })
                })

                break;
            default:
                sw_listBaseTravel({}, token).then(r => {

                    console.log('response r', r)
                    if (r.Success == 1) {
        
                        dispatch({
                            type: GOT_BASE_TRAVEL,
                            payload: r.Data
                        })
                        callback();
                    }
        
        
        
                }).catch(e => {
                    console.log('e', e.getMessage)
                })

                
        }

    }
}
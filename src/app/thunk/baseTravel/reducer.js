import { 
    WAITING_BASE_TRAVEL,
    GOT_BASE_TRAVEL
} from './types'

const initialState = {
    arrayBaseTravel: [],
    loading:false,
    proTab:'Programados'
}

export default (state = initialState, action) => {
    switch (action.type) {
        case WAITING_BASE_TRAVEL:
            return { ...state, loading: true }
        case GOT_BASE_TRAVEL:
            return { ...state, loading: false,
                arrayBaseTravel:action.payload.viajes,
                proTab:action.payload.tab
            }
        default:
            return {...state}
    }
}
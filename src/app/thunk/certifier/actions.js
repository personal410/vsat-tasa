import { sw_sackLots, sw_plants, sw_saveLot, sw_updateLot, sw_deleteLot, sw_unitInWay, sw_saveSack, sw_getResampling, sw_saveResampling } from '../../api'
import { SACK_SET_CURRENT_LOT, RESAMPLING_SET_CURRENT_LOT } from './types'

export const getSackLots = (payload) => {
    return (_dispatch, getState) => {
        const { logIn } = getState()
        const { callback } = payload
        sw_sackLots(logIn.user.token).then(r => {
            if(r.Success == 1){
                callback(null, r.Data == null ? [] : r.Data)
            }else{
                callback(r.Message, null)
            }
        }).catch(e => {
            callback(e.message, null)
        })
    }
}
export const getPlants = (payload) => {
    return (_dispatch, getState) => {
        const { logIn } = getState()
        const { callback } = payload
        sw_plants(logIn.user.token).then(r => {
            if(r.Success == 1){
                callback(null, r.Data == null ? [] : r.Data)
            }else{
                callback(r.Message, null)
            }
        }).catch(e => {
            callback(e.message, null)
        })
    }
}
export const setCurrentSackLot = (payload) => {
    return (dispatch) => {
        const { currentLot, callback } = payload
        dispatch({
            type: SACK_SET_CURRENT_LOT,
            payload: currentLot
        })
        callback()
    }
}
export const saveLot = (payload) => {
    return (_dispatch, getState) => {
        const { logIn } = getState()
        const { plantId, lote, callback, loteId } = payload
        if(loteId == null){
            sw_saveLot(logIn.user.token, { Base: plantId, Lote: lote }).then(r => {
                if(r.Success == 1){
                    callback(null)
                }else{
                    callback(r.Message)
                }
            }).catch(e => {
                callback(e.message)
            })
        }else{
            sw_updateLot(logIn.user.token, { Base: plantId, Lote: lote, LoteId: loteId }).then(r => {
                if(r.Success == 1){
                    callback(null)
                }else{
                    callback(r.Message)
                }
            }).catch(e => {
                callback(e.message)
            })
        }
    }
}
export const deleteLot = (payload) => {
    return (_dispatch, getState) => {
        const { logIn } = getState()
        const { LoteId, callback } = payload
        sw_deleteLot(logIn.user.token, { LoteId }).then(r => {
            if(r.Success == 1){
                callback(null)
            }else{
                callback(r.Message)
            }
        }).catch(e => {
            callback(e.message)
        })
    }
}
export const getUnitInWay = (payload) => {
    return (_dispatch, getState) => {
        const { logIn } = getState()
        const { callback } = payload
        sw_unitInWay(logIn.user.token).then(r => {
            if(r.Success == 0){
                let trucks = []
                if(r.Data != null){
                    r.Data.forEach((item) => {
                        item.title = item.VehiclePlate
                        item.id = item.RequesId
                        trucks.push(item)
                    })
                }
                callback(null, trucks)
            }else{
                callback(r.Message, null)
            }
        }).catch(e => {
            callback(e.message, null)
        })
    }
}
export const saveSack = (payload) => {
    return (_dispatch, getState) => {
        const { loteId, sackCount, comments, temperatures, currentIdTruck, callback } = payload
        if(sackCount.length == 0){
            callback("Debe ingresar el número de sacos")
            return
        }
        if(currentIdTruck == -1){
            callback("Debe seleccionar el camión")
            return
        }
        const temperatureMissing = temperatures.filter((v) => { return v.length == 0 }).length > 0
        if(temperatureMissing){
            callback("Debe ingresar todas las temperaturas")
            return
        }
        const { logIn, latitude, longitude } = getState()
        let params = { LoteId: loteId, Note: comments, Latitude: latitude, Longitude: longitude, NumSack: sackCount, RequestId: currentIdTruck }
        let avgTemperature = 0
        temperatures.forEach((v, i) => {
            params["Temp" + (i + 1)] = v
            avgTemperature += parseInt(v)
        })
        avgTemperature = avgTemperature / temperatures.length
        params["AvgTemp"] = avgTemperature
        sw_saveSack(logIn.user.token, params).then(r => {
            if(r.Success == 1){
                callback(null)
            }else{
                callback(r.Message)
            }
        }).catch(e => {
            callback(e.message)
        })
    }
}

export const getResamplingLots = (payload) => {
    return (_dispatch, getState) => {
        const { logIn } = getState()
        const { callback } = payload
        sw_getResampling(logIn.user.token).then(r => {
            if(r.Success == 1){
                callback(null, r.Data.Lotes == null ? [] : r.Data.Lotes)
            }else{
                callback(r.Message, null)
            }
        }).catch(e => {
            callback(e.message, null)
        })
    }
}
export const setCurrentResamplingLot = (payload) => {
    return (dispatch) => {
        const { currentLot, callback } = payload
        dispatch({
            type: RESAMPLING_SET_CURRENT_LOT,
            payload: currentLot
        })
        callback()
    }
}
export const saveResampling = (payload) => {
    return (_dispatch, getState) => {
        const { loteId, sackCount, min, max, callback } = payload
        if(sackCount.length == 0){
            callback("Debe ingresar el número de sacos")
            return
        }
        if(min.length == 0){
            callback("Debe ingresar la temperatura mínima")
            return
        }
        if(max.length == 0){
            callback("Debe ingresar la temperatura máxima")
            return
        }
        let vMin = parseInt(min)
        let vMax = parseInt(max)
        if(vMin > vMax){
            callback("La temperatura mínima debe ser menor o igual a la temperatura máxima")
            return
        }
        const { logIn, latitude, longitude } = getState()
        let params = { LoteId: loteId, Max: max, Min: min, NumSack: sackCount, Note: "", Latitude: latitude, Longitude: longitude }
        sw_saveResampling(logIn.user.token, params).then(r => {
            if(r.Success == 1){
                callback(null)
            }else{
                callback(r.Message)
            }
        }).catch(e => {
            callback(e.message)
        })
    }
}
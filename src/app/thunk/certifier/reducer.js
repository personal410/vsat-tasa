import { SACK_SET_CURRENT_LOT, RESAMPLING_SET_CURRENT_LOT } from './types'

const initialState = {
    currentSackLot: null,
    currentResamplingLot: null
}

export default (state = initialState, action) => {
    switch(action.type){
        case SACK_SET_CURRENT_LOT:
            return { ...state, currentSackLot: action.payload }
        case RESAMPLING_SET_CURRENT_LOT:
            return { ...state, currentResamplingLot: action.payload }
        default:
            return state
    }
}
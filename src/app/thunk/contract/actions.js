
import {
    WAITING_CONTRACT,
    STOP_WAITING_CONTRACT,
    GOT_LIST_CONTRACT,
    GOT_LOTE_CONTRACT
} from './types'


import { sw_listContracts, sw_listContractLote, sw_saveShipmentLote } from '../../api'

export const getListContracts = (info) => {
    const { token } = info
    return (dispatch) => {
        dispatch({
            type: WAITING_CONTRACT
        })


        sw_listContracts({}, token).then(r => {
            if (r.Success == 1) {


                dispatch({
                    type: GOT_LIST_CONTRACT,
                    payload: r.Data
                })
                dispatch({
                    type: STOP_WAITING_CONTRACT
                })
            }
        }).catch(e => {
            dispatch({
                type: STOP_WAITING_CONTRACT
            })


        })

        
    }
}


export const getListContractLote = (info) => {
    const { token, ContractId, callback } = info
    return dispatch => {
        dispatch({
            type: WAITING_CONTRACT
        })


        sw_listContractLote({ ContractId }, token).then(r => {
            if (r.Success == 1) {


                dispatch({
                    type: GOT_LOTE_CONTRACT,
                    payload: r.Data
                })


                callback();
                

            }
            dispatch({
                type: STOP_WAITING_CONTRACT
            })

        }).catch(e => {
            dispatch({
                type: STOP_WAITING_CONTRACT
            })

        })

        
        

    }
}

export const saveShipmentLote = (info) => {
    const { token, ContractId, InputMin, InputMax, LotesId, callback } = info

    // "ContractId":1,
    // "Lotes":[{
    // 	"Id":18,
    // 	"Min":10,
    // 	"Max":39
    // },
    // {
    // 	"Id":19,
    // 	"Min":10,
    // 	"Max":39
    // }]

    return dispatch => {
        dispatch({
            type: WAITING_CONTRACT
        })


        let Lotes = []


        LotesId.forEach((value,index)=>{
            const item={Id:value,Min:InputMin[index],Max:InputMax[index]}
            Lotes.push(item)
        })


        sw_saveShipmentLote({ ContractId,Lotes}, token).then(r => {
            console.log('responde del server',r)


            if (r.Success == 1) {
                callback();
            }
            dispatch({
                type: STOP_WAITING_CONTRACT
            })


        }).catch(e => {

            dispatch({
                type: STOP_WAITING_CONTRACT
            })

        })
        

    }
}


import {
    WAITING_CONTRACT,
    STOP_WAITING_CONTRACT,
    GOT_LIST_CONTRACT,
    GOT_LOTE_CONTRACT
} from './types'

const initialState = {
    listContracts: [],
    lotesContract: [],
    loading: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case WAITING_CONTRACT:
            return {
                ...state,
                loading: true
            }
        case STOP_WAITING_CONTRACT:
            return {
                ...state,
                loading: false
            }
        case GOT_LIST_CONTRACT:
            return {
                ...state,
                listContracts: action.payload
            }
        case GOT_LOTE_CONTRACT:
            return {
                ...state,
                lotesContract:action.payload
            }
        default:
            return state
    }
}
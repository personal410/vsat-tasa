import {
    CLEAR_STATE_NOTIFICACTIONS,
    GETTING_NOTIFICATIONS,
    GOT_NOTIFICATIONS,
    ERROR_GETTING_NOTIFICATIONS,
    SHOW_BADGE
} from './types'
import { sw_notificationList, sw_listNotificationAlmacen, sw_checkNotification, sw_qualityNotificationList, sw_certifierNotificationList } from '../../api'

import { DRIVER_ROLE_ID, GUARD_ROLE_ID, STORAGE_ROLE_ID, QUALITY_ROLE_ID, CERTIFIER_ROLE_ID ,TRANSPORT_ROLE_ID} from '../../../resources/consts'
import { showAlert } from '../../util'

export const clearStateNotifications = (info) => {
    const { callback } = info
    return (dispatch) => {
        dispatch({
            type: CLEAR_STATE_NOTIFICACTIONS
        })
        callback();
    }
}
export const initialDataNotifications = (info) => {
    const { token, RoleId } = info
    return (dispatch) => {
        dispatch({
            type: GETTING_NOTIFICATIONS
        })

        switch (RoleId) {
            case DRIVER_ROLE_ID:
            case GUARD_ROLE_ID:
                sw_notificationList({}, token).then(r => {
                    dispatch({
                        type: GOT_NOTIFICATIONS,
                        payload: r.Data
                    })
                }).catch(e => {
                    dispatch({
                        type: ERROR_GETTING_NOTIFICATIONS,
                    })
                })
                break;
            case STORAGE_ROLE_ID:
                sw_listNotificationAlmacen({}, token).then(r => {
                    dispatch({
                        type: GOT_NOTIFICATIONS,
                        payload: r.Data
                    })
                }).catch(e => {
                    dispatch({
                        type: ERROR_GETTING_NOTIFICATIONS,
                    })
                })
                break;
            case QUALITY_ROLE_ID:

                sw_qualityNotificationList({}, token).then(r => {
                    dispatch({
                        type: GOT_NOTIFICATIONS,
                        payload: r.Data
                    })
                }).catch(e => {
                    dispatch({
                        type: ERROR_GETTING_NOTIFICATIONS,
                    })
                })

                break;
            case CERTIFIER_ROLE_ID:

                sw_certifierNotificationList({}, token).then(r => {
                    dispatch({
                        type: GOT_NOTIFICATIONS,
                        payload: r.Data
                    })
                }).catch(e => {
                    dispatch({
                        type: ERROR_GETTING_NOTIFICATIONS,
                    })
                })

                break;
            case TRANSPORT_ROLE_ID:

                // sw_certifierNotificationList({}, token).then(r => {
                    dispatch({
                        type: GOT_NOTIFICATIONS,
                        payload: []
                    })
                // }).catch(e => {
                //     dispatch({
                //         type: ERROR_GETTING_NOTIFICATIONS,
                //     })
                // })

                break;
            default:
                showAlert({ msg: "Perfil desconocido", type: 1 })
                dispatch({
                    type: ERROR_GETTING_NOTIFICATIONS,
                })
                
        }

    }
}
export const showBadge = (payload) => {
    return dispatch => {
        dispatch({
            type: SHOW_BADGE,
            payload
        })
    }
}


export const goCheckNotification = (info) => {
    const { token, LoteId, callback } = info

    return dispatch => {
        sw_checkNotification({ LoteId }, token).then(r => {

            callback();
        }).catch(e => {

        })
    }


}
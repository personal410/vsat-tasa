import {
    CLEAR_STATE_NOTIFICACTIONS,
    GETTING_NOTIFICATIONS,
    GOT_NOTIFICATIONS,
    ERROR_GETTING_NOTIFICATIONS,
    SHOW_BADGE
} from './types'

const initialState = {
    notifications: [],
    loading: false,
    showBadge: false
}
export default (state = initialState, action) => {
    switch (action.type) {
        case CLEAR_STATE_NOTIFICACTIONS:
            return { ...initialState }
        case GETTING_NOTIFICATIONS:
            return {
                ...state,
                loading: true
            }
        case GOT_NOTIFICATIONS:
            return {
                ...state,
                notifications: action.payload,
                loading:false
            }
        case ERROR_GETTING_NOTIFICATIONS:
            return{
                ...state,
                loading:false
            }
        case SHOW_BADGE:
            return{
                ...state,
                showBadge: action.payload
            }
        default:
            return state
    }
}
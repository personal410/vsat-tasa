import {
    GETTING_PLATES,
    GOT_PLATES,
    ERROR_GET_PLATES,
    GETTING_LOTS,
    GOT_LOTS,
    ERROR_GET_LOTS,
    GETTING_NICHOS,
    GOT_NICHOS,
    GETTING_DIFFERENCE,
    GOT_DIFFERENCE,
    SAVING_RECEPTION,
    SAVED_RECEPTION,
    STOP_LOADING_RECEPTION

} from './types'
import { sw_platesList, sw_lotsList, sw_nichosList, sw_differenceSackList, sw_saveReception } from '../../api'
import { showAlert } from '../../util'

export const listPlates = (info) => {
    const { token, callback } = info
    return (dispatch) => {

        dispatch({
            type: GETTING_PLATES
        })
        sw_platesList({}, token).then(r => {

            dispatch({
                type: GOT_PLATES,
                payload: r.Data
            })
            callback()
        }).catch(e => {
            dispatch({
                type: ERROR_GET_PLATES,
            })
        })
    }
}

export const listLots = (info) => {
    const { Plate, token, callback } = info
    return (dispatch) => {

        dispatch({
            type: GETTING_LOTS
        })

        sw_lotsList({ Plate }, token).then(r => {
            console.log('list lots',r)
            if (r.Success == 1) {


                dispatch({
                    type: GOT_LOTS,
                    payload: r.Data
                })
                callback();
            } else {
                dispatch({
                    type: GOT_LOTS,
                    payload: []
                })
                callback();

            }

            dispatch({
                type: STOP_LOADING_RECEPTION
            })


        }).catch(e => {
            dispatch({
                type: ERROR_GET_LOTS,
            })
            dispatch({
                type: STOP_LOADING_RECEPTION
            })
        })
    }
}

export const listNichos = (info) => {
    const { Sector, Zone, token, callback } = info

    return (dispatch) => {

        dispatch({
            type: GETTING_NICHOS,
        })

        sw_nichosList({ Sector, Zone }, token).then(r => {
            if (r.Success == 1) {

                dispatch({
                    type: GOT_NICHOS,
                    payload: r.Data
                })
                dispatch({
                    type: STOP_LOADING_RECEPTION
                })
                callback();
            }

        }).catch(e => {
            dispatch({
                type: STOP_LOADING_RECEPTION
            })

        })


    }
}

export const differenceSacklist = (info) => {
    const { token } = info
    return (dispatch) => {

        dispatch({
            type: GETTING_DIFFERENCE,
        })

        sw_differenceSackList({}, token).then(r => {
            if (r.Success == 1) {

                dispatch({
                    type: GOT_DIFFERENCE,
                    payload: r.Data
                })
            }

            dispatch({
                type: STOP_LOADING_RECEPTION
            })

        }).catch(e => {
            dispatch({
                type: STOP_LOADING_RECEPTION
            })
        })
    }

}

export const saveReception = (info) => {
    const { LoteId, Temp1, Temp2, Temp3, Temp4, Temp5, AvgTemp, Latitude, Longitude, NumSack, DifferenceSakcId, LockerIds, ActionId, token, callback } = info

    return (dispatch) => {
        dispatch({
            type: SAVING_RECEPTION
        })

        if (ActionId <= 0) {
            showAlert({ msg: 'Es necesario ingresar todas las temperaturas', type: 2 })
            dispatch({
                type: SAVED_RECEPTION
            })
            return false
        }

        if (LockerIds.length <= 0) {
            showAlert({ msg: 'Debe seleccionar algún nicho', type: 2 })
            dispatch({
                type: SAVED_RECEPTION
            })
            return false
        }

        sw_saveReception(
            {
                LoteId
                , Temp1
                , Temp2
                , Temp3
                , Temp4
                , Temp5
                , AvgTemp
                , Note: ''
                , Latitude
                , Longitude
                , NumSack
                , DifferenceSakcId
                , LockerIds
                , ActionId

            }, token).then(r => {

                console.log('r recepcion',r)
                dispatch({
                    type: SAVED_RECEPTION
                })

                if (r.Success == 1) {



                    callback();
                }
                dispatch({
                    type: STOP_LOADING_RECEPTION
                })

            }).catch(e => {

                dispatch({
                    type: SAVED_RECEPTION
                })


                dispatch({
                    type: STOP_LOADING_RECEPTION
                })
            })
    }
}
import {
    GETTING_PLATES,
    GOT_PLATES,
    ERROR_GET_PLATES,
    GETTING_LOTS,
    GOT_LOTS,
    ERROR_GET_LOTS,
    GETTING_NICHOS,
    GOT_NICHOS,
    GETTING_DIFFERENCE,
    GOT_DIFFERENCE,
    SAVING_RECEPTION,
    SAVED_RECEPTION,
    STOP_LOADING_RECEPTION
} from './types'

const initialState = {
    plates: [],
    loading: false,
    lots: [],
    range: [],
    nichos: [],
    difference: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GETTING_PLATES:
            return {
                ...state,
                loading: true
            }
        case GOT_PLATES:
            return {
                ...state,
                loading: false,
                plates: action.payload,
                lots: []
            }
        case ERROR_GET_PLATES:
            return {
                ...state,
                loading: true
            }
        case GETTING_LOTS:
            return {
                ...state,
                loading: true
            }
        case GOT_LOTS:
            return {
                ...state,
                loading: false,
                lots: action.payload.Lotes,
                range: action.payload.AttributeTemperature
            }
        case GETTING_NICHOS:
            return {
                ...state,
                loading: true
            }
        case GOT_NICHOS:
            return {
                ...state,
                loading: false,
                nichos: action.payload
            }
        case ERROR_GET_LOTS:
            return {
                ...state,
                loading: false
            }
        case GETTING_DIFFERENCE:
            return {
                ...state,
                loading: true
            }
        case GOT_DIFFERENCE:
            return {
                ...state,
                difference: action.payload,
                loading: false
            }
        case SAVING_RECEPTION:
            return {
                ...state,loading: true
            }
        case SAVED_RECEPTION:
            return {
                ...state,loading: false
            }
        case STOP_LOADING_RECEPTION:
            return {
                ...state,
                loading:false
            }
        default:
            return state
    }
}
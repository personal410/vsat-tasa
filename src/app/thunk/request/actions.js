import {
    REQUEST_START_LOADING,
    REQUEST_STOP_LOADING,
    REQUEST_GOT_DATA,
    REQUEST_FILTER_PROVIDER,
    SET_INPUT_REQUEST
} from './types'

import _ from 'lodash'

import {sw_bases,sw_typeDoc,sw_product,sw_serviceType, sw_provider, sw_RegisterRequest, sw_cancelRequest, sw_updateRegisterRequest} from '../../api'
import { showAlert} from '../../util'
import moment from 'moment'

export const getInitialData = (info) =>{
    const {token} = info
    return (dispatch)=>{

        dispatch({
            type:REQUEST_START_LOADING
        })

        console.log('token request',token)


        const requests = [
            sw_bases({},token),
            sw_product({},token),
            sw_serviceType({},token),
            sw_provider({},token)

        ]
        console.log('entrando aqui')

        Promise.all(requests).then(function (values) {
            console.log('values',values)
            //BASES { value: 0, label: 'Seleccione Placa' }
            const basesTemp = values[0].Data
            let origen=[]
            let destino=[]
            basesTemp.map(item=>{
                origen.push({value: item.Id, label: item.Name})
                destino.push({value: item.Id, label: item.Name})
            })

            origen.push({value: 0, label: 'Seleccione Origen'})
            destino.push({value: 0, label: 'Seleccione Destino'})

            //PRODUCTS
            const productsTemp = values[1].Data

            let producto = []
            productsTemp.map(pro=>producto.push({value: pro.Id, label: pro.Name}) )
            producto.push({value: 0, label: 'Seleccione Producto'})
            //SERVICES TYPE
            const servicesTemp = values[2].Data
            let tipo=[]
            servicesTemp.map(tip=>tipo.push({value: tip.Id, label: tip.Description}) )
            tipo.push({value: 0, label: 'Seleccione Producto'})
            //PROVIDERS
            const providersTemp = values[3].Data

            
            dispatch({
                type:REQUEST_GOT_DATA,
                payload:{
                    origen:_.orderBy(origen,['value','ASC']),
                    destino:_.orderBy(destino,['value','ASC']),
                    producto:_.orderBy(producto,['value','ASC']),
                    tipo:_.orderBy(tipo,['value','ASC']),
                    proveedor:providersTemp,
                    date:moment().format('YYYY-MM-DD'),
                    hour:moment().format('HH:mm'),
                }
            })

            dispatch({
                type:REQUEST_STOP_LOADING
            })

        })
    }
}
export const filterProvider = (info) =>{
    const {idProduct, providers} = info
    return (dispatch) =>{
        dispatch({
            type:REQUEST_START_LOADING
        })

        let providerFiler=[]
        const providersTemp=providers.filter(i=>i.ProductId == idProduct)

        providersTemp.map(p=>providerFiler.push({value: p.Id, label: p.Name}))
        providerFiler.push({value: 0, label: 'Sel. Proveedor'})
        dispatch({
            type:REQUEST_FILTER_PROVIDER,
            payload:_.orderBy(providerFiler,['value','ASC']),
        })

        dispatch({
            type:REQUEST_STOP_LOADING
        })

    }
}

export const saveRequest = (info) =>{
    const {idOrigen,nameOrigen,idDestino,nameDestino,fecha,hora,idProducto,idTipo,idProveedor,unidades,observacion,RegisterId,token, callback} = info
    return (dispatch)=>{

        dispatch({
            type:REQUEST_START_LOADING
        })

        if(idOrigen == 0 || idDestino == 0 ||fecha == '' ||hora == '' ||idProducto == 0 ||idTipo == 0 ||idProveedor == 0 ||unidades == 0){
            showAlert({type:2,msg:'Debe de llenar todos los campos'})
            dispatch({
                type:REQUEST_STOP_LOADING
            })
            return false
        }

        const DateInitial=fecha+' '+hora
        console.log('response save',RegisterId)
        if(RegisterId==0){
            sw_RegisterRequest({
                ProviderId:[idProveedor],
                QuantityUnits:[unidades],
                BaseOrigenId:`${idOrigen}`,
                BaseOrigenText:nameOrigen,
                BaseFinalId:`${idDestino}`,
                BaseFinalText:nameDestino,
                Note:observacion,
                DateInitial,
                QuantityUnitTotal:1,
                ProductId:idProducto,
                TypeId:`${idTipo}`
            },token).then(r=>{
                console.log('response save',r)
                
                callback();
                dispatch({
                    type:REQUEST_STOP_LOADING
                })

            }).catch(e=>{ 
                console.log('error',e.message)
                dispatch({
                    type:REQUEST_STOP_LOADING
                })
            })
        }else{

            sw_updateRegisterRequest({
                RequestId:`${RegisterId}`,
                ProviderId:`${idProveedor}`,
                BaseOrigenId:`${idOrigen}`,
                BaseOrigenText:nameOrigen,
                BaseFinalId:`${idDestino}`,
                BaseFinalText:nameDestino,
                Note:observacion,
                DateInitial,
                QuantityUnitTotal:1,
                ProductId:`${idProducto}`,
                TypeId:`${idTipo}`,
                
            },token).then(r=>{
                console.log('response save',r)
                callback();
                dispatch({
                    type:REQUEST_STOP_LOADING
                })
            }).catch(e=>{ 
                console.log('error',e)
                dispatch({
                    type:REQUEST_STOP_LOADING
                })
            })



        }

        
        



    }
}

export const setInputRequest = (info) =>{
    const {prop,value}=info
    return (dispatch)=>{
        dispatch({
            type:SET_INPUT_REQUEST,
            payload:{prop,value}
        })
    }
}

export const setNameBase = (info) =>{
    const {prop,base,idBase}=info
    return (dispatch)=>{
        const value = base.filter(b=>b.value==idBase)
        dispatch({
            type:SET_INPUT_REQUEST,
            payload:{prop,value:value[0].label}
        })
    }
}

export const deleteRequest = (info)=>{

    const {RequestId,token, callback} = info
    return (dispatch)=>{

        sw_cancelRequest({
                RequestId:RequestId,
                Note:"SE HA CANCELADO EL VIAJE",
                Reason:1
            },token).then(r=>{
                console.log('response save',r)
                callback();
            }).catch(e=>{ 
                console.log('error',e.message)
            })

        dispatch({
            type:REQUEST_STOP_LOADING
        })
        



    }

}
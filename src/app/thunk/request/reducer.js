import {
    REQUEST_START_LOADING,
    REQUEST_STOP_LOADING,
    REQUEST_GOT_DATA,
    REQUEST_FILTER_PROVIDER,
    SET_INPUT_REQUEST
} from './types'


const initialState={
    origen:[],
    destino:[],
    producto:[],
    tipo:[],
    proveedor:[],
    proveedorFilter:[],
    loading:false,
    idSelectOrigen:0,
    idSelectDestino:0,
    idSelectProduct:0,
    idSelectType:0,
    idSelectProvider:0,
    date: '2019-11-24' ,
    hour: "20:00",
    unit:'',
    observation:'',
    RegisterId:0,
    OrigenName:'',
    DestinoName:''
    
}

export default (state=initialState, action)=>{
    
    switch(action.type){
        case REQUEST_START_LOADING:
            return {
                ...state,
                loading:true
            }
        case REQUEST_STOP_LOADING:
                return {
                    ...state,
                    loading:false
                }
        case REQUEST_GOT_DATA:
            return {
                ...state,
                origen:action.payload.origen,
                destino:action.payload.destino,
                producto:action.payload.producto,
                tipo:action.payload.tipo,
                proveedor:action.payload.proveedor,
                proveedorFilter:[{ value: 0, label: 'Sel. Proveedor' }],
                RegisterId:0,
                date: action.payload.date,
                hour:action.payload.hour,
            }
        case SET_INPUT_REQUEST:
            const {prop , value}= action.payload
            return{
                ...state,
                [prop]:value
            }
        case REQUEST_FILTER_PROVIDER:
            return {
                ...state,
                proveedorFilter:action.payload
            }
        default:
            return state
    }
}
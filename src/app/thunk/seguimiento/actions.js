import {
    WAITING_FOLLOW,
    GOT_LIST_FOLLOW,
    STOP_WAITING_FOLLOW
} from './types'

import { showAlert } from '../../util'
import { sw_listFollow, sw_saveFollow } from '../../api'

export const getListFollow = (info) => {
    const { token ,callback} = info

    return (dispatch) => {
        dispatch({
            type: WAITING_FOLLOW
        })


        sw_listFollow({}, token).then(r => {
            console.log('responde follow', r)
            if (r.Success == 1) {


                dispatch({
                    type: GOT_LIST_FOLLOW,
                    payload: r.Data
                })

            }

            callback();

        }).catch(e => {
            console.log('Mensaje de error', e.message)

            showAlert({ type: 2, msg: 'Error en la conexión' })
        })

        dispatch({
            type: STOP_WAITING_FOLLOW
        })
    }
}

export const saveFollow = (info) => {
    const { LoteId, Temp1, Temp2, Temp3, Temp4, Temp5, Temp6, Temp7, Temp8, Temp9, AvgTemp, token, Latitude, Longitude, ActionId , callback} = info
    return (dispatch) => {

        if (Temp1 == 0 || Temp2 == 0 || Temp3 == 0 || Temp4 == 0 || Temp5 == 0 || Temp6 == 0 || Temp7 == 0 || Temp8 == 0 || Temp9 == 0 || AvgTemp == 0) {
            showAlert({ type: 2, msg: 'Debe ingresar todas las temperaturas' })
            return false
        }
        dispatch({
            type: WAITING_FOLLOW
        })


        sw_saveFollow({ LoteId, Temp1, Temp2, Temp3, Temp4, Temp5, Temp6, Temp7, Temp8, Temp9, AvgTemp, token, Latitude, Longitude, ActionId }, token).then(r => {
            console.log('response save',r)

            if (r.Success == 1) {
                callback();
            }

        }).catch(e => {
            showAlert({ type: 2, msg: 'Error en la conexión' })
        })

        dispatch({
            type: STOP_WAITING_FOLLOW
        })
    }

}


import {
    WAITING_FOLLOW,
    GOT_LIST_FOLLOW,
    STOP_WAITING_FOLLOW
} from './types'
const initialState = {
    loading: false,
    listFollow: [],
    rangeFollow:[]
}

export default (state = initialState, action) => {
    switch (action.type) {
        case WAITING_FOLLOW:
            return {
                ...state,
                loading: true
            }
        case GOT_LIST_FOLLOW:
            return {
                ...state,
                listFollow: action.payload.Lotes,
                rangeFollow:action.payload.AttributeTemperature
            }
        case STOP_WAITING_FOLLOW:
            return {
                ...state,
                loading: false
            }
        default:
            return state
    }
}
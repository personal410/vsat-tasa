import { sw_sendLocation, sw_myServices, sw_startService, sw_activityList, sw_checkActivity, sw_cancelTravel, sw_cancelReasons } from '../../api'
import { SET_CURRENT_TRAVEL, SET_WATCHER_ID, SET_CURRENT_TASKS, SET_CURRENT_POSITION, CHECK_TASK, SET_REFRESH_TRAVEL, SET_IN_INITIAL_SERVICE } from './types'
import { getUTCDateText } from '../../util'

export const getMyServices = (payload) => {
    return dispatch => {
        const { token, status, callback } = payload
        sw_myServices(token, status).then(r => {
            if (r.Success == 0) {
                callback(null, r.Data == null ? [] : r.Data)

            } else {
                callback(r.Message, null)
            }
            dispatch({
                type: SET_REFRESH_TRAVEL,
                payload: false
            })
        }).catch(e => {
            callback(e.message, null)
        })
    }
}
export const setCurrentTravel = (payload) => {
    return dispatch => {
        const { travel, callback } = payload
        dispatch({
            type: SET_CURRENT_TRAVEL,
            payload: travel
        })
        callback()
    }
}

export const activityList = (payload) => {
    return dispatch => {
        const { RequestId, token, callback } = payload
        newRequest = RequestId.toString()
        console.log('activityList')
        sw_activityList({ RequestId: newRequest }, token).then(r => {
            console.log('r',r)
            if (r.Success == 1) {
                dispatch({
                    type: SET_CURRENT_TASKS,
                    payload: r.Data
                })
                callback(null)
            } else {
                callback(r.Message)
            }
        }).catch(e => {
            console.log('error',e)
            callback(e.message, null)
        })

    }
}
export const startService = (payload) => {
    return dispatch => {
        const { RequestId, token, callback } = payload
        sw_startService({ RequestId }, token).then(r => {
            if (r.Success == 1) {
                dispatch({
                    type: SET_CURRENT_TASKS,
                    payload: r.Data
                })
                callback(null)
            } else {
                callback(r.Message)
            }
        }).catch(e => {
            callback(e.message, null)
        })
    }
}
export const setWatcherId = (payload) => {
    return dispatch => {
        dispatch({
            type: SET_WATCHER_ID,
            payload
        })
    }
}
export const setCurrentPosition = payload => {
    return dispatch => {
        dispatch({
            type: SET_CURRENT_POSITION,
            payload
        })
    }
}
export const checkActivity = payload => {
    return dispatch => {
        const { RequestId, ActivityId, Latitude, Longitude, token, InInitialService, callback } = payload
        const DateCheck = getUTCDateText()
        sw_checkActivity({ RequestId, ActivityId, DateCheck, Latitude, Longitude }, token).then(r => {
            console.log('responde r', r)
            if (r.Success == 1) {
                dispatch({
                    type: CHECK_TASK,
                    payload: { ActivityId, DateCheck }
                })
                callback(null)


                dispatch({
                    type: SET_IN_INITIAL_SERVICE,
                    payload: InInitialService
                })

            } else {
                callback(r.Message)
            }
        }).catch(e => {
            callback(e.message)
        })
    }
}

export const resetInitialService = (id) => {
    return (dispatch) => {
        dispatch({
            type: SET_IN_INITIAL_SERVICE,
            payload: id
        })
    }
}
export const sendLocation = (params) => {

    return dispatch => {
        const { latitude, longitude, speed, course, altitude, date, event, token, ETA, RequestId } = params
        sw_sendLocation({ latitude, longitude, speed, course, altitude, date, event, ETA, RequestId }, token).then((r) => {
            console.log('sw_sendLocation ok')
        }).catch((e) => {
            console.warn('e: ' + e.code)
        })

    }
}
export const cancelTravel = (payload) => {
    const { RequestId, token, Reason, Note, callback } = payload
    return (dispatch) => {
        sw_cancelTravel({
            RequestId,
            Reason,
            Note
        }, token).then((r) => {
            callback();
            dispatch({
                type: SET_REFRESH_TRAVEL,
                payload: true
            })

        }).catch((e) => {
            console.warn('e: ' + e.code)
        })



    }
}
export const listCancelTravelReason = (payload) => {
    const { token, callback } = payload

    return (dispatch) => {

        sw_cancelReasons({
        }, token).then((r) => {
            if (r.Success == 1) {
                let listReason = [{ key: '0', id: 0, title: 'Seleccione un motivo' }]
                r.Data.forEach((item) => {
                    listReason.push({ key: `${item.Id}`, id: item.Id, title: item.Description })
                })
                callback(listReason)
            }
        }).catch((e) => {
            console.warn('e: ' + e.code)
        })

    }
}
export const refreshTavel = payload => {
    return dispatch => {
        dispatch({
            type: SET_REFRESH_TRAVEL,
            payload
        })
    }
}
import { SET_CURRENT_TRAVEL, SET_CURRENT_TASKS, SET_WATCHER_ID, SET_CURRENT_POSITION, CHECK_TASK, SET_REFRESH_TRAVEL,SET_IN_INITIAL_SERVICE } from './types'

const initialState = {
    currentTravel: null,
    currentTasks: [],
    watcherId: null,
    currentPosition: null,
    refreshTravel:false,
    initialService:1
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_CURRENT_TRAVEL:
            return { ...state, currentTravel: action.payload }
        case SET_CURRENT_TASKS:
            return { ...state, currentTasks: action.payload }
        case SET_WATCHER_ID:
            return { ...state, watcherId: action.payload }
        case SET_CURRENT_POSITION:
            return { ...state, currentPosition: action.payload }
        case CHECK_TASK:
            const { ActivityId, DateCheck } = action.payload
            let { currentTasks } = state
            idxTask = currentTasks.findIndex(task => task.Id == ActivityId)
            currentTasks[idxTask].DateRegister = DateCheck
            currentTasks[idxTask].StatusId = 2
            return { ...state, currentTasks }
        case SET_REFRESH_TRAVEL:
            return { ...state,refreshTravel:action.payload}
        case SET_IN_INITIAL_SERVICE:
            return {...state,initialService:action.payload}
        default:
            return state
    }
}
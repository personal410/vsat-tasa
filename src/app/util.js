import { Alert } from 'react-native'
import moment from 'moment'

export function isEmpty(str) {
    return (!str || 0 === str.length);
}
export function showAlert({ msg, type }) {
    switch (type) {
        case 1:
            return Alert.alert('Exito', msg)
        case 2:
            return Alert.alert('Alerta', msg)
        case 3:
            return Alert.alert('Error', msg)

    }
}
export function calculateDistance(firstCoords, secondCoords) {
    var R = 6371000; // Radius of the earth in m
    var dLat = deg2rad(firstCoords.latitude - secondCoords.latitude);
    var dLon = deg2rad(firstCoords.longitude - secondCoords.longitude);
    var a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(deg2rad(firstCoords.latitude)) * Math.cos(deg2rad(secondCoords.latitude)) * Math.sin(dLon / 2) * Math.sin(dLon / 2)
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
}

export function getUTCDateText(date = new Date()) {
    // let horas=0;
    // let resta=0;
    // dias=formatTwoNumbers(date.getUTCDate() ) *1
    // console.log('Number.parseInt(formatTwoNumbers(date.getUTCHours() ) )',Number.parseInt(formatTwoNumbers(date.getUTCHours() ) ))
    // if(formatTwoNumbers(date.getUTCHours() )  <5 ){
    //     resta= 5- ( formatTwoNumbers(date.getUTCHours())*1 )
    //     horas=24-resta
    //     dias=dias-1
    // }else{
    //     horas= ( formatTwoNumbers(date.getUTCHours())  * 1)-5
        
    // }
    // return date.getUTCFullYear().toString() + '-' + formatTwoNumbers(date.getUTCMonth() + 1) + '-' + dias + ' ' + horas + ':' + formatTwoNumbers(date.getUTCMinutes()) + ':' + formatTwoNumbers(date.getUTCSeconds())
    return moment()
}

export function getUTCDate0Text(date = new Date()){
    return date.getUTCFullYear().toString() + '-' + formatTwoNumbers(date.getUTCMonth() + 1) + '-' + formatTwoNumbers(date.getUTCDate()) + ' ' + formatTwoNumbers(date.getUTCHours()) + ':' + formatTwoNumbers(date.getUTCMinutes()) + ':' + formatTwoNumbers(date.getUTCSeconds())
}


function deg2rad(deg) {
    return deg * (Math.PI / 180)
}
function formatTwoNumbers(text) {
    return ('0' + text.toString()).substr(-2)

}
export function  Capitalize(str){
    return str.charAt(0).toUpperCase() + str.slice(1);

}
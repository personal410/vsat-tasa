import React, { PureComponent } from 'react';
import { Image, Text, TouchableOpacity, View, FlatList } from 'react-native'
import { spaceBlack, spaceBlackTrans, red, lightBlue } from '../../resources/color'
import { ic_green_check, ic_triangle_warning } from '../../resources/icon'

class DarkDropDown extends PureComponent {

    render() {
        
        
        if (!this.props.isVisible) {
            return null
        }
        return (
            
            <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' ,}}>
                <View style={{borderWidth:1,borderColor:'black'}} >
                    <View style={{ backgroundColor: spaceBlack, padding: 20, paddingBottom: 10, flexDirection: "row" }}>

                        <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={ic_triangle_warning} style={{ width: 25, height: 25, resizeMode: 'contain', tintColor: '#EB6560' }} />
                        </View>
                        <View style={{ width: '80%', justifyContent: 'center' }}>
                            <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 22 }}>{this.props.title}</Text>
                            <Text style={{ color: 'white', fontSize: 17 }}>{this.props.subtitle}</Text>
                        </View>

                    </View>
                    {(this.props.listReason.length > 0) ?
                        <FlatList
                            style={{ height: 200, backgroundColor: '#FFFFFF' }}
                            data={this.props.listReason}
                            keyExtractor={(item) => `${item.id}`}
                            renderItem={({ item }) => {
                                return (
                                    <TouchableOpacity style={{ backgroundColor: '#FFF', borderBottomColor: '#CCC', borderBottomWidth: 1, height: 50, width: '100%' }}
                                        onPress={() => this.props.selectReason(item.id, item.title)}
                                    >
                                        <Text style={{ paddingLeft: 30, marginTop: 20, width: '80%', color: spaceBlack }}>{item.title}</Text>
                                        <Image style={{ width: 15, height: 15, resizeMode: 'contain', alignSelf: 'flex-end', marginTop: -15, marginRight: 20, 
                                        tintColor: (item.id == this.props.idReason) ? '' : '#CCC' }} 
                                        source={ic_green_check} />
                                    </TouchableOpacity>
                                )
                            }
                            }
                        /> : null}

                    {/* <View style={{ alignSelf: 'center', backgroundColor: '#FFF', padding:30, flexDirection: "row" }}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <TouchableOpacity
                                onPress={() => { this.props.acceptDifferenceReason() }}
                                style={{ backgroundColor: lightBlue, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center', height: 35 }}>
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Guardar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => { this.props.cancelDifferenceReason()  }}
                                style={{ backgroundColor: '#EB6560', marginStart: 10, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Cancelar</Text>
                            </TouchableOpacity>
                        </View>
                    </View> */}
                </View>
            </View>
        );

   

    }
}

export default DarkDropDown;
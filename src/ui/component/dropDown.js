import React, { PureComponent } from 'react';
import { Image, Text, TouchableOpacity, View, FlatList } from 'react-native'
import { spaceBlack, spaceBlackTrans } from '../../resources/color'
import { ic_green_check } from '../../resources/icon'
import _ from 'lodash'

class DropDown extends PureComponent {

    state = {
        selected: []
    }
    componentDidUpdate() {
       
        if (this.props.listReason != this.state.selected) {
            this.setState({ selected: this.props.listReason })
        }
    }

    render() {
        
        if (!this.props.isVisible) {
            return null
        }
        return (
            <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end', flex: 1 }}>
                <View>
                    <View style={{ alignSelf: 'stretch', backgroundColor: '#FFF', padding: 20, paddingBottom: 10, flexDirection: "row" }}>
                        <View>
                            <Text style={{ fontWeight: 'bold', color: spaceBlack, fontSize: 20 }}>{this.props.title}</Text>
                        </View>
                    </View>
                    <FlatList
                        style={{ height: 200, backgroundColor: '#FFFFFF' }}
                        data={_.orderBy(this.state.selected, ['id'], ['asc'])}
                        keyExtractor={(item) => `${item.id}`}
                        renderItem={({ item }) => {

                            return (
                                <TouchableOpacity style={{ backgroundColor: '#FFF', borderBottomColor: '#CCC', borderBottomWidth: 1, height: 50, width: '100%' }}
                                    onPress={() => {
                                        this.props.selectReason(item.id, item.title)
                                        if (this.props.multiple) {
                                            this.props.selectItem(item)
                                            
                                        }

                                    }}
                                >
                                    <Text style={{ paddingLeft: 30, marginTop: 20, width: '80%', color: spaceBlack }}>{item.title}</Text>
                                    {/* <Image
                                        style={{
                                            width: 15, height: 15, resizeMode: 'contain', alignSelf: 'flex-end', marginTop: -15, marginRight: 20,
                                            tintColor: (!this.props.multiple) ? (item.id == this.props.idReason) ? '' : '#CCC' : (item.isSelected) ? '' : '#CCC'
                                        }}
                                        source={ic_green_check} /> */}
                                    <Image style={[{
                                        width: 15, height: 15, resizeMode: 'contain', alignSelf: 'flex-end', marginTop: -15, marginRight: 20
                                    }, (!this.props.multiple) ? (item.id == this.props.idReason) ? null : { tintColor: '#CCC' } : (item.isSelected) ? null : { tintColor: '#CCC' }
                                    
                                    ]}

                                        source={ic_green_check} />
                                </TouchableOpacity>
                            )
                        }
                        }
                    />
                    {this.props.children}
                </View>
            </View>
        );
    }
}

export default DropDown;
import React, { PureComponent } from 'react';
import {View, ActivityIndicator, Dimensions} from 'react-native';

import { blackTransparent, lightBlue } from '../../resources/color'
const {width,height} = Dimensions.get('window');
class Modal extends PureComponent {
    constructor(props){
        super(props);
    }
    render(){
        if(!this.props.isVisible){
            return null
        }
        return(
            <View 
                style={{position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, flex: 1, alignItems: 'center', flexDirection: 'column',  justifyContent: 'space-around', backgroundColor: blackTransparent}}>

                <View style={{backgroundColor: 'white', width: width-20, height: height-200, borderRadius: 10}}>
                    {this.props.children}
                </View>
            </View>
        );
    }
}

export default Modal;
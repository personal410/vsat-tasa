import React, { PureComponent } from 'react'
import { View, Image } from 'react-native'
import { connect } from 'react-redux'

import { lightBlue } from '../../resources/color'
import { ic_home, ic_truck, ic_notification, ic_check_list } from '../../resources/icon'

class TabBarItem extends PureComponent {
    render(){
        const tint = this.props.focused ? lightBlue : 'white'
        let icon = null
        let showBadge = false
        switch(this.props.type){
            case 0:
                icon = ic_home
                break;
            case 1:
                icon = ic_truck
                break;
            case 2:
                icon = ic_notification
                if(this.props.showBadge){
                    showBadge = true
                }
                break;
            case 3:
                icon = ic_check_list
                break;
        }
        return (
            <View style={{ width: 25, height: 25 }}>
                <Image
                    resizeMode={'contain'}
                    source={icon}
                    style={{ width: 25, height: 25, tintColor: tint }} />
                { showBadge ?
                    <View style={{ width: 10, height: 10, backgroundColor: 'red', position: 'absolute', borderRadius: 5, right: 0 }} /> 
                    : null
                }
            </View>
        )
    }
}
const mapStateProps = ({notifications}) => {
    const { showBadge } = notifications
    return { showBadge }
}
export default connect(mapStateProps, { })(TabBarItem);
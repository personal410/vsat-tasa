import React, { PureComponent } from 'react';
import { Alert, FlatList, Image, SafeAreaView, Text, TouchableOpacity, View, StatusBar, Dimensions } from 'react-native';
import { connect } from 'react-redux'
import Geolocation from 'react-native-geolocation-service'
import firebase from 'react-native-firebase'

import { spaceBlack, lightBlue, lighterBlue, green, lightGreen,red } from '../../../resources/color'
import { ic_calendar, ic_location, ic_vertical_line, ic_get_out, ic_sync, ic_truck_front, ic_clock_2, ic_buildings } from '../../../resources/icon'
import { logOut, setLocationUser } from '../../../app/thunk/auth/actions'
import { listBaseTravel } from '../../../app/thunk/baseTravel/actions'
import { setWatcherId } from '../../../app/thunk/travel/actions'
import Loader from '../../component/loader'
import { Capitalize } from '../../../app/util'
import DropdownAlert from 'react-native-dropdownalert';

import { TRANSPORT_ROLE_ID} from '../../../resources/consts'

import Modal from '../../component/modal'

import { deleteRequest, setInputRequest, filterProvider} from '../../../app/thunk/request/actions'

class BaseTravelsScreen extends PureComponent {
    nameTab = []
    nameProductTab = []

    constructor(props) {
        super(props)
        this.state = {
            tabs: [{ key: "1", title: "0 En planta" }, { key: "2", title: "0 En curso" }, { key: "3", title: "0 Programados" }],
            products: [{ key: "1", title: "Harina de pescado" }, { key: "2", title: "Aceite de pescado" }],
            idxSelectedTab: "1",
            idxSelectedProduct: "1",
            baseTravels: [{ Id: 1 }],
            ongoingTravels: [{ Id: 2 }, { Id: 3 }],
            scheduledTravels: [{ Id: 4 }, { Id: 5 }, { Id: 6 }],
            loading: false,
            finishedLoaded: false,
            headerColor: 'dark-content',
            selectedTab: -1,
            selectedNameTab: '',
            selectedTabProduct: -1,
            selectProductNameTab: '',
            arrayTabProduct: [],
            arrayItemProduct: [],
            visibleModal:false,
            itemModal:null

        }
        this._getOut.bind(this)
        this._onTabPressed.bind(this)
        this._onProductPressed.bind(this)
        this._updateData.bind(this)
    }

    componentDidMount() {
        Geolocation.clearWatch(this.props.watcherId)
        this.didFocusListener = this.props.navigation.addListener('didFocus', () => {
            this.setState({ headerColor: 'dark-content' })
        })

        this.props.listBaseTravel({
            token: this.props.user.token,RoleId:this.props.user.RoleId, callback: () => {

                if (this.props.arrayBaseTravel.length > 0) {
                    this.setState({ selectedTab: 0, selectedTabProduct: -1, selectedNameTab: `${Object.keys(this.props.arrayBaseTravel[0])[0]}`, arrayItemProduct: [] }, () => {
                        this.setState({ arrayTabProduct: this.props.arrayBaseTravel[this.state.selectedTab][this.state.selectedNameTab] }, () => {
                            if (this.state.arrayTabProduct.length > 0) {
                                this.setState({ selectedTabProduct: 0, selectProductNameTab: `${Object.keys(this.state.arrayTabProduct[0])[0]}` }, () => {
                                    this.setState({ arrayItemProduct: this.props.arrayBaseTravel[this.state.selectedTab][this.state.selectedNameTab][this.state.selectedTabProduct][this.state.selectProductNameTab] })
                                })
                            } else {
                                this.setState({ selectedTabProduct: -1, selectProductNameTab: '' }, () => {
                                    this.setState({ arrayItemProduct: [] })
                                })
                            }
                        })
                    })
                } else {

                    this.setState({ selectedTab: -1, selectedTabProduct: -1, selectedNameTab: '', arrayItemProduct: [] }, () => {
                        this.setState({ arrayTabProduct: [] }, () => {
                            this.setState({ selectedTabProduct: -1, selectProductNameTab: '' }, () => {
                                this.setState({ arrayItemProduct: [] })
                            })

                        })
                    })

                }
            }
        })

        this.onNotificationListener = firebase.notifications().onNotification(async notification => {
            const { title, body, data } = notification
            this.dropDownAlertRef.alertWithType('info', title, body);
            this.props.listBaseTravel({
                token: this.props.user.token,RoleId:this.props.user.RoleId, callback: () => {

                    if (this.props.arrayBaseTravel.length > 0) {
                        this.setState({ selectedTab: 0, selectedTabProduct: -1, selectedNameTab: `${Object.keys(this.props.arrayBaseTravel[0])[0]}`, arrayItemProduct: [] }, () => {
                            this.setState({ arrayTabProduct: this.props.arrayBaseTravel[this.state.selectedTab][this.state.selectedNameTab] }, () => {
                                if (this.state.arrayTabProduct.length > 0) {
                                    this.setState({ selectedTabProduct: 0, selectProductNameTab: `${Object.keys(this.state.arrayTabProduct[0])[0]}` }, () => {
                                        this.setState({ arrayItemProduct: this.props.arrayBaseTravel[this.state.selectedTab][this.state.selectedNameTab][this.state.selectedTabProduct][this.state.selectProductNameTab] })
                                    })
                                } else {
                                    this.setState({ selectedTabProduct: -1, selectProductNameTab: '' }, () => {
                                        this.setState({ arrayItemProduct: [] })
                                    })
                                }
                            })
                        })
                    } else {

                        this.setState({ selectedTab: -1, selectedTabProduct: -1, selectedNameTab: '', arrayItemProduct: [] }, () => {
                            this.setState({ arrayTabProduct: [] }, () => {
                                this.setState({ selectedTabProduct: -1, selectProductNameTab: '' }, () => {
                                    this.setState({ arrayItemProduct: [] })
                                })

                            })
                        })

                    }

                }
            })
        })

        this.onNotificationOpenedListener = firebase.notifications().onNotificationOpened(notificationOpen => {
            const { action } = notificationOpen.notification.data
            if (action != null && action != undefined) {
                this._updateData(action)
            }
        })

        const watchOptions = Platform.OS === 'ios' ? { enableHighAccuracy: true, distanceFilter: 1 } : { enableHighAccuracy: true, distanceFilter: 1, interval: 10, fastestInterval: 10 }
        const watcherId = Geolocation.watchPosition((position) => {
            this.props.setLocationUser({ latitude: position.coords.latitude, longitude: position.coords.longitude })
        }, () => {

        }, watchOptions)
        this.props.setWatcherId(watcherId)
    }

    _updateData(action) {
        this.props.listBaseTravel({
            token: this.props.user.token,RoleId:this.props.user.RoleId, callback: () => {
                console.log('callback')
                console.log('this.props.arrayBaseTravel.length', this.props.arrayBaseTravel.length)
                if (this.props.arrayBaseTravel.length > 0) {
                    this.setState({ selectedTab: 0, selectedTabProduct: -1, selectedNameTab: `${Object.keys(this.props.arrayBaseTravel[0])[0]}`, arrayItemProduct: [] }, () => {
                        this.setState({ arrayTabProduct: this.props.arrayBaseTravel[this.state.selectedTab][this.state.selectedNameTab] }, () => {
                            if (this.state.arrayTabProduct.length > 0) {
                                this.setState({ selectedTabProduct: 0, selectProductNameTab: `${Object.keys(this.state.arrayTabProduct[0])[0]}` }, () => {
                                    this.setState({ arrayItemProduct: this.props.arrayBaseTravel[this.state.selectedTab][this.state.selectedNameTab][this.state.selectedTabProduct][this.state.selectProductNameTab] })
                                })
                            } else {
                                this.setState({ selectedTabProduct: -1, selectProductNameTab: '' }, () => {
                                    this.setState({ arrayItemProduct: [] })
                                })
                            }
                        })
                    })
                } else {

                    this.setState({ selectedTab: -1, selectedTabProduct: -1, selectedNameTab: '', arrayItemProduct: [] }, () => {
                        this.setState({ arrayTabProduct: [] }, () => {
                            this.setState({ selectedTabProduct: -1, selectProductNameTab: '' }, () => {
                                this.setState({ arrayItemProduct: [] })
                            })

                        })
                    })

                }
            }
        })
    }
    componentWillUnmount() {
        this.didFocusListener.remove()
        this.onNotificationListener()
        this.onNotificationOpenedListener()
    }
    _onTabPressed(newKey) {
        if (newKey != this.state.idxSelectedTab) {
            this.setState({ idxSelectedTab: newKey })
        }
    }
    _onProductPressed(newKey) {
        if (newKey != this.state.idxSelectedProduct) {
            this.setState({ idxSelectedProduct: newKey })

        }
    }

    _getOut() {
        Alert.alert("Alerta", "¿Está seguro de cerrar sesión?", [{
            text: 'Si', onPress: () => {
                this.props.logOut({ token: this.props.user.token })
                this.props.navigation.navigate('LogIn')
            }
        }, { text: 'No' }])
    }
    numberItems = (pos, name) => {
        let number = 0

        if (this.props.arrayBaseTravel[pos][name].length > 0) {
            this.props.arrayBaseTravel[pos][name].map((i) => {
                let subName = Object.keys(i)[0]
                number = number + i[subName].length
            })

        }

        return number
    }

    numberProducts = (pos, name) => {
        return this.state.arrayTabProduct[pos][name].length
    }


    /////////////////////////
    cancelarModal = () => {
        this.setState({visibleModal:false})
    }
    eliminarPendiente = (item) => {
        Alert.alert("Cuidado", "¿Está seguro de eliminar solicitud?", [{
            text: 'Aceptar', onPress: () => {
                this.props.deleteRequest({RequestId:item.RequestId,token:this.props.user.token , callback:()=>{
                    this._updateData(null)
                    this.setState({visibleModal:false})
                }})
            }
        }, { text: 'Cancelar' }])
    }

    EditarPendiente = (item) =>{

        Alert.alert("Alerta", "¿Está seguro de editar solicitud?", [{
            text: 'Aceptar', onPress: () => {
                const time = item.DateInitial
                this.props.setInputRequest({prop:'idSelectOrigen',value:item.BaseOrigenId})
                this.props.setInputRequest({prop:'idSelectDestino',value:item.BaseFinalId})
                this.props.setInputRequest({prop:'idSelectProduct',value:item.ProductId})
                this.props.setInputRequest({prop:'idSelectType',value:item.TypeId})
                this.props.setInputRequest({prop:'idSelectProvider',value:item.ProviderId})
                this.props.setInputRequest({prop:'date',value:time.substr(0,10)})
                this.props.setInputRequest({prop:'hour',value:time.substr(11,5)})
                this.props.setInputRequest({prop:'unit',value:'1'})
                this.props.setInputRequest({prop:'observation',value:item.Note})
                this.props.setInputRequest({prop:'RegisterId',value:item.RequestId})
                this.props.setInputRequest({prop:'OrigenName',value:item.BaseOrigenName})
                this.props.setInputRequest({prop:'DestinoName',value:item.BaseFinalName})

                this.props.filterProvider({idProduct:item.ProductId, providers:this.props.proveedor})
                this.setState({visibleModal:false},()=>{
                    this.props.navigation.navigate('Request')
                })
            }
        }, { text: 'Cancelar' }])


        
        
    }

    render() {
        if (this.props.token == null) {
            return null
        }
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                <StatusBar barStyle={this.state.headerColor} />
                <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, height: 44, alignItems: 'center', justifyContent: 'center' }}>
                    <Image
                        source={{ uri: this.props.user.PictureUrlLog }}
                        style={{ width: 170, height: 170, resizeMode: 'contain' }} />

                    <View style={{ position: 'absolute', right: 20, height: 44, justifyContent: 'center' }}>
                        <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => { this._getOut() }}>
                            <Image
                                source={ic_get_out}
                                style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                            <Text style={{ color: 'white', fontSize: 12 }}>Salir</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Text style={{ marginStart: 20, marginTop: 20, fontWeight: 'bold' }}>Viajes</Text>
                <View style={{ alignSelf: 'stretch', height: 40, marginTop: 5, backgroundColor: 'white' }}>
                    <FlatList
                        style={{ flex: 1 }}
                        data={this.props.arrayBaseTravel}
                        horizontal
                        keyExtractor={(i, index) => { return index.toString() }}
                        renderItem={({ item, index }) => {
                            const backgroundColor = (this.state.selectedTab == index) ? lightBlue : 'white'
                            const textColor = (this.state.selectedTab == index) ? 'white' : 'lightgray'
                            const borderColor = (this.state.selectedTab == index) ? lighterBlue : 'lightgray'
                            const fontWeight = (this.state.selectedTab == index) ? 'bold' : 'normal'
                            this.nameTab[index] = `${Object.keys(item)[0]}`
                            return (
                                <View style={{ justifyContent: 'center', marginStart: 10 }}>
                                    <TouchableOpacity style={{ alignContent: 'center', justifyContent: 'center', backgroundColor: backgroundColor, paddingStart: 15, paddingEnd: 15, height: 30, borderRadius: 15, borderWidth: 1, borderColor: borderColor }}
                                        onPress={() => {
                                            this.setState({ selectedTab: index, selectedTabProduct: -1, selectedNameTab: this.nameTab[index], arrayItemProduct: [] }, () => {


                                                this.setState({ arrayTabProduct: this.props.arrayBaseTravel[this.state.selectedTab][this.state.selectedNameTab] }, () => {
                                                    if (this.state.arrayTabProduct.length > 0) {

                                                        this.setState({ selectedTabProduct: 0, selectProductNameTab: `${Object.keys(this.state.arrayTabProduct[0])[0]}` }, () => {
                                                            this.setState({ arrayItemProduct: this.props.arrayBaseTravel[this.state.selectedTab][this.state.selectedNameTab][this.state.selectedTabProduct][this.state.selectProductNameTab] })
                                                        })
                                                    }
                                                })
                                            })


                                        }}>
                                        <Text style={{ color: textColor, fontWeight: fontWeight }}>{this.numberItems(index, Object.keys(item)[0])} {Object.keys(item)[0]}</Text>
                                    </TouchableOpacity>
                                </View>
                            )
                        }} />
                </View>
                <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray', marginTop: 10 }} />
                <View style={{ alignSelf: 'stretch', height: 40, marginTop: 10, backgroundColor: 'white' }}>
                    {
                        (this.state.arrayTabProduct.length > 0) ?
                            <FlatList
                                style={{ flex: 1 }}
                                data={this.state.arrayTabProduct}
                                horizontal
                                keyExtractor={(i, index) => { return index.toString() }}
                                renderItem={({ item, index }) => {
                                    this.nameProductTab[index] = `${Object.keys(item)[0]}`
                                    const backgroundColor = (this.state.selectedTabProduct == index) ? lightGreen : 'white'
                                    const textColor = (this.state.selectedTabProduct == index) ? green : 'lightgray'
                                    const borderColor = (this.state.selectedTabProduct == index) ? lighterBlue : 'lightgray'
                                    const fontWeight = (this.state.selectedTabProduct == index) ? 'bold' : 'normal'
                                    return (
                                        <View style={{ justifyContent: 'center', marginStart: 10 }}>
                                            <TouchableOpacity style={{ alignContent: 'center', justifyContent: 'center', backgroundColor: backgroundColor, paddingStart: 15, paddingEnd: 15, height: 30, borderRadius: 15, borderWidth: 1, borderColor: borderColor }}
                                                onPress={() => {
                                                    this.setState({ selectedTabProduct: index, selectProductNameTab: this.nameProductTab[index] }, () => {
                                                        this.setState({ arrayItemProduct: this.props.arrayBaseTravel[this.state.selectedTab][this.state.selectedNameTab][this.state.selectedTabProduct][this.state.selectProductNameTab] })
                                                    })

                                                }}>
                                                <Text style={{ color: textColor, fontWeight: fontWeight }}>{this.numberProducts(index, Object.keys(item)[0])} {Object.keys(item)[0]}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    )
                                }} />
                            : null
                    }
                </View>
                <View style={{ flex: 1 }}>
                    {
                        (this.state.arrayItemProduct.length > 0 || this.state.selectedTabProduct == -1) ?
                            <FlatList
                                style={{ flex: 1 }}
                                data={this.state.arrayItemProduct}
                                keyExtractor={(i, index) => { return index.toString() }}
                                renderItem={({ item }) => {
                                    return (
                                        (this.props.proTab != this.state.selectedNameTab && this.state.selectedNameTab != "Programados") ?
                                            <View style={{ height: 100, marginLeft: 10, marginTop: 5, width: Dimensions.get('window').width - 20 }}>
                                                <View
                                                    style={{ flex: 1, flexDirection: "row", borderWidth: 1, borderColor: 'lightgray', borderRadius: 5, height: 120 }}>
                                                    <View style={{ borderTopLeftRadius: 5, borderBottomLeftRadius: 5, paddingStart: 10, paddingEnd: 10, backgroundColor: 'white', justifyContent: 'center', width: '45%', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                                                        {/* <View style={{ height: '70%', justifyContent: 'space-around', alignItems: 'center', width: '35%' }}>
                                                    <View style={{backgroundColor: lightBlue, width: 45, height: 45,alignItems:'center', justifyContent:'center', borderRadius:100}}>
                                                        <Text style={{ textAlign: 'center', color: 'white', fontWeight: 'bold',  textAlignVertical: 'center', fontSize: 16 }}>HP</Text>
                                                    </View>
                                                </View> */}
                                                        <View style={{ height: '70%', alignItems: 'center', borderRightColor: '#CCC', borderRightWidth: 1, width: '100%' }}>
                                                            <View style={{ width: '100%', flexDirection: 'row' }}>
                                                                <Image source={ic_truck_front} style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue, marginRight: 10 }} />
                                                                <Text style={{ color: 'black', fontWeight: 'bold' }}>Placa:</Text>
                                                            </View>

                                                            <Text style={{ color: 'black', marginTop: 10, fontSize: 16 }}>{item.Plate}</Text>

                                                        </View>
                                                    </View>
                                                    <View style={{ padding: 10, flex: 1, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5, width: '55%' }}>
                                                        <View style={{ height: '75%', justifyContent: 'space-evenly' }}>

                                                            <Text style={{ fontWeight: 'bold', fontSize: 14 }}>{(this.state.selectedNameTab == "En Curso") ? 'Tiempo a Destino' : 'Tiempo en Planta'}:</Text>


                                                            <View style={{ backgroundColor: green, width: 100, textAlign: 'center', borderRadius: 10, height: 20 }}>
                                                                <Text style={{ color: 'white', textAlign: 'center', fontSize: 14, fontWeight: 'bold', textAlignVertical: 'center', alignSelf: 'center' }}>{item.Time}</Text>
                                                            </View>
                                                        </View>
                                                        <View style={{ alignItems: 'flex-end' }}>
                                                            <Text style={{ fontWeight: 'bold', color: lightBlue, fontSize: 15, paddingEnd: 10, marginBottom: 10 }}>{item.LoadUnload}</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                            :
                                            (this.props.user.RoleId == TRANSPORT_ROLE_ID)?

                                            <TouchableOpacity 
                                            onPress={()=> (this.props.proTab == this.state.selectedNameTab)?this.setState({visibleModal:true,itemModal:item}):null }
                                            style={{ padding: 5, flex: 1, height: 100 }}>
                                                <View
                                                    style={{ flex: 1, flexDirection: "row", borderWidth: 1, borderColor: 'lightgray', borderRadius: 5 }}>
                                                    <View style={{ backgroundColor: lightBlue, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, paddingStart: 10, paddingEnd: 10, justifyContent: 'center' }}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Text style={{ color: 'white', flex: 1, fontWeight: 'bold', fontSize: 16 }}>
                                                                {Capitalize(item.dayLetter)}
                                                            </Text>
                                                            <Image
                                                                source={ic_calendar}
                                                                style={{ width: 15, height: 15, resizeMode: 'contain' }} />
                                                        </View>
                                                        <Text style={{ color: 'white', marginTop: 10 }}>{item.dayNumber + " - " + item.hours}</Text>
                                                    </View>
                                                    <View style={{ padding: 10, flex: 1, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5 }}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: 'red' }} source={ic_location} />
                                                            <Text style={{ marginStart: 10 }}>{item.BaseOrigenName}</Text>
                                                        </View>
                                                        <View style={{ flex: 1 }}>
                                                            <Image style={{ width: 20, flex: 1, resizeMode: 'contain' }} source={ic_vertical_line} />
                                                        </View>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_location} />
                                                            <Text style={{ marginStart: 10 }}>{item.BaseFinalName}</Text>
                                                        </View>
                                                        <View style={{ alignSelf: 'stretch', flexDirection: 'row',marginTop:5}}>
                                                            <Image style={{ width: 20, height: 20, resizeMode: 'contain' }} source={ic_buildings} />
                                                            <Text style={{ marginStart: 10, fontFamily: "ProductSans-Regular" }}><Text style={{ fontWeight: 'bold', fontFamily: "ProductSans-Regular" }}>Empresa:</Text>{`${item.Name}`}</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </TouchableOpacity>

                                            :
                                            <View style={{ padding: 5, flex: 1, height: 100 }}>
                                                <View
                                                    style={{ flex: 1, flexDirection: "row", borderWidth: 1, borderColor: 'lightgray', borderRadius: 5 }}>
                                                    <View style={{ backgroundColor: lightBlue, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, paddingStart: 10, paddingEnd: 10, justifyContent: 'center' }}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Text style={{ color: 'white', flex: 1, fontWeight: 'bold', fontSize: 16 }}>
                                                                {Capitalize(item.dayLetter)}
                                                            </Text>
                                                            <Image
                                                                source={ic_calendar}
                                                                style={{ width: 15, height: 15, resizeMode: 'contain' }} />
                                                        </View>
                                                        <Text style={{ color: 'white', marginTop: 10 }}>{item.dayNumber + " - " + item.hours}</Text>
                                                    </View>
                                                    <View style={{ padding: 10, flex: 1, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5 }}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: 'red' }} source={ic_location} />
                                                            <Text style={{ marginStart: 10 }}>{item.BaseOrigenName}</Text>
                                                        </View>
                                                        <View style={{ flex: 1 }}>
                                                            <Image style={{ width: 20, flex: 1, resizeMode: 'contain' }} source={ic_vertical_line} />
                                                        </View>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_location} />
                                                            <Text style={{ marginStart: 10 }}>{item.BaseFinalName}</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                    )

                                }}
                            />
                            : <Text style={{ textAlign: 'center', marginTop: 20 }}>No existe ningún viaje</Text>
                    }
                </View>
                <Loader isVisible={this.state.loading} />
                <Modal isVisible={this.state.visibleModal} >
                    {/* CABECERA */}
                    <View style={{width:'100%',height:'20%',backgroundColor:'#52B6DC',borderTopLeftRadius:10,borderTopRightRadius:10,padding:20}}>
                        <View style={{flexDirection:'row',marginBottom:10}}>
                            <Image source={ic_calendar} style={{ width:20,height:20,resizeMode:'contain', marginRight:20}} />
                            <Text style={{color:'white',fontFamily:'ProductSans-Regular',fontSize:18}}>{(this.state.itemModal) ? `${this.state.itemModal.dayLetter} ` :null}</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <Image source={ic_clock_2} style={{ width:20,height:20,resizeMode:'contain', marginRight:20, tintColor:'white'}} />
                            <Text style={{color:'white',fontFamily:'ProductSans-Regular',fontSize:18}}>{(this.state.itemModal) ? `${this.state.itemModal.hours} ` :null}</Text>
                        </View>
                    </View>
                    {/* CUERPO */}
                    <View style={{width:'100%',height:'60%',backgroundColor:'white'}}>
                        <View style={{ alignSelf: 'stretch', padding: 20 }}>

                            <View style={{ flexDirection: 'row' }}>
                                <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: red }} source={ic_location} />
                                <Text style={{ marginStart: 10,fontSize:18,fontWeight: 'bold', fontFamily: "ProductSans-Regular" }}>Origen:</Text>
                                <Text style={{ marginStart: 10,fontWeight: 'bold',fontSize:18, fontFamily: "ProductSans-Regular",color: 'gray' }}>{(this.state.itemModal) ? `${this.state.itemModal.BaseOrigenName} ` :null}</Text>
                            </View>

                            <View style={{ flexDirection: 'row' }}>
                                <Image style={{ width: 20, height: 40, resizeMode: 'contain' }} source={ic_vertical_line} />
                            </View>

                            <View style={{ flexDirection: 'row' ,marginBottom:15}}>
                                <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_location} />
                                <Text style={{ marginStart: 10,fontSize:18,fontWeight: 'bold', fontFamily: "ProductSans-Regular" }}>Destino:</Text>
                                <Text style={{ marginStart: 10,fontWeight: 'bold',fontSize:18, fontFamily: "ProductSans-Regular",color: 'gray' }}>{(this.state.itemModal) ? `${this.state.itemModal.BaseFinalName} ` :null}</Text>
                            </View>

                            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />

                            <View style={{ alignSelf: 'stretch', flexDirection: 'row',marginTop:15,marginBottom:15 }}>
                                <Image style={{ width: 20, height: 20, resizeMode: 'contain' }} source={ic_buildings} />
                                <Text style={{ marginStart: 10,fontSize:18, fontFamily: "ProductSans-Regular" }}><Text style={{ fontWeight: 'bold', fontFamily: "ProductSans-Regular" }}>Empresa:</Text>{(this.state.itemModal) ? `${this.state.itemModal.Name} ` :null}</Text>
                            </View>
                        </View>
                    </View>
                    {/* BOTONES */}
                    <View style={{width:'100%',height:'20%',backgroundColor:'white',justifyContent:'center'}}>
                            <View style={{flexDirection:'row',width:'100%', justifyContent:'space-around'}}>
                                <TouchableOpacity
                                onPress={()=>this.eliminarPendiente(this.state.itemModal)}
                                style={{width:'30%', height:40, backgroundColor:'#EB6560', borderRadius:15, alignItems:'center',justifyContent:'center'}}
                                >
                                    <Text style={{color:'white',fontWeight:'bold',fontFamily:"ProductSans-Regular"}}>Eliminar</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                onPress={()=>this.EditarPendiente(this.state.itemModal)}
                                style={{width:'30%', height:40, backgroundColor:'#52B6DC', borderRadius:15, alignItems:'center',justifyContent:'center'}}
                                >
                                    <Text style={{color:'white',fontWeight:'bold',fontFamily:"ProductSans-Regular"}}>Editar</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                onPress={()=>this.cancelarModal()}
                                style={{width:'30%', height:40, backgroundColor:'#CCCCCC', borderRadius:15, alignItems:'center',justifyContent:'center'}}
                                >
                                    <Text style={{color:'white',fontWeight:'bold',fontFamily:"ProductSans-Regular"}}>Cancelar</Text>
                                </TouchableOpacity>

                            </View> 
                    </View>
                </Modal>
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />

            </SafeAreaView>
        )
    }
}
const mapStateProps = ({ logIn, travel, baseTravel,request }) => {
    if (logIn.user == null) {
        return {}
    }
    const { user } = logIn
    const { token } = user
    const { refreshTravel, watcherId } = travel
    const { arrayBaseTravel, proTab } = baseTravel
    const { 
        idSelectProduct, 
        proveedor
    } = request

    console.log('arrayBaseTravel',arrayBaseTravel)
    console.log('proTab',proTab)
    return { token, refreshTravel, user, arrayBaseTravel, proTab, watcherId , idSelectProduct, proveedor}
}
export default connect(mapStateProps, { logOut, listBaseTravel, setLocationUser, setWatcherId, deleteRequest , setInputRequest, filterProvider})(BaseTravelsScreen);
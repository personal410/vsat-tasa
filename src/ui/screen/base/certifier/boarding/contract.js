import React, { PureComponent } from 'react';
import { Alert, FlatList, Image, SafeAreaView, StatusBar, Text, TouchableOpacity, View, TextInput } from 'react-native';
import { connect } from 'react-redux'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { spaceBlack, lightBlue, red, spaceBlackTrans } from '../../../../../resources/color'

import { ic_hand_truck, ic_arrow_left, ic_contract, ic_box, ic_user_white, ic_exclamation_mark } from '../../../../../resources/icon'


import { getListContractLote, saveShipmentLote } from '../../../../../app/thunk/contract/actions'
import Loader from '../../../../component/loader'

class ContractScreen extends PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            options: [{ key: "1", title: "ENSAQUE" }, { key: "2", title: "REMUESTREO" }, { key: "3", title: "EMBARQUE" }],
            headerColor: 'light-content',
            textInputMin: [],
            textInputMax: [],
            lotesId: [],
            visibleAlert: false
        }

        this._onSelectOption.bind(this)
        this._goBack.bind(this)


    }
    componentDidMount() {
        this.props.getListContractLote({
            ContractId: this.props.navigation.getParam('Id', 0), token: this.props.user.token, callback: () => {
                let arrLotId = []
                this.props.lotesContract.map(i => arrLotId.push(i.Id))

                this.setState({ lotesId: arrLotId }, () => {
                    console.log('this.state.lotesId', this.state.lotesId)
                })
            }
        })
    }
    _onSelectOption(index) {
        if (index == 0) {
            this.props.navigation.navigate('SackLotList')
        } else if (index == 1) {

        } else if (index == 2) {

        }
    }

    _closeAlert = () => {
        this.setState({ visibleAlert: false })
    }


    _showAlert = () => {
        if (this.state.lotesId.length == this.state.textInputMin.length
            && this.state.lotesId.length == this.state.textInputMax.length) {
            this.setState({ visibleAlert: true })
        } else {
            Alert.alert(
                'Cuidado!',
                'Debe ingresar todos las temperaturas',
                { cancelable: true },
            );
        }
    }


    _onConfirmSave = () => {
        

        this.props.saveShipmentLote({
            ContractId: this.props.navigation.getParam('Id', 0), InputMin: this.state.textInputMin, InputMax: this.state.textInputMax,
            LotesId: this.state.lotesId, token: this.props.user.token,callback:()=>{
                this._goBack()
            }
        })
    }


    _goBack() {
        this.props.navigation.goBack();
    }



    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
                <KeyboardAwareScrollView >
                    <StatusBar barStyle={this.state.headerColor} />
                    <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 50, alignItems: 'center', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
                        <TouchableOpacity style={{ alignItems: 'center', }} onPress={() => { this._goBack() }} >
                            <Image style={{ width: 20, height: 20, resizeMode: 'contain', marginRight: 30, marginLeft: 10 }} source={ic_arrow_left} />
                        </TouchableOpacity>

                        <Text style={{ color: spaceBlack, fontWeight: 'bold', fontSize: 16 }}>Embarque</Text>
                    </View>
                    <View style={{ padding: 20, }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_contract} />
                            <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 10 }}>Contrato</Text>
                        </View>
                        <Text style={{ paddingLeft: 30, paddingTop: 10, fontSize: 18 }}>{this.props.navigation.getParam('Contract', '')}</Text>
                    </View>

                    <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray', marginTop: 10 }} />

                    <View style={{ padding: 20, flex: 1 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_box} />
                            <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 10 }}>Número de lote</Text>
                        </View>
                        <FlatList
                            style={{ flex: 1 }}
                            data={this.props.lotesContract}
                            renderItem={({ item, index }) => {
                                return (
                                    <View
                                        style={{ alignSelf: 'stretch', height: 80, marginTop: 15, backgroundColor: 'white', borderRadius: 5, borderWidth: 0.5, borderColor: '#CCC' }}>

                                        <TouchableOpacity style={{ flex: 1, flexDirection: "row", alignItems: 'center', justifiContent: 'center' }} onPress={() => { this._onSelectOption(item) }} >

                                            <View style={{ width: '60%', justifyContent: 'center', flexDirection: 'row' }} >
                                                <Text style={{ fontSize: 16 }}>{item.Lote}</Text>
                                            </View>
                                            <View style={{ width: '40%', justifyContent: 'space-around', flexDirection: 'row' }} >
                                                <View style={{ alignSelf: 'center' }}>
                                                    <TextInput
                                                        style={{ backgroundColor: '#ECECEC', borderRadius: 5, fontSize: 18, textAlign: 'center', textAlignVertical: 'center', height: 40, borderWidth: 0.3, borderColor: '#B1B1B1', width: 40 }}
                                                        maxLength={2}
                                                        keyboardType={'numeric'}
                                                        onChangeText={minTemp => {
                                                            let { textInputMin } = this.state;
                                                            textInputMin[index] = minTemp * 1;
                                                            this.setState({ textInputMin: textInputMin })
                                                        }}
                                                        value={this.state.textInputMin[index]}

                                                    />
                                                    <Text style={{ fontSize: 12 }}>Mínima</Text>
                                                </View>

                                                <View style={{ alignSelf: 'center' }}>
                                                    <TextInput
                                                        style={{ backgroundColor: '#ECECEC', borderRadius: 5, fontSize: 18, textAlign: 'center', textAlignVertical: 'center', height: 40, borderWidth: 0.3, borderColor: '#B1B1B1', width: 40 }}
                                                        maxLength={2}
                                                        keyboardType={'numeric'}
                                                        onChangeText={maxTemp => {
                                                            let { textInputMax } = this.state;
                                                            textInputMax[index] = maxTemp * 1;
                                                            this.setState({ textInputMax: textInputMax })
                                                        }}
                                                        value={this.state.textInputMax[index]}
                                                    />
                                                    <Text style={{ fontSize: 12 }}>Maxima</Text>
                                                </View>


                                            </View>

                                        </TouchableOpacity>
                                    </View>
                                )
                            }}
                        />

                    </View>

                    <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray', marginTop: 10 }} />

                    <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'space-around' }}>
                        <TouchableOpacity style={{ width: 120, padding: 5, alignItems: 'center', justifiContent: 'center', backgroundColor: '#52B6DC', borderRadius: 15 }}
                            onPress={() => { this._showAlert() }} >
                            <Text style={{ fontSize: 16, color: 'white', fontWeight: 'bold' }}>Guardar</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: 120, padding: 5, alignItems: 'center', justifiContent: 'center', backgroundColor: '#EB6560', borderRadius: 15 }}
                            onPress={() => { this._goBack() }} >
                            <Text style={{ fontSize: 16, color: 'white', fontWeight: 'bold' }}>Cancelar</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>

                {(this.state.visibleAlert) ?
                    <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' }}>
                        <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, padding: 20, flexDirection: "row" }}>
                            <Image
                                source={ic_user_white}
                                style={{ tintColor: lightBlue, width: 20, height: 20, marginEnd: 20, resizeMode: 'contain' }} />
                            <View style={{ flex: 1 }}>
                                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20 }}>¿Está usted seguro?</Text>
                            </View>
                        </View>
                        <View style={{ alignSelf: 'stretch', padding: 20, backgroundColor: 'white' }}>
                            <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
                                <Image source={ic_exclamation_mark} style={{ width: 20, height: 20, marginEnd: 20 }} />
                                <Text>Esta usted seguro de guardar la información registrada</Text>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 30, marginTop: 20, flexDirection: 'row', justifyContent: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => { this._onConfirmSave() }}
                                    style={{ backgroundColor: lightBlue, width: '50%', borderRadius: 15, alignItems: 'center', justifyContent: 'center', marginRight: 20 }}>
                                    <Text style={{ color: 'white' }}>Confirmar</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => { this._closeAlert() }}
                                    style={{ backgroundColor: red, width: '50%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white' }}>Cancelar</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </View>
                    : null}
                    <Loader isVisible={this.props.loading} />
            </SafeAreaView>
        )
    }
}

const mapStateProps = (state) => {
    const { user } = state.logIn
    const { lotesContract,loading } = state.contract
    console.log('loading', loading)
    return { user, lotesContract,loading }
}


export default connect(mapStateProps, { getListContractLote, saveShipmentLote })(ContractScreen);
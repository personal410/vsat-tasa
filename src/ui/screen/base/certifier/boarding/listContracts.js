import React, { PureComponent } from 'react';
import { FlatList, Image, SafeAreaView, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux'

import { spaceBlack, lightBlue } from '../../../../../resources/color'
import { ic_arrow_left, ic_contract } from '../../../../../resources/icon'
import { getListContracts } from '../../../../../app/thunk/contract/actions'

class ListContractsScreen extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            options: [{ key: "1", title: "HPE-19-11-004" }, { key: "2", title: "HPE-19-11-005" }, { key: "3", title: "HPE-19-11-006" }],
            headerColor: 'light-content'
        }
        this._onSelectOption.bind(this)
        this._goBack.bind(this)
    }
    componentDidMount(){
        this.props.getListContracts({token:this.props.user.token})
        this.didFocusListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('dark-content')
        })
    }
    componentWillUnmount(){
        this.didFocusListener.remove()
    }
    _onSelectOption(item) {
        this.props.navigation.navigate('Contract',{
            Id:item.Id,
            Contract:item.Contract
        })
    }

    _goBack() {
        this.props.navigation.goBack();
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 50, alignItems: 'center', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
                    <TouchableOpacity style={{ alignItems: 'center', }} onPress={() => { this._goBack() }} >
                        <Image style={{ width: 20, height: 20, resizeMode: 'contain', marginRight: 30, marginLeft: 10 }} source={ic_arrow_left} />
                    </TouchableOpacity>

                    <Text style={{ color: spaceBlack, fontWeight: 'bold', fontSize: 16 }}>Embarque</Text>
                </View>
                <View style={{ marginLeft: 20, marginTop: 20, height: 40, flexDirection: 'row' }}>
                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_contract} />
                    <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 10 }}>Contrato</Text>
                </View>
                <FlatList
                    style={{ flex: 1, marginTop: 0 }}
                    data={this.props.listContracts}
                    renderItem={({ item, index }) => {
                        return (
                            <View
                                style={{ alignSelf: 'stretch', height: 50, marginTop: 15, marginLeft: 20, marginRight: 20, backgroundColor: lightBlue, borderRadius: 5 }}>
                                <TouchableOpacity style={{ flex: 1, flexDirection: "row", alignItems: 'center', justifiContent: 'center' }} onPress={() => { this._onSelectOption(item) }} >
                                    <Text style={{ flex: 1, color: 'white', fontSize: 18, fontWeight: 'bold', textAlign: 'center', alingSelf: 'center' }}>{item.Contract}</Text>
                                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', marginRight: 10, tintColor: 'white', transform: [{ rotate: '180deg' }] }} source={ic_arrow_left} />
                                </TouchableOpacity>
                            </View>
                        )
                    }}
                />
                
            </SafeAreaView>
        )
    }
}

const mapStateProps = (state) => {
    const { user } = state.logIn
    const { listContracts } = state.contract
    return { user, listContracts }
}
export default connect(mapStateProps, {getListContracts})(ListContractsScreen);
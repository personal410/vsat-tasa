import React, { PureComponent } from 'react';
import { Alert, FlatList, Image, SafeAreaView, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux'

import { ic_get_out, ic_box_opened, ic_resampling, ic_ship_with_cargo } from '../../../../resources/icon'
import { spaceBlack, lightBlue } from '../../../../resources/color'
import { logOut } from '../../../../app/thunk/auth/actions'

class CertifierMenuScreen extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            options: [{ key: "1", title: "ENSAQUE", icon: ic_box_opened }, { key: "2", title: "REMUESTREO", icon: ic_resampling }, { key: "3", title: "EMBARQUE", icon: ic_ship_with_cargo }]
        }
        this._getOut.bind(this)
        this._onSelectOption.bind(this)
    }
    componentDidMount() {
        this.didFocusListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('light-content')
        })
    }
    componentWillUnmount() {
        this.didFocusListener.remove()
    }
    _getOut() {
        Alert.alert("Alerta", "¿Está seguro de cerrar sesión?", [{
            text: 'Si', onPress: () => {
                this.props.logOut({ token: this.props.user.token })
                this.props.navigation.navigate('LogIn')
            }
        }, { text: 'No' }])
    }
    _onSelectOption(index) {
        if (index == 0) {
            this.props.navigation.navigate('SackLotList')

        } else if (index == 1) {
            this.props.navigation.navigate('ResamplingLotList')
        } else if (index == 2) {

            this.props.navigation.navigate('ListContracts')
        }
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: spaceBlack }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, height: 44, alignItems: 'center', justifyContent: 'center' }}>
                        <Image
                            source={{ uri: (this.props.user) ? this.props.user.PictureUrlLog : null }}
                            style={{ width: 150, height: 150, resizeMode: 'contain' }} />
                        <View style={{ position: 'absolute', right: 20, height: 44, justifyContent: 'center' }}>
                            <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => { this._getOut() }}>
                                <Image
                                    source={ic_get_out}
                                    style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                <Text style={{ color: 'white', fontSize: 12 }}>Salir</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <FlatList
                        style={{ flex: 1, paddingTop: 10, backgroundColor: 'white' }}
                        data={this.state.options}
                        renderItem={({ item, index }) => {
                            return (
                                <View
                                    style={{ alignSelf: 'stretch', height: 80, margin: 10, backgroundColor: lightBlue, borderRadius: 5 }}>
                                    <TouchableOpacity style={{ flex: 1, flexDirection: "row", alignItems: 'center', }} onPress={() => { this._onSelectOption(index) }} >
                                        <Image style={{ width: 40, height: 40, resizeMode: 'contain', marginRight: 30, marginLeft: 20, tintColor: 'white' }} source={item.icon} />
                                        <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>{item.title}</Text>
                                    </TouchableOpacity>
                                </View>
                            )
                        }}
                    />
                </SafeAreaView>
            </View>
        )
    }
}


const mapStateProps = (state) => {
    const { user } = state.logIn
    return { user }

}
export default connect(mapStateProps, { logOut })(CertifierMenuScreen);
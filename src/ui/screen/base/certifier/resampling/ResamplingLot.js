import React, { PureComponent } from 'react';
import { Alert, Image, SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux'


import { ic_arrow_left, ic_box, ic_sack_2, ic_thermometer, ic_location } from '../../../../../resources/icon'

import { spaceBlack, lightBlue, almostWhite, red } from '../../../../../resources/color'
import Loader from '../../../../component/loader'
import { showAlert } from '../../../../../app/util'
import { saveResampling } from '../../../../../app/thunk/certifier/actions'

class ResamplingLotScreen extends PureComponent {
    state = {
        loading: false,
        sackCount: "",
        min: "",
        max: ""
    }
    constructor(props){
        super(props)
        this._onBack.bind(this)
        this._onSaveResampling.bind(this)
    }
    _onBack(){
        this.props.navigation.goBack()
    }
    _onSaveResampling(){
        this.setState({ loading: true }, () => {
            const { sackCount, min, max } = this.state
            this.props.saveResampling({ loteId: this.props.currentResamplingLot.Id, sackCount, min, max, callback: (e) => {
                this.setState({ loading: false }, () => {
                    if(e == null){
                        Alert.alert("Remuestreo", "¡Registro Exitoso!\nLote " + this.props.currentResamplingLot.Lote , [{ text: 'OK', onPress: () => {
                            this.props.navigation.goBack()
                        }}])
                    }else{
                        showAlert({ type: 2, msg: e })
                    }
                })
            }})
        })
    }
    render(){
        const { Lote, ZoneName, SectorName, Locker } = this.props.currentResamplingLot
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 44, alignItems: 'center', flexDirection: 'row', paddingStart: 20, paddingEnd: 20 }}>
                        <TouchableOpacity
                            onPress={() => { this._onBack() }}>
                            <Image source={ic_arrow_left} style={{ width: 40, height: 20, resizeMode: 'contain' }} />
                        </TouchableOpacity>
                        <Text style={{ color: spaceBlack, fontSize: 16, fontWeight: 'bold', flex: 1, marginStart: 20 }}>Remuestreo</Text>
                    </View>
                    <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                    <Loader isVisible={this.state.loading} />
                    <KeyboardAwareScrollView style={{ flex: 1, backgroundColor: almostWhite }}>
                        <ScrollView style={{ flex: 1, backgroundColor: almostWhite }}>
                            <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_box} />
                                    <Text style={{ marginStart: 10, fontSize: 15, fontWeight: 'bold' }}>Número de lote</Text>
                                </View>
                                <Text style={{ marginStart: 30, marginTop: 10, fontSize: 18 }}>{Lote}</Text>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                            <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_location} />
                                    <Text style={{ marginStart: 10, fontSize: 15, fontWeight: 'bold' }}>Ubicación</Text>
                                </View>
                                <View style={{ marginStart: 30, alignSelf: 'stretch', marginTop: 10 }}>
                                    <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                                        <Text style={{ flex: 1, fontSize: 15 }}><Text style={{ fontWeight: 'bold' }}>Zona: </Text>{ZoneName}</Text>
                                        <Text style={{ flex: 1, fontSize: 15 }}><Text style={{ fontWeight: 'bold' }}>Sector: </Text>{SectorName}</Text>
                                    </View>
                                    <Text style={{ flex: 1, fontSize: 15, marginTop: 10 }}><Text style={{ fontWeight: 'bold' }}>Nichos: </Text>{Locker.map((i) => { return i.Name }).join(", ")}</Text>
                                </View>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                            <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_sack_2} />

                                    <Text style={{ marginStart: 10, fontSize: 15, fontWeight: 'bold' }}>Número de sacos</Text>
                                    <TextInput 
                                        value={this.state.sackCount}
                                        maxLength={4}
                                        keyboardType={'number-pad'}
                                        style={{backgroundColor: 'lightgray', width: 50, marginStart: 10, borderRadius: 5, borderWidth: 1, borderColor: 'gray', padding: 5 }}
                                        onChangeText={(t) => { this.setState({ sackCount: t }) }} />
                                </View>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                            <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_thermometer} />
                                    <Text style={{ marginStart: 10, fontSize: 15, fontWeight: 'bold' }}>Temperatura</Text>
                                </View>
                                <View 
                                    style={{ backgroundColor: 'white', marginTop: 10, padding: 20, borderRadius: 5, borderWidth: 1, borderColor: 'lightgray', flexDirection: 'row', alignSelf: 'stretch' }}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Mínima</Text>
                                        <TextInput 
                                            value={this.state.min}
                                            maxLength={2}
                                            keyboardType={'numeric'}
                                            style={{backgroundColor: 'lightgray', flex: 1, marginStart: 10, borderRadius: 5, borderWidth: 1, borderColor: 'gray', padding: 5 }}
                                            onChangeText={(t) => { this.setState({ min: t }) }} />
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginStart: 20 }}>
                                        <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Máxima</Text>
                                        <TextInput 
                                            value={this.state.max}
                                            maxLength={2}
                                            keyboardType={'numeric'}
                                            style={{backgroundColor: 'lightgray', flex: 1, marginStart: 10, borderRadius: 5, borderWidth: 1, borderColor: 'gray', padding: 5 }}
                                            onChangeText={(t) => { this.setState({ max: t }) }} />
                                    </View>
                                </View>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                            <View style={{ padding: 20 }}>
                                <View style={{ flex: 1, flexDirection: 'row', }}>
                                    <TouchableOpacity
                                        onPress={() => { this._onSaveResampling() }}
                                        style={{ backgroundColor: lightBlue, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center', height: 35 }}>
                                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Guardar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => { this._onBack() }}
                                        style={{ backgroundColor: red, marginStart: 10, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Cancelar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </KeyboardAwareScrollView>
                    <Loader isVisible={this.state.loading} />
                </SafeAreaView>
            </View>
        )
    }
}
const mapStateProps = ({ certifier }) => {
    const { currentResamplingLot } = certifier
    return { currentResamplingLot }
}
export default connect(mapStateProps, { saveResampling })(ResamplingLotScreen);
import React, { PureComponent } from 'react';
import { FlatList, Image, SafeAreaView, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux'

import { ic_arrow_left, ic_box } from '../../../../../resources/icon'
import { spaceBlack, lightBlue, almostWhite } from '../../../../../resources/color'
import Loader from '../../../../component/loader'
import { getResamplingLots, setCurrentResamplingLot } from '../../../../../app/thunk/certifier/actions'
import { showAlert } from '../../../../../app/util';

class ResamplingLotListScreen extends PureComponent {
    state = {
        lotList: [],
        loading: true
    }
    constructor(props){
        super(props)
        this._onBack.bind(this)
        this._onLotSelected.bind(this)
    }
    componentDidMount(){
        this.didFocusListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('dark-content')
        })
        this.props.getResamplingLots({ callback: (e, data) => {
            if(e == null){
                this.setState({ lotList: data, loading: false })
            }else{
                this.setState({ loading: false })
                showAlert({ msg: e, type: 2 })
            }
        }})
    }
    componentWillUnmount(){
        this.didFocusListener.remove()
    }
    _onBack(){
        this.props.navigation.goBack()
    }
    _onLotSelected(index){
        this.props.setCurrentResamplingLot({ currentLot: this.state.lotList[index], callback: () => {
            this.props.navigation.navigate('ResamplingLot')
        } })
    }
    render(){
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 44, alignItems: 'center', flexDirection: 'row', paddingStart: 20, paddingEnd: 20 }}>
                        <TouchableOpacity
                            onPress={() => { this._onBack() }}>
                            <Image source={ic_arrow_left} style={{ width: 40, height: 20, resizeMode: 'contain' }} />
                        </TouchableOpacity>
                        <Text style={{ color: spaceBlack, fontSize: 22, fontWeight: 'bold', flex: 1, marginStart: 20 }}>Remuestreo</Text>
                    </View>
                    <View style={{ flex: 1, backgroundColor: almostWhite, padding: 20 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_box} />
                            <Text style={{ marginStart: 10, fontSize: 15, fontWeight: 'bold' }}>Lotes</Text>
                        </View>
                        <FlatList 
                            data={this.state.lotList}
                            style={{ flex: 1 }}
                            keyExtractor={({Id}) => { return Id.toString() }}
                            renderItem={({ item, index }) => {
                                return (
                                    <TouchableOpacity 
                                        style={{ alignSelf: 'stretch', flexDirection: 'row', alignItems: 'center' , height: 100, borderRadius: 10, borderWidth: 1, borderColor: 'lightgray', backgroundColor: 'white', marginVertical: 10, flexDirection: 'row' }}
                                        onPress={() => this._onLotSelected(index) }>
                                        <View style={{ flex: 5, alignSelf: 'stretch', backgroundColor: lightBlue, borderTopLeftRadius: 10, borderBottomLeftRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 15 }}>{item.Lote}</Text>
                                        </View>
                                        <View style={{ flex: 7, alignSelf: 'stretch', padding: 15, flexDirection: 'row', alignItems: 'center' }}>
                                            <View>
                                                <Text style={{ fontWeight: 'bold' }}>Zona:</Text>
                                                <Text style={{ fontWeight: 'bold' }}>Sector:</Text>
                                                <Text style={{ fontWeight: 'bold' }}>Nicho:</Text>
                                            </View>
                                            <View style={{ marginStart: 10 }}>
                                                <Text>{item.ZoneName}</Text>
                                                <Text>{item.SectorName}</Text>
                                                <Text>{item.Locker.map((i) => { return i.Name }).join(", ")}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }} />
                    </View>
                    <Loader isVisible={this.state.loading} />
                </SafeAreaView>
            </View>
        )
    }
}
const mapStateProps = () => {
    return { }
}
export default connect(mapStateProps, { getResamplingLots, setCurrentResamplingLot })(ResamplingLotListScreen);
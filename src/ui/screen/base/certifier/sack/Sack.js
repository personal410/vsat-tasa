import React, { PureComponent } from 'react';
import { Alert, FlatList, Image, SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux'


import { ic_arrow_left, ic_box, ic_sack_2, ic_truck_front, ic_thermometer, ic_trailer } from '../../../../../resources/icon'

import { spaceBlack, lightBlue, almostWhite, red } from '../../../../../resources/color'
import { getUnitInWay, saveSack } from '../../../../../app/thunk/certifier/actions'
import Loader from '../../../../component/loader'
import DropDown from '../../../../component/dropDown'
import { showAlert } from '../../../../../app/util'

class SackScreen extends PureComponent {
    state = {
        loading: true,
        sackCount: "",
        truckPlate: "Seleccione",
        platformPlate: "",
        trucks: [],
        currentIdTruck: -1,
        visibleSelect: false,
        temperatures: [],
        comments: ""
    }
    constructor(props){
        super(props)
        this._onBack.bind(this)
        this._onSelectTruck.bind(this)
        this._onSaveSack.bind(this)
    }
    componentDidMount(){
        this.props.getUnitInWay({ callback: (e, data) => {
            if(e == null){
                this.setState({ trucks: data, loading: false })
            }else{
                this.setState({ loading: false })
                showAlert({ msg: e, type: 2 })
            }
        }})
    }
    _onBack(){
        this.props.navigation.goBack()
    }
    _onSelectTruck(id, title){
        const { trucks } = this.state
        const idx = trucks.findIndex(item => { return item.id == id })
        const truck = trucks[idx]
        this.setState({ currentIdTruck: id, truckPlate: title, platformPlate: truck.TrailerName, visibleSelect: false })
    }
    _onSaveSack(){
        this.setState({ loading: true }, () => {
            const { sackCount, comments, temperatures, currentIdTruck } = this.state
            this.props.saveSack({ loteId: this.props.currentSackLot.Id, sackCount, comments, temperatures, currentIdTruck, callback: (e) => {
                this.setState({ loading: false }, () => {
                    if(e == null){
                        Alert.alert("Ensaque", "¡Registro Exitoso!\nLote " + this.props.currentSackLot.Lote , [{ text: 'OK', onPress: () => {
                            this.props.navigation.goBack()
                        }}])
                    }else{
                        showAlert({ type: 2, msg: e })
                    }
                })
            }})
        })
    }
    render(){
        const { sackCount, truckPlate, platformPlate, temperatures, comments } = this.state
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 44, alignItems: 'center', flexDirection: 'row', paddingStart: 20, paddingEnd: 20 }}>
                        <TouchableOpacity
                            onPress={() => { this._onBack() }}>
                            <Image source={ic_arrow_left} style={{ width: 40, height: 20, resizeMode: 'contain' }} />
                        </TouchableOpacity>
                        <Text style={{ color: spaceBlack, fontSize: 16, fontWeight: 'bold', flex: 1, marginStart: 20 }}>Ensaque</Text>
                    </View>
                    <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                    <Loader isVisible={this.state.loading} />
                    <KeyboardAwareScrollView style={{ flex: 1, backgroundColor: almostWhite }}>
                        <ScrollView style={{ flex: 1, backgroundColor: almostWhite }}>
                            <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_box} />
                                    <Text style={{ marginStart: 10, fontSize: 15, fontWeight: 'bold' }}>Número de lote</Text>
                                </View>
                                <Text style={{ marginStart: 30, marginTop: 10, fontSize: 18 }}>{this.props.currentSackLot.Lote}</Text>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                            <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_sack_2} />

                                    <Text style={{ marginStart: 10, width: 130, fontSize: 15, fontWeight: 'bold' }}>Número de sacos</Text>
                                    <TextInput 
                                        value={sackCount}
                                        maxLength={4}
                                        keyboardType={'numeric'}
                                        style={{backgroundColor: 'lightgray', width: 50, marginStart: 10, borderRadius: 5, borderWidth: 1, borderColor: 'gray', padding: 5 }}
                                        onChangeText={(t) => {
                                            let v = parseInt(t)
                                            let sackCount = ""
                                            let currentTemperatures = this.state.temperatures
                                            let newTemperatures = []
                                            if(!isNaN(v)){
                                                if(v > 1000){
                                                    v = 1000
                                                }
                                                sackCount = v.toString()
                                                const cant = v / 200
                                                for(i = 0; i < cant; i++){
                                                    let temperature = ""
                                                    if(i < currentTemperatures.length){
                                                        temperature = currentTemperatures[i]
                                                    }
                                                    newTemperatures.push(temperature)
                                                }
                                            }
                                            this.setState({ sackCount, temperatures: newTemperatures })
                                        }} />
                                </View>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                            <View style={{ alignSelf: 'stretch', padding: 20, flex: 0 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'stretch', flex: 0 }}>
                                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_truck_front} />
                                    <Text style={{ marginStart: 10, width: 130, fontSize: 15, fontWeight: 'bold' }}>Placa camión</Text>
                                    <TouchableOpacity 
                                        style={{backgroundColor: 'lightgray', flex: 1, marginStart: 10, borderRadius: 5, borderWidth: 1, borderColor: 'gray', padding: 5 }}
                                        onPress={() => { this.setState({ visibleSelect: true }) }}>
                                        <Text>{truckPlate}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                            <View style={{ alignSelf: 'stretch', padding: 20, flex: 0 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'stretch', flex: 0 }}>
                                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_trailer} />
                                    <Text style={{ marginStart: 10, width: 130, fontSize: 15, fontWeight: 'bold' }}>Placa plataforma</Text>
                                    <View 
                                        style={{backgroundColor: 'lightgray', flex: 1, marginStart: 10, borderRadius: 5, borderWidth: 1, borderColor: 'gray', padding: 5 }}>
                                        <Text>{platformPlate}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                            <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_thermometer} />
                                    <Text style={{ marginStart: 10, fontSize: 15, fontWeight: 'bold' }}>Temperatura</Text>
                                </View>
                                <View 
                                    style={{ backgroundColor: 'white', marginTop: 10, padding: 10, borderRadius: 5, borderWidth: 1, borderColor: 'lightgray' }}>
                                    <FlatList 
                                        style={{ alignSelf: 'stretch' }}
                                        data={temperatures}
                                        key={temperatures.length}
                                        numColumns={temperatures.length == 0 ? 1 : temperatures.length}
                                        keyExtractor={(_item, index) => { return index.toString() }}
                                        renderItem={({item, index}) => {
                                            return (
                                                <TextInput
                                                    style={{ backgroundColor: '#ECECEC', flex: 1, height: 40, marginLeft: 5, marginRight: 5, borderWidth: 1, borderColor: '#A9A9A9', borderRadius: 4, padding: 0, textAlign: 'center', fontSize: 18, textAlignVertical: 'center' }}
                                                    keyboardType={'numeric'}
                                                    maxLength={2}
                                                    onChangeText={text => { 
                                                        temperatures[index] = text
                                                        this.setState(temperatures)
                                                     }}
                                                    value={item} />
                                            )
                                        }} />
                                </View>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                            <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ marginStart: 30, fontSize: 15, fontWeight: 'bold' }}>Observaciones</Text>
                                </View>
                                <TextInput 
                                    multiline={true}
                                    onChangeText={(text) => { this.setState({ comments: text }) }}
                                    maxLength={100}
                                    placeholder={"Escriba una breve observación"}
                                    style={{ borderColor: 'lightgray', backgroundColor: 'white', borderRadius: 5, borderWidth: 1, height: 100, padding: 5, height: 100, alignSelf: 'stretch', marginTop: 10 }} 
                                    value={comments} />
                                <Text style={{ textAlign: 'right', marginEnd: 10, marginTop: 5, fontWeight: '700', color: lightBlue }}>{`${comments.length}/100`}</Text>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                            <View style={{ padding: 20 }}>
                                <View style={{ flex: 1, flexDirection: 'row', }}>
                                    <TouchableOpacity
                                        onPress={() => { this._onSaveSack() }}
                                        style={{ backgroundColor: lightBlue, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center', height: 35 }}>
                                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Guardar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => { this._onBack() }}
                                        style={{ backgroundColor: red, marginStart: 10, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Cancelar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </KeyboardAwareScrollView>
                    <DropDown
                        isVisible={this.state.visibleSelect}
                        listReason={this.state.trucks}
                        selectReason={this._onSelectTruck.bind(this)}
                        idReason={this.state.currentIdTruck}
                        title={'Seleccione una Unidad'} />
                    <Loader isVisible={this.state.loading} />
                </SafeAreaView>
            </View>
        )
    }
}
const mapStateProps = ({ certifier }) => {
    const { currentSackLot } = certifier

    return { currentSackLot }
}
export default connect(mapStateProps, { getUnitInWay, saveSack })(SackScreen);
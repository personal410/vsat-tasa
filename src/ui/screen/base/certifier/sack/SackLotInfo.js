import React, { PureComponent } from 'react';
import { Alert, Image, Keyboard, SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux'

import { ic_arrow_left, ic_floppy_disk, ic_factory, ic_box_white, ic_arrow_down, ic_triangle_warning, ic_exclamation_mark } from '../../../../../resources/icon'
import { spaceBlack, spaceBlackTrans, green, lightBlue, red, almostWhite } from '../../../../../resources/color'
import DropDown from '../../../../component/dropDown'
import Loader from '../../../../component/loader'
import { getPlants, saveLot } from '../../../../../app/thunk/certifier/actions'
import { showAlert } from '../../../../../app/util'

class SackLotInfoScreen extends PureComponent{
    constructor(props){
        super(props)
        let beginLotTemp = ''
        let codeTemp = ''
        if(this.props.currentSackLot != null){
            const { Lote } = this.props.currentSackLot
            beginLotTemp = Lote.substr(0, 5)
            codeTemp = Lote.substr(5, 5)
        }
        this.state = {
            idPlant: 0,
            beginLot: beginLotTemp,
            plants: [],
            visibleSelect: false,
            code: codeTemp,
            showConfirmationAlert: false,
            loading: true
        }
        this._onBack.bind(this)
        this._onSaveLot.bind(this)
        this._saveLot.bind(this)
        this._onSelectPlant.bind(this)
    }
    componentDidMount(){
        this.props.getPlants({ callback: (e, data) => {
            if(e == null){
                let plants = []
                data.forEach((item) => {
                    item.title = item.Name
                    item.id = item.Id
                    plants.push(item)
                })
                let newState = { plants, loading: false }
                if(this.props.currentSackLot != null){
                    const { CompanyBaseId } = this.props.currentSackLot
                    newState = {...newState, idPlant: CompanyBaseId }
                }
                this.setState(newState)
            }else{
                this.setState({ loading: false })
                showAlert({ msg: e, type: 2 })
            }
        }})
    }
    _onBack(){
        this.props.navigation.goBack()
    }
    _onSaveLot(){
        Keyboard.dismiss()
        this.setState({ showConfirmationAlert: true })
    }
    _saveLot(){
        const { idPlant, beginLot, code } = this.state
        const loteId = this.props.currentSackLot == null ? null : this.props.currentSackLot.Id
        this.setState({ loading: true }, () => {
            this.props.saveLot({ plantId: idPlant, lote: beginLot + code, loteId, callback: (e) => {
                this.setState({ loading: false }, (e) => {
                    if(e == null){
                        Alert.alert("Lote", "Registro exitoso", [{ text: 'OK', onPress: () => {
                            this.props.navigation.goBack()
                        }}])
                    }else{
                        showAlert({ msg: e, type: 2 })
                    }
                })
            }})
        })
    }
    _onSelectPlant(idReason, _title){
        const { plants } = this.state
        const idx = plants.findIndex(item => { return item.Id == idReason })
        this.setState({ idPlant: idReason, beginLot: plants[idx].Alias + new Date().getFullYear().toString().substr(2, 2), visibleSelect: false })
    }
    render(){
        const { idPlant, plants, code, beginLot } = this.state
        const idx = plants.findIndex(item => { return item.Id == idPlant })
        const currentPlant = idx == -1 ? "" : plants[idx].title
        const btnIsEnable = idPlant != 0 && code.length == 5
        const btnTintColor = btnIsEnable ? green : 'gray'
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 44, alignItems: 'center', flexDirection: 'row', paddingStart: 20, paddingEnd: 20 }}>
                        <TouchableOpacity
                            onPress={() => { this._onBack() }}>
                            <Image source={ic_arrow_left} style={{ width: 40, height: 20, resizeMode: 'contain' }} />
                        </TouchableOpacity>
                        <Text style={{ color: spaceBlack, fontSize: 16, fontWeight: 'bold', flex: 1, marginStart: 20 }}>Guardar lote</Text>
                        <TouchableOpacity
                            disabled={!btnIsEnable}
                            onPress={() => { this._onSaveLot() }}>
                            <Image source={ic_floppy_disk} style={{ width: 30, height: 30, marginStart: 20, resizeMode: 'contain', tintColor: btnTintColor }} />
                        </TouchableOpacity>
                    </View>
                    <KeyboardAwareScrollView style={{ flex: 1, backgroundColor: almostWhite }}>
                        <ScrollView style={{ flex: 1, padding: 20, backgroundColor: almostWhite }}>
                            <View style={{ alignSelf: 'stretch', flexDirection: 'row', marginTop: 10 }}>
                                <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_factory} />
                                <Text style={{ marginStart: 10, fontSize: 18, fontWeight: 'bold' }}>Planta</Text>
                            </View>
                            <TouchableOpacity 
                                style={{ height: 50, borderWidth: 1, marginTop: 10, borderColor: 'lightgray', borderRadius: 5, alignSelf: 'stretch', flexDirection: 'row', backgroundColor: 'white', alignItems: 'center' }}
                                onPress={() => this.setState({ visibleSelect: true })}
                            >
                                <View style={{ flex: 1 }}>
                                    <Text style={{ marginLeft: 10, color: 'gray', flex: 0 }}>{currentPlant}</Text>
                                </View>
                                <Image style={{ width: 15, height: 15, marginEnd: 10, resizeMode: 'contain' }} source={ic_arrow_down} />
                            </TouchableOpacity>
                            <View style={{ alignSelf: 'stretch', flexDirection: 'row', marginTop: 10 }}>
                                <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_box_white} />
                                <Text style={{ marginStart: 10, fontSize: 18, fontWeight: 'bold' }}>Número de lote</Text>
                            </View>
                            <View 
                                style={{ height: 50, borderWidth: 1, marginTop: 10, borderColor: 'lightgray', borderRadius: 5, alignSelf: 'stretch', flexDirection: 'row', backgroundColor: 'white', alignItems: 'center' }}
                                onPress={() => this.setState({ visibleSelect: true })}
                            >
                                <View style={{ flex: 1 }}>
                                    <Text style={{ marginLeft: 5, color: spaceBlack, flex: 0 }}>{beginLot}</Text>
                                </View>
                                <TextInput
                                    value={code}
                                    maxLength={5}
                                    keyboardType={'number-pad'}
                                    style={{ flex: 1, backgroundColor: 'lightgray', borderRadius: 5, margin: 5, padding: 5 }}
                                    onChangeText={txt => { this.setState({ code: txt }) } } />
                            </View>
                        </ScrollView>
                    </KeyboardAwareScrollView>
                    <DropDown
                        isVisible={this.state.visibleSelect}
                        listReason={this.state.plants}
                        selectReason={this._onSelectPlant.bind(this)}
                        idReason={this.state.idPlant}
                        title={'Seleccione un opción'} />
                    {!this.state.showConfirmationAlert ? null :
                        <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' }}>
                            <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, padding: 20 }}>
                                <View style={{ flex: 0, alignSelf: 'stretch', flexDirection: "row" }}>
                                    <Image
                                        source={ic_triangle_warning}
                                        style={{ tintColor: red, width: 20, height: 20, marginEnd: 20, resizeMode: 'contain' }} />
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20 }}>No. de lote</Text>
                                </View>
                                <Text style={{ color: 'white', marginTop: 5, fontSize: 25, flex: 0, alignSelf: 'stretch', textAlign: 'center' }}>{beginLot.substr(0, 3) + " " + beginLot.substr(3, 2) + code}</Text>
                            </View>
                            <View style={{ alignSelf: 'stretch', padding: 20, backgroundColor: 'white' }}>
                                <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
                                    <Image source={ic_exclamation_mark} style={{ width: 20, height: 20, marginEnd: 20 }} />
                                    <Text>¿Desea guardar el lote?</Text>
                                </View>
                                <View style={{ alignSelf: 'stretch', height: 30, marginTop: 20, flexDirection: 'row' }}>
                                    <TouchableOpacity
                                        onPress={() => { this._saveLot() }}
                                        style={{ backgroundColor: lightBlue, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white' }}>Guardar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => { this.setState({ showConfirmationAlert: false }) }}
                                        style={{ backgroundColor: red, marginStart: 10, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white' }}>Cancelar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    }
                    <Loader isVisible={this.state.loading} />
                </SafeAreaView>
            </View>
        )
    }
}
const mapStateProps = ({ certifier }) => {
    const { currentSackLot } = certifier
    return { currentSackLot }
}
export default connect(mapStateProps, { getPlants, saveLot })(SackLotInfoScreen);
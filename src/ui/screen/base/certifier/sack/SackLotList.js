import React, { PureComponent } from 'react';
import { Alert, FlatList, Image, SafeAreaView, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux'

import { ic_arrow_left, ic_add, ic_pencil, ic_trash_can, ic_triangle_warning, ic_exclamation_mark } from '../../../../../resources/icon'
import { spaceBlack, spaceBlackTrans, green, lightBlue, red, almostWhite } from '../../../../../resources/color'
import { getSackLots, setCurrentSackLot, deleteLot } from '../../../../../app/thunk/certifier/actions'
import Loader from '../../../../component/loader'
import { showAlert } from '../../../../../app/util'

class SackLotListScreen extends PureComponent {
    state = {
        lotList: [],
        loading: true,
        showDeleteAlert: false,
        currentIdx: -1
    }
    constructor(props){
        super(props)
        this._onBack.bind(this)
        this._onLotSelected.bind(this)
        this._onAddLot.bind(this)
        this._onLotModify.bind(this)
        this._onLotDelete.bind(this)
    }
    componentDidMount(){
        this.didFocusListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('dark-content')
            if(!this.state.loading){
                this.setState({ loading: true })
            }
            this.props.getSackLots({ callback: (e, data) => {
                if(e == null){
                    this.setState({ lotList: data, loading: false })
                }else{
                    this.setState({ loading: false })
                    showAlert({ msg: e, type: 2 })
                }
            }})
        })
    }
    componentWillUnmount(){
        this.didFocusListener.remove()
    }
    _onBack(){
        this.props.navigation.goBack()
    }
    _onAddLot(){
        this.props.setCurrentSackLot({ currentLot: null , callback: () => {
            this.props.navigation.navigate('SackLotInfo')
        }})
    }
    _onLotSelected(index){
        this.props.setCurrentSackLot({ currentLot: this.state.lotList[index], callback: () => {
            this.props.navigation.navigate('Sack')
        }})
    }
    _onLotModify(index){
        this.props.setCurrentSackLot({ currentLot: this.state.lotList[index], callback: () => {
            this.props.navigation.navigate('SackLotInfo')
        }})
    }
    _onLotDelete(){
        let index = this.state.currentIdx
        this.setState({ showDeleteAlert: false, loading: true }, () => {
            this.props.deleteLot({ LoteId: this.state.lotList[index].Id, callback: (e) => {
                this.setState({ loading: false }, () => {
                    if(e == null){
                        Alert.alert("Lote", "Se eliminó correctamente", [{ text: 'OK', onPress: () => {
                            let newLotList = [...this.state.lotList]
                            newLotList.splice(index, 1)
                            this.setState({ lotList: newLotList, currentIdx: -1 })
                        }}])
                    }else{
                        showAlert({ msg: e, type: 2 })
                    }
                })
            }})
        })
    }
    render(){
        let lote = ""
        if(this.state.currentIdx > -1){
            lote = this.state.lotList[this.state.currentIdx].Lote
        }
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 44, alignItems: 'center', flexDirection: 'row', paddingStart: 20, paddingEnd: 20 }}>
                        <TouchableOpacity
                            onPress={() => { this._onBack() }}>
                            <Image source={ic_arrow_left} style={{ width: 40, height: 20, resizeMode: 'contain' }} />
                        </TouchableOpacity>
                        <Text style={{ color: spaceBlack, fontSize: 16, fontWeight: 'bold', flex: 1, marginStart: 20 }}>Lotes</Text>
                        <TouchableOpacity
                            onPress={() => { this._onAddLot() }}>
                            <Image source={ic_add} style={{ width: 30, height: 30, marginStart: 20, resizeMode: 'contain', tintColor: green }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, backgroundColor: almostWhite, padding: 20 }}>
                        <FlatList 
                            data={this.state.lotList}
                            style={{ flex: 1 }}
                            keyExtractor={({Id}) => { return Id.toString() }}
                            renderItem={({ item, index }) => {
                                return (
                                    <TouchableOpacity
                                        style={{ alignSelf: 'stretch', flexDirection: 'row', alignItems: 'center' , height: 60, borderRadius: 10, borderWidth: 1, borderColor: spaceBlack, backgroundColor: 'white', paddingStart: 20, paddingEnd: 20, marginTop: 10, marginBottom: 10 }}
                                        onPress={() => this._onLotSelected(index) }>
                                        <View style={{ flex: 1 }}>
                                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>{item.Lote}</Text>
                                        </View>
                                        <TouchableOpacity 
                                            style={{ width: 40, height: 40, borderRadius: 20, marginStart: 10, padding: 10, backgroundColor: lightBlue }}
                                            onPress={() => this._onLotModify(index) }>
                                            <Image source={ic_pencil} style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                        </TouchableOpacity>
                                        <TouchableOpacity 
                                            style={{ width: 40, height: 40, borderRadius: 20, marginStart: 10, padding: 10, backgroundColor: red }}
                                            onPress={() => this.setState({ showDeleteAlert: true, currentIdx: index }) }>
                                            <Image source={ic_trash_can} style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                        </TouchableOpacity>
                                    </TouchableOpacity>
                                )
                            }} />
                    </View>
                    {!this.state.showDeleteAlert ? null :
                        <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' }}>
                            <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, padding: 20 }}>
                                <View style={{ flex: 0, alignSelf: 'stretch', flexDirection: "row" }}>
                                    <Image
                                        source={ic_triangle_warning}
                                        style={{ tintColor: red, width: 20, height: 20, marginEnd: 20, resizeMode: 'contain' }} />
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20 }}>No. de lote</Text>
                                </View>
                                <Text style={{ color: 'white', marginTop: 5, fontSize: 25, flex: 0, alignSelf: 'stretch', textAlign: 'center' }}>{lote}</Text>
                            </View>
                            <View style={{ alignSelf: 'stretch', padding: 20, backgroundColor: 'white' }}>
                                <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
                                    <Image source={ic_exclamation_mark} style={{ width: 20, height: 20, marginEnd: 20 }} />
                                    <Text>¿Desea eliminar el lote?</Text>
                                </View>
                                <View style={{ alignSelf: 'stretch', height: 30, marginTop: 20, flexDirection: 'row' }}>
                                    <TouchableOpacity
                                        onPress={() => { this._onLotDelete() }}
                                        style={{ backgroundColor: lightBlue, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white' }}>Eliminar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => { this.setState({ showDeleteAlert: false, currentIdx: -1 }) }}
                                        style={{ backgroundColor: red, marginStart: 10, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white' }}>Cancelar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    }
                    <Loader isVisible={this.state.loading} />
                </SafeAreaView>
            </View>
        )
    }
}
const mapStateProps = () => {
    return { }
}
export default connect(mapStateProps, { getSackLots, setCurrentSackLot, deleteLot })(SackLotListScreen);
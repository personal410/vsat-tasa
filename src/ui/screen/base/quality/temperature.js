import React, { PureComponent } from 'react'
import { SafeAreaView, StatusBar, Text, View, FlatList, TouchableOpacity, Image, TextInput } from 'react-native'
import { connect } from 'react-redux'

import { spaceBlack, lightBlue, red, spaceBlackTrans, green } from '../../../../resources/color'
import { ic_box, ic_thermometer, ic_triangle_warning, ic_user_white, ic_location, ic_exclamation_mark, ic_phone } from '../../../../resources/icon'

import { saveFollow, getListFollow } from '../../../../app/thunk/seguimiento/actions'

import Loader from '../../../component/loader'

class TemperatureScreen extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            headerColor: 'dark-content',
            temp1: '',
            temp2: '',
            temp3: '',
            temp4: '',
            temp5: '',
            temp6: '',
            temp7: '',
            temp8: '',
            temp9: '',
            avg: '0',
            textInputs: [],
            visibleAlert: false,
            ActionId: 0,
            DescriptionAction: '',
            Interval: 0,
            loading: false

        }
    }

    componentDidMount() {
        this.didFocusListener = this.props.navigation.addListener('didFocus', () => {
            this.setState({ headerColor: 'dark-content' })
        })

    }
    componentWillUnmount() {
        this.didFocusListener.remove()
    }

    avgTemperature = () => {
        if (this.state.textInputs.length > 0) {
            let sum = this.state.textInputs.reduce(function (a, b) { return a + b; });
            let avg = sum / 9;
            this.setState({ avg: avg })
        }


    }

    acceptSaveTemperature = () => {


        if (this.props.rangeFollow.length > 0) {
            this.props.rangeFollow.map(ran => {
                console.log('ran', ran)

                if (this.state.avg >= ran.Min && this.state.avg <= ran.Max) {
                    if (ran.Id == 1) {
                        this.setState({ ActionId: ran.Id }, () => {
                            this.directSaveTemperature();
                        })
                    } else {
                        this.setState({ visibleAlert: true, ActionId: ran.Id, DescriptionAction: ran.Description, Interval: ran.Interval })
                    }

                }
            })


        }

    }

    directSaveTemperature = () => {
        this.setState({ loading: true }, () => {
            this.props.saveFollow({
                LoteId: this.props.navigation.getParam('id', 0), Temp1: this.state.temp1, Temp2: this.state.temp2, Temp3: this.state.temp3, Temp4: this.state.temp4,
                Temp5: this.state.temp5, Temp6: this.state.temp6, Temp7: this.state.temp7, Temp8: this.state.temp8, Temp9: this.state.temp9, AvgTemp: this.state.avg,
                token: this.props.user.token, Latitude: this.props.latitude, Longitude: this.props.longitude, ActionId: this.state.ActionId, callback: () => {
                    this.props.getListFollow({
                        token: this.props.user.token, callback: () => {
                            this.setState({ loading: false },()=>{
                                this.props.navigation.goBack()
                            })
                            
                        }
                    })
                }
            })

        })

    }
    cancelAlert = () => {
        this.setState({
            visibleAlert: false
        })
    }
    cancelSaveTemperature = () => {
        this.props.navigation.goBack()
    }

    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
                <StatusBar barStyle={this.state.headerColor} />
                <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 60, justifyContent: 'center' }}>
                    <Text style={{ color: spaceBlack, fontSize: 18, fontWeight: 'bold', marginStart: 20, alignSelf: 'stretch' }}>Seguimiento</Text>
                </View>
                <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />

                <View style={{ padding: 20, }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_box} />
                        <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 10 }}>Número de lote</Text>
                    </View>
                    <Text style={{ paddingLeft: 30, paddingTop: 10, fontSize: 18 }}>{this.props.navigation.getParam('lote', '')}</Text>
                </View>
                <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />

                <View style={{ padding: 20, width: '100%' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_thermometer} />
                        <Text style={{ fontWeight: 'bold', fontSize: 18, marginLeft: 10 }}>Temperatura</Text>
                    </View>

                    <View style={{ marginTop: 20, backgroundColor: 'white', width: '100%', height: 140, borderWidth: 0.5, borderColor: '#CCC', borderRadius: 5, justifyContent: 'space-around' }}>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', marginLeft: 10, marginRight: 10, marginTop: 10 }}>
                            <TextInput
                                style={{ backgroundColor: '#ECECEC', borderRadius: 5, fontSize: 18, textAlign: 'center', textAlignVertical: 'center', height: 40, borderWidth: 0.3, borderColor: '#B1B1B1', width: 50 }}
                                maxLength={2}
                                keyboardType={'numeric'}
                                onChangeText={temp1 => {

                                    let { textInputs } = this.state;
                                    textInputs[0] = temp1 * 1;
                                    this.setState({ temp1: temp1, textInputs },()=>{
                                        if(this.state.temp1.length==2){
                                            this.secondTextInput.focus();
                                        }
                                    })


                                }}
                                value={this.state.temp1}
                                onBlur={() => this.avgTemperature()}
                            />
                            <TextInput
                                style={{ backgroundColor: '#ECECEC', borderRadius: 5, fontSize: 18, textAlign: 'center', textAlignVertical: 'center', height: 40, borderWidth: 0.3, borderColor: '#B1B1B1', width: 50 }}
                                maxLength={2}
                                keyboardType={'numeric'}
                                onChangeText={temp2 => {
                                    let { textInputs } = this.state;
                                    textInputs[1] = temp2 * 1;
                                    this.setState({ temp2: temp2, textInputs },()=>{
                                        if(this.state.temp2.length==2){
                                            this.thridTextInput.focus();
                                        }
                                    })
                                }}
                                ref={(input) => { this.secondTextInput = input; }}
                                value={this.state.temp2}
                                onBlur={() => this.avgTemperature()}
                            />
                            <TextInput
                                style={{ backgroundColor: '#ECECEC', borderRadius: 5, fontSize: 18, textAlign: 'center', textAlignVertical: 'center', height: 40, borderWidth: 0.3, borderColor: '#B1B1B1', width: 50 }}
                                maxLength={2}
                                keyboardType={'numeric'}
                                ref={(input) => { this.thridTextInput = input; }}
                                onChangeText={temp3 => {
                                    let { textInputs } = this.state;
                                    textInputs[2] = temp3 * 1;
                                    this.setState({ temp3: temp3 },()=>{
                                        if(this.state.temp3.length==2){
                                            this.fourTextInput.focus();
                                        }
                                    })
                                }}
                                value={this.state.temp3}
                                onBlur={() => this.avgTemperature()}
                            />
                            <TextInput
                                style={{ backgroundColor: '#ECECEC', borderRadius: 5, fontSize: 18, textAlign: 'center', textAlignVertical: 'center', height: 40, borderWidth: 0.3, borderColor: '#B1B1B1', width: 50 }}
                                maxLength={2}
                                keyboardType={'numeric'}
                                ref={(input) => { this.fourTextInput = input; }}
                                onChangeText={temp4 => {
                                    let { textInputs } = this.state;
                                    textInputs[3] = temp4 * 1;
                                    this.setState({ temp4: temp4 },()=>{
                                        if(this.state.temp4.length==2){
                                            this.fiveTextInput.focus();
                                        }
                                    })
                                }}
                                value={this.state.temp4}
                                onBlur={() => this.avgTemperature()}
                            />
                            <TextInput
                                style={{ backgroundColor: '#ECECEC', borderRadius: 5, fontSize: 18, textAlign: 'center', textAlignVertical: 'center', height: 40, borderWidth: 0.3, borderColor: '#B1B1B1', width: 50 }}
                                maxLength={2}
                                keyboardType={'numeric'}
                                ref={(input) => { this.fiveTextInput = input; }}
                                onChangeText={temp5 => {
                                    let { textInputs } = this.state;
                                    textInputs[4] = temp5 * 1;
                                    this.setState({ temp5: temp5 },()=>{
                                        if(this.state.temp5.length==2){
                                            this.sixTextInput.focus();
                                        }
                                    })
                                }}
                                value={this.state.temp5}
                                onBlur={() => this.avgTemperature()}
                            />
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', marginLeft: 10, marginRight: 10, marginBottom: 10 }}>
                            <TextInput
                                style={{ backgroundColor: '#ECECEC', borderRadius: 5, fontSize: 18, textAlign: 'center', textAlignVertical: 'center', height: 40, borderWidth: 0.3, borderColor: '#B1B1B1', width: 50 }}
                                maxLength={2}
                                keyboardType={'numeric'}
                                ref={(input) => { this.sixTextInput = input; }}
                                onChangeText={temp6 => {
                                    let { textInputs } = this.state;
                                    textInputs[5] = temp6 * 1;
                                    this.setState({ temp6: temp6 },()=>{
                                        if(this.state.temp6.length==2){
                                            this.sevenTextInput.focus();
                                        }
                                    })
                                }}
                                value={this.state.temp6}
                                onBlur={() => this.avgTemperature()}
                            />
                            <TextInput
                                style={{ backgroundColor: '#ECECEC', borderRadius: 5, fontSize: 18, textAlign: 'center', textAlignVertical: 'center', height: 40, borderWidth: 0.3, borderColor: '#B1B1B1', width: 50 }}
                                maxLength={2}
                                keyboardType={'numeric'}
                                ref={(input) => { this.sevenTextInput = input; }}
                                onChangeText={temp7 => {
                                    let { textInputs } = this.state;
                                    textInputs[6] = temp7 * 1;
                                    this.setState({ temp7: temp7 },()=>{
                                        if(this.state.temp7.length==2){
                                            this.eightTextInput.focus();
                                        }
                                    })
                                }}
                                value={this.state.temp7}
                                onBlur={() => this.avgTemperature()}
                            />
                            <TextInput
                                style={{ backgroundColor: '#ECECEC', borderRadius: 5, fontSize: 18, textAlign: 'center', textAlignVertical: 'center', height: 40, borderWidth: 0.3, borderColor: '#B1B1B1', width: 50 }}
                                maxLength={2}
                                keyboardType={'numeric'}
                                ref={(input) => { this.eightTextInput = input; }}
                                onChangeText={temp8 => {
                                    let { textInputs } = this.state;
                                    textInputs[7] = temp8 * 1;
                                    this.setState({ temp8: temp8 },()=>{
                                        if(this.state.temp8.length==2){
                                            this.nineTextInput.focus();
                                        }
                                    })
                                }}
                                value={this.state.temp8}
                                onBlur={() => this.avgTemperature()}
                            />
                            <TextInput
                                style={{ backgroundColor: '#ECECEC', borderRadius: 5, fontSize: 18, textAlign: 'center', textAlignVertical: 'center', height: 40, borderWidth: 0.3, borderColor: '#B1B1B1', width: 50 }}
                                maxLength={2}
                                keyboardType={'numeric'}
                                ref={(input) => { this.nineTextInput = input; }}
                                onChangeText={temp9 => {
                                    let { textInputs } = this.state;
                                    textInputs[8] = temp9 * 1;
                                    this.setState({ temp9: temp9 },()=>{
                                        if(this.state.temp9.length==2){
                                            this.nineTextInput.blur();
                                        }
                                    })
                                }}
                                value={this.state.temp9}
                                onBlur={() => this.avgTemperature()}
                            />
                            <TextInput
                                style={{ backgroundColor: '#ECECEC', borderRadius: 5, fontSize: 18, textAlign: 'center', textAlignVertical: 'center', height: 40, borderWidth: 0.3, borderColor: '#B1B1B1', width: 50 }}
                                maxLength={4}
                                editable={false}
                                value={`${this.state.avg}`}
                            />
                        </View>

                    </View>

                </View>



                <View style={{ padding: 20, justifyContent: 'flex-end', flex: 1, flexDirection: 'row', alignItems: 'flex-end' }}>
                    <TouchableOpacity
                        onPress={() => { this.acceptSaveTemperature() }}
                        style={{ backgroundColor: '#52B6DC', flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center', height: 35 }}>
                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Guardar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => { this.cancelSaveTemperature() }}
                        style={{ backgroundColor: '#EB6560', marginStart: 10, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center', height: 35 }}>
                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Cancelar</Text>
                    </TouchableOpacity>
                </View>

                {(this.state.visibleAlert) ?
                    <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' }}>
                        <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, padding: 20, flexDirection: "row" }}>
                            <Image
                                source={ic_triangle_warning}
                                style={{ tintColor: '#EB6560', width: 20, height: 20, marginEnd: 20, resizeMode: 'contain', marginTop: 10 }} />
                            <View style={{ flex: 1 }}>
                                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 24 }}>Este lote requiere:</Text>
                                <Text style={{ color: 'white', marginTop: 5, fontSize: 18 }}>{this.state.DescriptionAction}</Text>
                            </View>
                        </View>
                        <View style={{ alignSelf: 'stretch', padding: 20, backgroundColor: 'white' }}>
                            <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
                                <Image source={ic_exclamation_mark} style={{ width: 20, height: 20, marginEnd: 20 }} />
                                <Text>La proxima revisión es en {this.state.Interval} horas</Text>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 30, marginTop: 20, flexDirection: 'row', justifyContent: 'space-around' }}>
                                <TouchableOpacity
                                    onPress={() => { this.directSaveTemperature() }}
                                    style={{ backgroundColor: '#52B6DC', width: '40%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', fontWeight: 'bold' }}>Confirmar</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => { this.cancelAlert() }}
                                    style={{ backgroundColor: '#EB6560', width: '40%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', fontWeight: 'bold' }}>Cancelar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    : null}


                    <Loader isVisible={this.state.loading} />
            </SafeAreaView>
        )
    }
}

const mapStateProps = (state) => {
    const { user, latitude, longitude } = state.logIn
    const { rangeFollow } = state.seguimiento

    return { user, rangeFollow, latitude, longitude }
}
export default connect(mapStateProps, { saveFollow, getListFollow })(TemperatureScreen);
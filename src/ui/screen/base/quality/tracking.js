import React, { PureComponent } from 'react'
import { SafeAreaView, StatusBar, Text, View, FlatList, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'


import { spaceBlack } from '../../../../resources/color'
import { getListFollow, saveFollow } from '../../../../app/thunk/seguimiento/actions'

import { setLocationUser } from '../../../../app/thunk/auth/actions'

import { setWatcherId } from '../../../../app/thunk/travel/actions'
import Geolocation from 'react-native-geolocation-service'

import _ from 'lodash'



class TrackingScreen extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            headerColor: 'dark-content'
        }
    }

    componentDidMount() {
        Geolocation.clearWatch(this.props.watcherId)
        this.didFocusListener = this.props.navigation.addListener('didFocus', () => {
            this.setState({ headerColor: 'dark-content' })
        })
        this.props.getListFollow({ token: this.props.user.token,callback:()=>{
            
        } })

        const watchOptions = Platform.OS === 'ios' ? { enableHighAccuracy: true, distanceFilter: 1 } : { enableHighAccuracy: true, distanceFilter: 1, interval: 10, fastestInterval: 10 }
        const watcherId = Geolocation.watchPosition((position) => {

            this.props.setLocationUser({ latitude: position.coords.latitude, longitude: position.coords.longitude })

        }, () => {

        }, watchOptions)

        this.props.setWatcherId(watcherId)
    }
    componentWillUnmount() {
        this.didFocusListener.remove()
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
                <StatusBar barStyle={this.state.headerColor} />
                <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 60, justifyContent: 'center' }}>
                    <Text style={{ color: spaceBlack, fontSize: 18, fontWeight: 'bold', marginStart: 20, alignSelf: 'stretch' }}>Seguimiento</Text>
                </View>
                <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />

                <View style={{ padding: 20, flex: 1 }}>
                    <FlatList
                        style={{ flex: 1 }}
                        data={this.props.listFollow}
                        renderItem={({ item, index }) => {
                            return (
                                <TouchableOpacity
                                    style={{ width: '100%', height: 160, backgroundColor: 'white', marginBottom: 10, borderWidth: 0.5, borderColor: '#CCC', borderRadius: 5 }}
                                    onPress={() => {
                                        this.props.navigation.navigate('Temperature', {
                                            id: item.Id,
                                            lote: item.Lote
                                        })
                                    }}
                                >
                                    <View style={{ flex: 1, borderBottomColor: '#CCC', borderBottomWidth: 0.2, flexDirection: 'row', marginLeft: 20, marginRight: 20, alignItems: 'center' }}>
                                        <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 16 }}>Lote</Text>
                                        <Text style={{ color: 'black', fontSize: 18, marginLeft: 15,fontWeight:'bold' }}>{item.Lote}</Text>
                                    </View>

                                    <View style={{ flex: 3, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginLeft: 20, marginRight: 20, }}>
                                        <View style={{ flex: 1,justifyContent:'space-between' }}>
                                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Ubicación</Text>
                                            <Text style={{ color: 'black', fontSize: 16 }}>{`${item.ZoneName}\n${item.SectorName}\nNicho `} {(item.Locker.length > 0) ? item.Locker.map(l => l.Name + ' ') : null}</Text>
                                        </View>

                                        <View style={{ flex: 1,justifyContent:'space-between' }}>

                                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Próxima medición</Text>
                                            <View >
                                                <View
                                                    style={{ backgroundColor: '#52B6DC', padding: 2, paddingLeft: 10, paddingRight: 10, borderRadius: 10 }}
                                                >
                                                    <Text style={{ color: 'white', textAlign: 'center' }}>{item.DayLetterCheckPrev} {item.DayNumberrCheckPrev}</Text>
                                                </View>

                                                <View
                                                    style={{ backgroundColor: '#54B87E', padding: 2, paddingLeft: 10, paddingRight: 10, borderRadius: 10, marginTop: 8, }}
                                                >
                                                    <Text style={{ color: 'white', textAlign: 'center' }}>{item.HoursCheckPrev}</Text>
                                                </View>


                                            </View>

                                        </View>

                                    </View>


                                </TouchableOpacity>
                            )
                        }
                        }

                    />
                </View>
            </SafeAreaView>
        )
    }
}

const mapStateProps = (state) => {
    const { watcherId } = state.travel
    const { user, latitude, longitude } = state.logIn
    const { loading, listFollow } = state.seguimiento
    return { loading, listFollow:_.orderBy(listFollow,['DateCheckPrev'],['asc']), user, watcherId, latitude, longitude }
}

export default connect(mapStateProps, { getListFollow, setLocationUser, setWatcherId })(TrackingScreen);
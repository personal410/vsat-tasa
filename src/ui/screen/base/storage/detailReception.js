import React, { PureComponent } from 'react'
import { SafeAreaView, StatusBar, Text, View, Image, TextInput, FlatList, TouchableOpacity, Alert } from 'react-native'
import { connect } from 'react-redux'

import { spaceBlack, lightBlue, backgroundLeftColor, red } from '../../../../resources/color'
import { ic_truck_front, ic_box, ic_arrow_left, ic_location, ic_arrow_down } from '../../../../resources/icon'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DropDown from '../../../component/dropDown'
import DarkDropDown from '../../../component/darkDropDown'
import { listNichos, saveReception, listLots } from '../../../../app/thunk/reception/actions'

import Loader from '../../../component/loader'

import _ from 'lodash'

class DetailReceptionScreen extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            textInputs: [],
            countInputs: [],
            sacos: '',
            avgInputs: 0,
            visibleDropDown: false,
            typeMeSack: 0,
            typeAtrribute: 0,
            listReason: [],
            listDifferenceReason: [],
            visibleDifference: false,
            idDifferenceReason: 0,
            titleDifferenceReason: '',
            attribute: {},
            titleNichos: '',
            temp1: 0,
            temp2: 0,
            temp3: 0,
            temp4: 0,
            temp5: 0,
            loading: false
        }
    }

    componentDidMount() {
        //Diferencia de sacos 3 cuando ingresa una cantidad mas de lo debida

        this.didFocusListener = this.props.navigation.addListener('didFocus', () => {
            //     this.setState({ headerColor: 'dark-content' })

            let DifferenceArray = [];
            // newArray.push({id:0,title:'Seleccione un motivo'})
            this.props.difference.map(i => DifferenceArray.push({ id: i.Id, title: i.Description }))
            this.setState({ listDifferenceReason: DifferenceArray }, () => {

            })


            this.props.listNichos({
                Zone: this.props.navigation.getParam('ZoneId', 0), Sector: this.props.navigation.getParam('SectorId', 0), token: this.props.user.token, callback: () => {
                    let newArray = [];
                    // newArray.push({id:0,title:'Seleccione un motivo'})
                    this.props.nichos.map(i => newArray.push({ id: i.Id, title: i.Name, isSelected: false }))
                    this.setState({ listReason: newArray })
                }
            })

        })
    }
    componentWillUnmount() {
        //this.didFocusListener.remove()
    }
    cancelDropDown = () => {


        let newArray = [];
        this.props.nichos.map(i => newArray.push({ id: i.Id, title: i.Name, isSelected: false }))

        this.setState({ visibleDropDown: false, listReason: newArray, titleNichos: '' })
    }

    acceptDropDown = () => {


        const listSelected = this.state.listReason.filter(e => e.isSelected)

        this.setState({ visibleDropDown: false, titleNichos: _.toString(_.map(listSelected, 'title')) })
    }

    goBack = () => {
        this.props.navigation.goBack()
    }
    selectDifferenceReason = (id, title) => {
        this.setState({ idDifferenceReason: id, titleDifferenceReason: title, visibleDifference: false, typeMeSack: 1 })
    }
    acceptDifferenceReason = () => {
        this.setState({ visibleDifference: false })
    }
    cancelDifferenceReason = () => {
        this.setState({ visibleDifference: false, titleDifferenceReason: '', idDifferenceReason: 0, sacos: '', textInputs: [] })
    }

    selectReason = (id, title) => {
        const listSelected = this.state.listReason.filter(e => e.isSelected)
    }


    btnSaveReception = () => {

        this.setState({ loading: true }, () => {

            this.props.saveReception({
                LoteId: this.props.navigation.getParam('Id', 0), Temp1: this.state.temp1, Temp2: this.state.temp2,
                Temp3: this.state.temp3, Temp4: this.state.temp4, Temp5: this.state.temp5, AvgTemp: this.state.avgInputs,
                Latitude: this.props.latitude, Longitude: this.props.longitude, NumSack: this.state.sacos,
                DifferenceSakcId: this.state.idDifferenceReason, LockerIds: _.map(this.state.listReason.filter(e => e.isSelected), 'id'),
                //LockerIds: [4],
                ActionId: (this.state.attribute.Id) ? this.state.attribute.Id : 0, token: this.props.user.token, callback: () => {
                    this.props.listLots({
                        Plate: this.props.navigation.getParam('Plate', 0), token: this.props.user.token, callback: () => {
                            this.setState({ loading: true }, () => {
                                this.props.navigation.navigate('Reception')
                            })

                        }
                    })

                }
            })

        })

    }

    selectItem(item) {

        let newArray = this.state.listReason.filter(e => e.id != item.id)
        const newItem = { ...item, isSelected: !item.isSelected }
        newArray.push(newItem)
        this.setState({ listReason: newArray })

    }



    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
                <KeyboardAwareScrollView>
                    <View style={{ width: '100%', backgroundColor: '#FFF', height: 50, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => this.goBack()} style={{ justifyContent: 'center' }}>
                            <Image source={ic_arrow_left} style={{ width: 20, height: 20, resizeMode: 'contain', marginRight: 10, alignSelf: 'center', marginStart: 20 }} />
                        </TouchableOpacity>
                        <Text style={{ color: spaceBlack, fontSize: 20, fontWeight: 'bold', marginStart: 20, textAlignVertical: 'center' }}>Recepción</Text>
                    </View>

                    <View style={{ padding: 20, height: 100 }}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <Image source={ic_box} style={{ width: 20, height: 20, tintColor: lightBlue, resizeMode: 'contain', marginRight: 5 }} /><Text style={{ fontWeight: 'bold', fontSize: 14 }} >Número de lote</Text>
                        </View>
                        <View style={{ flex: 1, }}>
                            <Text style={{ marginStart: 40, fontSize: 16 }}>{this.props.navigation.getParam('Lote', 'NO-LOTE')}</Text>
                        </View>
                    </View>
                    <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />

                    <View style={{ padding: 20, height: 100, justifyContent: 'center' }}>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={ic_box} style={{ width: 20, height: 20, tintColor: lightBlue, resizeMode: 'contain', marginRight: 5 }} />
                            <Text style={{ fontWeight: 'bold', fontSize: 14 }} >Número de sacos</Text>
                            <TextInput
                                style={{ backgroundColor: '#ECECEC', height: '70%', width: 50, marginLeft: 10, marginRight: 10, borderWidth: 1, borderColor: '#A9A9A9', borderRadius: 4, textAlign: 'center', fontSize: 18, textAlignVertical: 'center' }}
                                keyboardType={'numeric'}
                                maxLength={4}
                                onChangeText={text => {

                                    if (text.length == 0) {
                                        this.setState({ countInputs: [] })
                                    } else if (text > 0 && text < 201) {
                                        this.setState({ countInputs: [1] })
                                    } else if (text > 200 && text < 401) {
                                        this.setState({ countInputs: [1, 2] })
                                    } else if (text > 400 && text < 601) {
                                        this.setState({ countInputs: [1, 2, 3] })
                                    } else if (text > 600 && text < 801) {
                                        this.setState({ countInputs: [1, 2, 3, 4] })
                                    } else if (text > 800) {
                                        this.setState({ countInputs: [1, 2, 3, 4, 5] })
                                    }
                                    this.setState({ sacos: text, textInputs: [] })


                                }}
                                onBlur={() => {

                                    if (this.state.sacos > 0 && this.state.sacos < this.props.navigation.getParam('NumSack', 0)) {
                                        this.setState({ visibleDifference: true })
                                    } else if (this.state.sacos == this.props.navigation.getParam('NumSack', 0)) {
                                        this.setState({ idDifferenceReason: 0, titleDifferenceReason: 'OK', visibleDifference: false, typeMeSack: 1 })
                                    } else {
                                        this.setState({ visibleDifference: false, typeMeSack: 0, idDifferenceReason: 0 })

                                        Alert.alert(
                                            'Cuidado !!',
                                            'La cantidad de sacos ingresada es mayor a la reportada',
                                            [
                                                { text: 'Volver a contar', onPress: () => this.setState({ sacos: 0, textInputs: [], countInputs: [] }) },
                                                { text: 'Reportar Problema', onPress: () => this.setState({ idDifferenceReason: 3, visibleDifference: false }) },
                                            ],
                                            { cancelable: false },
                                        );
                                    }

                                    this.setState({ typeAtrribute: 0 })

                                }}
                                value={this.state.sacos}
                            />
                            {(this.state.typeMeSack > 0) ?
                                <View style={{ backgroundColor: (this.state.idDifferenceReason == 1) ? '#DA5F5B' : (this.state.idDifferenceReason == 2) ? '#E89461' : '#54B87E', width: '30%', textAlign: 'center', borderRadius: 5 }} >
                                    <Text style={{ padding: 10, color: 'white', fontWeight: 'bold', textAlign: 'center', fontSize: 12 }}>{this.state.titleDifferenceReason}</Text>

                                </View>

                                : null}
                        </View>
                    </View>
                    <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                    <View style={{ padding: 20, }}>
                        <View style={{ height: 40, flexDirection: 'row' }}>
                            <Image source={ic_box} style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue, marginRight: 5 }} />
                            <Text style={{ fontWeight: 'bold', fontSize: 14 }} >Temperatura</Text>
                        </View>
                        <View 
                        //style={{ height: 90, backgroundColor: 'white', borderRadius: 5, borderWidth: 1, borderColor: '#D9D9D9', alignItems: 'center', justifyContent: 'space-around', flexDirection: 'row', width: '100%' }}
                        style={{ backgroundColor: 'white', marginTop: 10, padding: 10, borderRadius: 5, borderWidth: 1, borderColor: 'lightgray' }}
                        >

                            <FlatList
                                style={{ alignSelf: 'stretch' }}
                                data={this.state.countInputs}
                                key={this.state.countInputs.length}
                                numColumns={this.state.countInputs.length == 0 ? 1 : this.state.countInputs.length}
                                renderItem={({ item, index }) => {

                                    return (
                                        <TextInput
                                            //style={{ backgroundColor: '#ECECEC', marginLeft: 5, marginRight: 5, borderWidth: 1, borderColor: '#A9A9A9', borderRadius: 4, textAlign: 'center', fontSize: 18, textAlignVertical: 'center', marginTop: 15, }}
                                            style={{ backgroundColor: '#ECECEC', flex: 1, height: 40, marginLeft: 5, marginRight: 5, borderWidth: 1, borderColor: '#A9A9A9', borderRadius: 4, padding: 0, textAlign: 'center', fontSize: 18, textAlignVertical: 'center' }}
                                            keyboardType={'numeric'}
                                            maxLength={2}
                                            onChangeText={text => {
                                                let { textInputs } = this.state;
                                                textInputs[index] = text * 1;


                                                this.setState({ textInputs }, () => {


                                                    let sum = this.state.textInputs.reduce(function (a, b) { return a + b; });
                                                    let avg = sum / this.state.countInputs.length;
                                                    this.setState({
                                                        avgInputs: avg,
                                                    }, () => {

                                                    });

                                                });


                                            }}
                                            value={this.state.textInputs[index]}
                                            onBlur={() => {

                                                if (index == 0) {
                                                    this.setState({ temp1: this.state.textInputs[index] })

                                                } else if (index == 1) {
                                                    this.setState({ temp2: this.state.textInputs[index] })
                                                } else if (index == 2) {
                                                    this.setState({ temp3: this.state.textInputs[index] })
                                                } else if (index == 3) {
                                                    this.setState({ temp4: this.state.textInputs[index] })
                                                } else if (index == 4) {
                                                    this.setState({ temp5: this.state.textInputs[index] })
                                                }

                                                if (this.state.textInputs.length == this.state.countInputs.length) {

                                                    this.props.range.map(ran => {

                                                        if (this.state.avgInputs >= ran.Min && this.state.avgInputs <= ran.Max) {
                                                            this.setState({ attribute: ran })

                                                        }
                                                    })
                                                    this.setState({ typeAtrribute: 1 }, () => {

                                                    })
                                                }

                                            }}

                                        />
                                    )
                                }} />

                        </View>
                        {(this.state.typeAtrribute > 0) ?
                            <View style={{ marginTop: 15, backgroundColor: lightBlue, width: 180, justifyContent: 'center', alignItems: 'center', borderRadius: 10 }}>
                                <Text style={{ fontSize: 16, color: 'white' }}><Text style={{ fontWeight: 'bold' }}>Atributo: </Text>{this.state.attribute.Description}</Text>
                            </View>
                            : null}


                    </View>
                    <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />

                    <View style={{ padding: 20 }}>
                        <View style={{ flex: 1, flexDirection: 'row', }}>
                            <Image source={ic_location} style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue, marginRight: 5 }} /><Text style={{ fontWeight: 'bold', fontSize: 14 }} >Ubicación</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <Text style={{ marginStart: 25, fontSize: 16 }}><Text style={{ fontWeight: 'bold' }}>Zona: </Text>{this.props.navigation.getParam('ZoneName', '')}</Text>
                            <Text style={{ marginStart: 40, fontSize: 16 }}><Text style={{ fontWeight: 'bold' }}>Sector: </Text>{this.props.navigation.getParam('SectorName', '')}</Text>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <Text style={{ marginStart: 25, fontSize: 16, fontWeight: 'bold' }}>Nichos: </Text>
                            <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white', alignItems: 'center', height: 40, width: 200, borderWidth: 1, borderColor: '#A9A9A9', borderRadius: 5 }}
                                onPress={() => { this.setState({ visibleDropDown: true }) }}
                            >
                                <Text style={{ color: '#CCC', paddingLeft: 15 }}>{(this.state.titleNichos == '') ? 'Seleccione nichos' : this.state.titleNichos}</Text>
                                <Image source={ic_arrow_down} style={{ width: 15, height: 15, resizeMode: 'contain', marginRight: 15 }} />
                            </TouchableOpacity>

                        </View>

                    </View>

                    <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />

                    <View style={{ padding: 20 }}>
                        <View style={{ flex: 1, flexDirection: 'row', }}>
                            <TouchableOpacity
                                onPress={() => { this.btnSaveReception() }}
                                style={{ backgroundColor: lightBlue, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center', height: 35 }}>
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Guardar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => { this.goBack() }}
                                style={{ backgroundColor: red, marginStart: 10, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Cancelar</Text>
                            </TouchableOpacity>
                        </View>

                    </View>





                </KeyboardAwareScrollView>

                <DropDown isVisible={this.state.visibleDropDown} title={'Nichos Asignados'}
                    listReason={this.state.listReason}
                    selectItem={(item) => this.selectItem(item)}
                    selectReason={(id, title) => null}
                    multiple={true}
                >
                    <View style={{ alignSelf: 'center', backgroundColor: '#FFF', paddingRight: 20, paddingLeft: 20, paddingBottom: 30, flexDirection: "row" }}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <TouchableOpacity
                                onPress={() => { this.acceptDropDown() }}
                                style={{ backgroundColor: lightBlue, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center', height: 35 }}>
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Guardar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => { this.cancelDropDown() }}
                                style={{ backgroundColor: red, marginStart: 10, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Cancelar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </DropDown>

                <DarkDropDown isVisible={this.state.visibleDifference}
                    listReason={this.state.listDifferenceReason}
                    title={'Faltan ' + ((this.props.navigation.getParam('NumSack', 0) * 1) - (this.state.sacos * 1)) + ' sacos'}
                    subtitle={'Seleccione un motivo'}
                    selectReason={(id, title) => this.selectDifferenceReason(id, title)}
                    idReason={this.state.idDifferenceReason}
                    cancelDifferenceReason={() => this.cancelDifferenceReason()}
                    acceptDifferenceReason={() => this.acceptDifferenceReason()}
                />
                <Loader isVisible={this.state.laoding} />
            </SafeAreaView>
        )
    }
}

const mapStateProps = (state) => {
    const { user, latitude, longitude } = state.logIn
    const { nichos, range, difference } = state.reception
    return { user, nichos, range, difference, latitude, longitude }
}
export default connect(mapStateProps, { listNichos, saveReception, listLots })(DetailReceptionScreen);
import React, { PureComponent } from 'react'

import { SafeAreaView, StatusBar, Text, View, Image, TextInput, FlatList, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'

import { spaceBlack, lightBlue, backgroundLeftColor } from '../../../../resources/color'

import { ic_truck_front, ic_box, ic_arrow_left, ic_sync } from '../../../../resources/icon'
import SelectInput from 'react-native-select-input-ios'
import { listPlates, listLots, differenceSacklist } from '../../../../app/thunk/reception/actions'
import Loader from '../../../component/loader'

class ReceptionScreen extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            headerColor: 'dark-content',
            select: [{ value: 0, label: 'Seleccione Placa' }],
            idSelect: 0
        }
    }

    componentDidMount() {

        //this.didFocusListener = this.props.navigation.addListener('didFocus', () => {


        this.setState({ headerColor: 'dark-content' })
        this.props.listPlates({
            token: this.props.user.token, callback: () => {
                let selectArray = [{ value: 0, label: 'Seleccione Placa' }]
                this.props.plates.map(item => {
                    selectArray.push({ value: `${item.Plate}`, label: `${item.Plate}` })
                })
                this.setState({ select: selectArray })

            }
        })
        this.props.differenceSacklist({ token: this.props.user.token })



        //})
    }
    componentWillUnmount() {
        //this.didFocusListener.remove()

    }
    goToDetail = (item) => {
        this.props.navigation.navigate('DetailReception', {
            Id: item.Id,
            Lote: item.Lote,
            NumSack: item.NumSack,
            ZoneId: item.ZoneId,
            ZoneName: item.ZoneName,
            SectorId: item.SectorId,
            SectorName: item.SectorName,
            Plate: this.state.idSelect
        })
    }
    updatePlates = () => {
        this.props.listPlates({
            token: this.props.user.token, callback: () => {
                let selectArray = [{ value: 0, label: 'Seleccione Placa' }]
                this.props.plates.map(item => {
                    selectArray.push({ value: `${item.Plate}`, label: `${item.Plate}` })
                })
                this.setState({ select: selectArray, idSelect: 0 })

            }
        })
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <StatusBar barStyle={this.state.headerColor} />
                <View style={{ alignSelf: 'stretch', backgroundColor: '#FAFAFA', height: 50, justifyContent: 'center' }}>
                    <Text style={{ color: spaceBlack, fontSize: 18, fontWeight: 'bold', marginStart: 20, alignSelf: 'stretch' }}>Recepción</Text>
                </View>
                <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                <View style={{ flex: 1 }}>
                    <View style={{ padding: 20, height: 120 }}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <Image source={ic_truck_front} style={{ width: 20, height: 20, tintColor: lightBlue, marginRight: 20 }} />
                            <Text style={{ fontWeight: 'bold', fontSize: 18 }} >Número de Placa</Text>
                            <TouchableOpacity
                                onPress={() => { this.updatePlates() }}
                                style={{ flex: 1, flexDirection: "row", justifyContent: 'flex-end' }}>
                                <Image source={ic_sync} style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, }}>
                            <SelectInput
                                placeholder={'Número de Placa'}
                                placeholderTextColor={'#CCC'}
                                value={this.state.idSelect}
                                options={this.state.select}
                                labelStyle={{}}
                                style={{ backgroundColor: 'white', borderWidth: 1, borderColor: '#CCC', borderRadius: 5, paddingLeft: 12, height: 40, justifyContent: 'center' }}
                                onSubmitEditing={(v) => {

                                    this.setState({ idSelect: v })
                                    if (v == '0') {

                                    } else {
                                        this.props.listLots({
                                            Plate: v, token: this.props.user.token, callback: () => {

                                            }
                                        })
                                    }


                                }}

                            />
                        </View>
                    </View>
                    <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                    <View style={{ padding: 20, flex: 1 }}>
                        <View style={{ height: 30, flexDirection: 'row' }}>
                            <Image source={ic_box} 
                            style={{ width: 20, height: 20, tintColor: lightBlue, marginRight: 20 ,resizeMode: 'contain'}} />
                            <Text style={{ fontWeight: 'bold', fontSize: 18 }} >Lotes</Text>
                        </View>
                        <FlatList
                            data={this.props.lots}
                            renderItem={({ item }) => {
                                return (
                                    <View style={{ padding: 5, flex: 1, height: 90 }}>
                                        <TouchableOpacity
                                            onPress={() => { this.goToDetail(item) }}
                                            style={{ flex: 1, flexDirection: "row", borderWidth: 1, borderColor: 'lightgray', borderRadius: 5 }}>
                                            <View style={{ backgroundColor: lightBlue, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, paddingStart: 10, paddingEnd: 10, justifyContent: 'center', width: '45%', alignItems: 'center' }}>

                                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlignVertical: 'center', textAlign: 'center' }}>{item.Lote}</Text>

                                            </View>
                                            <View style={{ flex: 1, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5, width: '55%' }}>
                                                <View style={{ alignItems: 'flex-end', height: 15 }}>
                                                    <Image source={ic_arrow_left} style={{ width: 15, height: 15, resizeMode: 'contain', transform: [{ rotate: '180deg' }], marginRight: 10, marginTop: 8 }} />
                                                </View>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <Text style={{ paddingStart: 20, fontWeight: 'bold', fontSize: 14 }}>Zona:</Text><Text style={{ paddingStart: 5 }}>{item.ZoneName}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <Text style={{ paddingStart: 20, fontWeight: 'bold', fontSize: 14 }}>Sector:</Text><Text style={{ paddingStart: 5 }}>{item.SectorName}</Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                )
                            }
                            }
                        />
                    </View>
                </View>
                <Loader isVisible={this.props.loading} />
            </SafeAreaView>
        )
    }
}

const mapStateProps = (state) => {
    const { plates, lots, loading } = state.reception
    const { user } = state.logIn

    return { plates, lots, user, loading }
}
export default connect(mapStateProps, { listPlates, listLots, differenceSacklist })(ReceptionScreen);
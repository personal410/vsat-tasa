import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';
import { AppState, BackHandler, FlatList, Image, SafeAreaView, Text, TouchableOpacity, View, Linking, StatusBar } from 'react-native'
import Geolocation from 'react-native-geolocation-service'

import { spaceBlack, spaceBlackTrans, green, lightBlue, red } from '../../resources/color'
import { ic_location, ic_paper_plane, ic_scale, ic_race_flag, ic_shield, ic_unboxing, ic_green_check, ic_phone, ic_exclamation_mark } from '../../resources/icon'
import Location from '../../../Location'
import { checkActivity, resetInitialService, refreshTavel } from '../../app/thunk/travel/actions'
import { setCurrentPage } from '../../app/thunk/auth/actions'
import Loader from '../component/loader'
import { showAlert } from '../../app/util';

import moment from 'moment'


class CheckListScreen extends Component {
    constructor(props) {
        super(props)
        let count = 0
        for (i = 0; i < this.props.currentTasks.length; i++) {
            if (this.props.currentTasks[i].StatusId == 2) {
                count += 1
            }
        }
        this.state = {
            showRegistrationSuccessfull: false,
            currentTask: count,
            showCurrentTaskAlert: false,
            showNoCurrentTaskAlert: false,
            loading: false,
            numberTasks: this.props.currentTasks.length,
            requiredTask: '',
            showAlert: false
        }
        if (count == this.props.currentTasks.length) {
            this.props.currentTasks.push({ Id: -1 })
        }
        this._onCall.bind(this)
        this._onFinish.bind(this)
        this._onTaskSelected.bind(this)
    }
    componentDidMount() {
        this._navListener = this.props.navigation.addListener('didFocus', () => {
            this.props.setCurrentPage('checklist')
        })
        if (Platform.OS == 'android') {
            BackHandler.addEventListener('hardwareBackPress', this._onBackButtonPressed)
            AppState.addEventListener('change', this._onAppStateChanged)
        }
    }
    componentWillUnmount() {
        if (Platform.OS == 'android') {
            BackHandler.removeEventListener('hardwareBackPress', this._onBackButtonPressed)
            AppState.removeEventListener('change', this._onAppStateChanged)
        }
    }
    _onBackButtonPressed() {
        return true
    }
    _onAppStateChanged(newState) {
        if (newState == 'active') {
            Location.stopService()
        } else {
            Location.startService()
        }
    }

    _onCall() {
        this.setState({ showAlert: true })
    }
    _onConfirmCall() {
        Linking.openURL(`tel:${this.props.currentTravel.Phone}`)
        this.setState({ showAlert: false })
    }
    _onCancelCall() {
        this.setState({ showAlert: false })
    }
    _onAgree(){
        this.setState({ showNoCurrentTaskAlert: false })
    }
    _onConfirm(){
        const beforeTask = this.props.currentTasks[this.state.currentTask - 1];
        const task = this.props.currentTasks[this.state.currentTask]
        const a = moment(beforeTask.DateRegister)
        const b = moment();
        const secondsDiff = b.diff(a, 'seconds')
        if (false) {
            this.setState({ showCurrentTaskAlert: false })
            showAlert({type:2,msg:'Debe esperar al menos un minuto'})
        } else {
            const { latitude, longitude } = this.props.currentPosition
            this.setState({ loading: true }, () => {
                this.props.checkActivity({
                    RequestId: this.props.travelId, ActivityId: task.Id.toString(), Latitude: latitude.toString(), Longitude: longitude.toString(), token: this.props.token, InInitialService: task.InInitialService, callback: (e) => {
                        this.setState({ loading: false }, () => {
                            if (e == null) {
                                let newCurrentTask = this.state.currentTask + 1
                                if (newCurrentTask == this.props.currentTasks.length) {
                                    //this.props.currentTasks.push({Id: -1})
                                }
                                this.setState({ currentTask: newCurrentTask, showCurrentTaskAlert: false })
                            } else {
                                showAlert({ msg: e, type: 2 })
                            }
                            if (task.InInitialService == 3) {
                                this.props.navigation.navigate('Journey')
                            }
                            if (task.InInitialService == 5) {
                                this.props.refreshTavel(true)
                                this.props.resetInitialService(1);
                                this.props.setCurrentPage('travels')
                                this.props.navigation.navigate('Travels')
                            }
                        })
                    }
                })
            })

        }


    }
    _onCancel() {
        this.setState({ showCurrentTaskAlert: false })
    }
    _onFinish() {
        this.setState({ showRegistrationSuccessfull: true })
        Geolocation.clearWatch(this.props.watcherId)
        setTimeout(() => {
            this.props.setCurrentPage('travels')
            this.props.navigation.navigate('Travels')
        }, 2000);
    }
    _onTaskSelected(index) {
        if (index == this.state.currentTask) {
            this.setState({ showCurrentTaskAlert: true })
        } else {

            this.setState({ showNoCurrentTaskAlert: true, requiredTask: this.props.currentTasks[this.state.currentTask].ActivityName })
        }
    }

    goToMap(item) {
        this.props.navigation.goBack()
    }
    render() {
        const { currentTask, showRegistrationSuccessfull, showCurrentTaskAlert } = this.state
        let img = ic_location
        let text = ""
        if (showCurrentTaskAlert) {
            text = this.props.currentTasks[currentTask].ActivityName
        }
        return (
            <Fragment >
                <StatusBar barStyle="light-content" />
                <SafeAreaView style={{ flex: 0, backgroundColor: spaceBlack }} />
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, height: 60, alignItems: 'center', flexDirection: 'row' }}>
                        <Image source={ic_location} style={{ width: 40, height: 20, marginStart: 20, resizeMode: 'contain', tintColor: red }} />
                        <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold', marginStart: 20, flex: 1,fontFamily:"ProductSans-Regular" }}>Registro de Carga</Text>
                        <TouchableOpacity
                            style={{ alignItems: 'center', width: 36, height: 36, marginEnd: 20, justifyContent: 'center', backgroundColor: green, borderRadius: 20, marginStart: 20 }}
                            onPress={() => { this._onCall() }}>
                            <Image
                                source={ic_phone}
                                style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                        </TouchableOpacity>
                    </View>
                    <Text style={{ color: spaceBlack, fontSize: 16, marginStart: 20, fontWeight: 'bold', marginTop: 10,fontFamily:"ProductSans-Regular" }}>{currentTask}/{this.state.numberTasks}</Text>
                    <View style={{ flex: 1, margin: 10, borderColor: 'lightgray', borderRadius: 5, borderWidth: 1 }}>
                        <FlatList
                            style={{ flex: 1 }}
                            data={this.props.currentTasks}
                            keyExtractor={(item) => { return item.Id.toString() }}
                            renderItem={({ item, index }) => {

                                if (item.Id == -1) {
                                    return (
                                        <View style={{ alignSelf: 'stretch', padding: 10, alignItems: 'center' }}>
                                            <TouchableOpacity
                                                onPress={() => { this._onFinish() }}
                                                style={{ backgroundColor: lightBlue, width: '50%', height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={{ color: 'white' }}>Finalizar</Text>
                                            </TouchableOpacity>
                                        </View>
                                    )
                                }
                                const { ActivityName, DateRegister, Icon } = item
                                const completed = index < currentTask
                                const showTopLine = index > 0
                                const showBottomLine = index < this.state.numberTasks - 1
                                const topLineColor = completed ? green : 'lightgray'
                                const bottomLineColor = index < currentTask - 1 ? green : 'lightgray'
                                const timeBackgroundColor = DateRegister == null ? 'white' : lightBlue
                                const day_en = moment(DateRegister).format('d')
                                let day = ''
                                switch (day_en) {
                                    case '1':
                                        day = 'Lunes';
                                        break
                                    case '2':
                                        day = 'Martes';
                                        break
                                    case '3':
                                        day = 'Miercoles';
                                        break
                                    case '4':
                                        day = 'Jueves';
                                        break
                                    case '5':
                                        day = 'Viernes';
                                        break
                                    case '6':
                                        day = 'Sabado';
                                        break
                                    case '7':
                                        day = 'Domingo';
                                        break
                                }

                                let img = ic_location
                                switch (Icon) {
                                    case '1':
                                        img = ic_paper_plane;
                                        break
                                    case '2':
                                        img = ic_race_flag;
                                        break
                                    case '3':
                                        img = ic_shield;
                                        break
                                    case '4':
                                        img = ic_scale;
                                        break
                                    case '5':
                                        img = ic_unboxing;
                                        break
                                }

                                return (
                                    <TouchableOpacity
                                        onPress={index >= currentTask ? () => { this._onTaskSelected(index) } : null}
                                        disabled={index < currentTask}>
                                        <View style={{ alignSelf: 'stretch', height: 80, flexDirection: "row", paddingStart: 20, paddingEnd: 20, alignItems: 'center' }}>
                                            <Image source={img} style={{ width: 30, height: 30, resizeMode: 'contain', tintColor: (!completed) ? '#CCC' : lightBlue }} />
                                            <View style={{ marginStart: 10, flex: 1 }}>
                                                <Text style={{ fontWeight: 'bold', fontSize: 16 ,fontFamily:"ProductSans-Regular"}}>{ActivityName}</Text>
                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                    <View style={{ backgroundColor: timeBackgroundColor, width: (DateRegister == null || DateRegister == 'Invalid date') ? 0 : 80, height: 20, borderRadius: 10, alignItems: 'center', justifyContent: 'center' }}>
                                                        <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>{(DateRegister == null || DateRegister == 'Invalid date') ? "" : moment(DateRegister).format('hh:mm a')}</Text>
                                                    </View>
                                                    <Text style={{ color: spaceBlack, marginStart: 10, textAlignVertical: 'center',fontFamily:"ProductSans-Regular" }}>{(DateRegister == null || DateRegister == 'Invalid date') ? "" : day} {(DateRegister == null || DateRegister == 'Invalid date') ? "" : moment(DateRegister).format('D/M')}</Text>
                                                </View>
                                            </View>
                                            <View>
                                                <View style={{ width: 24, flex: 1 }}>
                                                    {showTopLine ? <View style={{ backgroundColor: topLineColor, width: 1, flex: 1, alignSelf: 'center' }} /> : null}
                                                </View>
                                                <View>
                                                    {completed ?
                                                        <Image source={ic_green_check} style={{ width: 24, height: 24, resizeMode: 'contain' }} />
                                                        :
                                                        <View style={{ width: 24, height: 24, backgroundColor: 'lightgray', borderRadius: 24 }} />
                                                    }
                                                </View>
                                                <View style={{ width: 24, flex: 1 }}>
                                                    {showBottomLine ? <View style={{ backgroundColor: bottomLineColor, width: 1, flex: 1, alignSelf: 'center' }} /> : null}
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }} />
                    </View>
                    {!showRegistrationSuccessfull ? null :
                        <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' }}>
                            <View style={{ backgroundColor: green, alignSelf: 'stretch', padding: 20 }}>
                                <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Registro de carga exitoso</Text>
                            </View>
                        </View>
                    }
                    {!this.state.showCurrentTaskAlert ? null :
                        <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' }}>
                            <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, padding: 20, flexDirection: "row" }}>
                                <Image
                                    source={img}
                                    style={{ tintColor: lightBlue, width: 20, height: 20, marginEnd: 20, resizeMode: 'contain' }} />
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20 ,fontFamily:"ProductSans-Regular"}}>Has llegado a <Text style={{ color: lightBlue,fontFamily:"ProductSans-Regular" }}>{text}</Text></Text>
                                    <Text style={{ color: 'white', marginTop: 10,fontFamily:"ProductSans-Regular" }}>Hora: {moment().format('hh:mm a')}</Text>
                                </View>
                            </View>
                            <View style={{ alignSelf: 'stretch', padding: 20, backgroundColor: 'white', paddingBottom: 35 }}>
                                <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
                                    <Image source={ic_exclamation_mark} style={{ width: 20, height: 20, marginEnd: 20 }} />
                                    <Text>¿Desea confirmar?</Text>
                                </View>
                                <View style={{ alignSelf: 'stretch', height: 30, flexDirection: 'row', marginTop: 10 }}>
                                    <TouchableOpacity
                                        onPress={() => { this._onConfirm() }}
                                        style={{ backgroundColor: lightBlue, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Confirmar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => { this._onCancel() }}
                                        style={{ backgroundColor: red, marginStart: 10, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Cancelar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    }

                    {!this.state.showNoCurrentTaskAlert ? null :
                        <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' }}>
                            <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, padding: 20, flexDirection: "row" }}>
                                <Image
                                    source={ic_exclamation_mark}
                                    style={{ tintColor: lightBlue, width: 20, height: 20, marginEnd: 20, resizeMode: 'contain' }} />
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20,fontFamily:"ProductSans-Regular" }}>¡Debes realizar <Text style={{ color: lightBlue }}>{this.state.requiredTask}</Text> antes de avanzar!</Text>
                                </View>
                            </View>
                            <View style={{ alignSelf: 'stretch', padding: 20, paddingTop: 10, backgroundColor: 'white', paddingBottom: 35 }}>
                                <View style={{ alignSelf: 'center', height: 30, flexDirection: 'row', marginTop: 10, width: 150 }}>
                                    <TouchableOpacity
                                        onPress={() => { this._onAgree() }}
                                        style={{ backgroundColor: lightBlue, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white', fontWeight: 'bold',fontFamily:"ProductSans-Regular" }}>De acuerdo</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    }

                    {!this.state.showAlert ? null :
                        <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end', width: '100%' }}>
                            <View style={{ padding: 20, backgroundColor: 'white', width: '100%', flexDirection: 'row', paddingBottom: 40 }}>
                                <View style={{ flexDirection: "row", alignSelf: "center", width: '20%', justifyContent: 'center' }}>
                                    <TouchableOpacity
                                        style={{ alignItems: 'center', width: 36, height: 36, marginEnd: 20, justifyContent: 'center', backgroundColor: green, borderRadius: 20, marginStart: 20 }}
                                        onPress={() => { null }}>
                                        <Image
                                            source={ic_phone}
                                            style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ height: 30, marginTop: 20, width: '80%', justifyContent: 'space-around' }}>
                                    <Text style={{ alignSelf: 'center', fontWeight: 'bold', paddingBottom: 40 ,fontFamily:"ProductSans-Regular"}}>Abrir aplicación de llamadas</Text>
                                    <View style={{ height: 30, marginTop: 20, width: '100%', flexDirection: 'row', justifyContent: 'space-around' }}>
                                        <TouchableOpacity
                                            onPress={() => { this._onConfirmCall() }}
                                            style={{ backgroundColor: lightBlue, width: '30%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Abrir</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => { this._onCancelCall() }}
                                            style={{ backgroundColor: red, width: '30%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Cancelar</Text>
                                        </TouchableOpacity>

                                    </View>

                                </View>

                            </View>
                        </View>
                    }



                    <Loader isVisible={this.state.loading} />
                </SafeAreaView>
            </Fragment>
        )
    }
}

const mapStateProps = ({ logIn, travel }) => {
    const { token } = logIn.user
    const { watcherId, currentPosition, currentTravel, currentTasks } = travel
    return { watcherId, currentTasks, currentPosition, travelId: currentTravel.Id.toString(), currentTravel, token }
}
export default connect(mapStateProps, { checkActivity, resetInitialService, refreshTavel, setCurrentPage })(CheckListScreen);
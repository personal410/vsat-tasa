import React, { Component } from 'react'
import { Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View, TextInput, Alert } from 'react-native'
import { connect } from 'react-redux'

import { spaceBlack, lightBlue, red, spaceBlackTrans } from '../../../resources/color'
import { ic_arrow_left, ic_calendar, ic_clock, ic_location, ic_vertical_line, ic_arrow_down, ic_exclamation_mark, ic_triangle_warning } from '../../../resources/icon'
import { cancelTravel, listCancelTravelReason, getMyServices } from '../../../app/thunk/travel/actions'
import SelectInput from 'react-native-select-input-ios'
import DropDown from '../../component/dropDown'
import Loader from '../../component/loader'
import {Capitalize} from '../../../app/util'

class CancelTravelScreen extends Component {
    state = {
        idReason: 0,
        selectReason: [{ key: '0', value: 0, label: 'Seleccione un motivo' }],
        countLabel: 0,
        loading: false,
        showStartJourneyAlert: false,
        visibleSelect: false,
        titleSelect: 'Seleccione un motivo',
        Notes: ''
    }
    constructor(props) {
        super(props)
        this._onBack.bind(this)
        this._cancelTravel.bind(this)
        this._onCancelAlert.bind(this)
        this._onStartCancel.bind(this)
        this._selectReason.bind(this)
    }
    componentDidMount() {
        this.setState({ loading: true }, () => {
            this.props.listCancelTravelReason({
                token: this.props.token, callback: (data) => {
                    this.setState({loading:false, selectReason:data})
                }
            })
        })
    }
    _onBack() {
        this.props.navigation.goBack()
    }
    _onCancelAlert() {
        this.setState({ showStartJourneyAlert: false })
    }
    _onStartCancel() {
        this.setState({ showStartJourneyAlert: true })
    }
    _cancelTravel(){
        if(this.state.idReason == 0){
            return Alert.alert('Atención', 'Usted tiene que seleccionar un motivo')
        }
        this.setState({ loading: true }, () => {
            this.props.cancelTravel({
                RequestId: this.props.travel.Id.toString(), token: this.props.token, Reason: this.state.idReason, Note: this.state.Notes, callback: () => {
                    this.setState({ loading: false }, () => {
                        this.props.navigation.navigate('Travels');
                    })
                }
            })
        })
    }
    _selectReason(idReason, title) {
        this.setState({ idReason: idReason });
        this.setState({ visibleSelect: false, titleSelect: title });
    }
    render() {
        const { travel } = this.props
        const { StatusId } = travel
        const isProgram = StatusId == 4
        const lineHeight = isProgram ? 20 : 40
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 44, alignItems: 'center', flexDirection: 'row' }}>
                    <TouchableOpacity
                        onPress={() => { this._onBack() }}>
                        <Image source={ic_arrow_left} style={{ width: 40, height: 20, marginStart: 20, resizeMode: 'contain' }} />
                    </TouchableOpacity>
                    <Text style={{ color: spaceBlack, fontSize: 18, fontWeight: 'bold', marginStart: 20,fontFamily:"ProductSans-Regular" }}>{'Cancelar Viaje'}</Text>
                </View>
                <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray', marginTop: 10 }} />
                <ScrollView style={{ flex: 1, margin: 10 }}>
                    <View style={{ borderColor: 'lightgray', borderRadius: 5, borderWidth: 1 }}>
                        <View style={{ flexDirection: 'row', alignSelf: 'stretch', padding: 20, height: 100, backgroundColor: red, borderTopLeftRadius: 5, borderTopRightRadius: 5 }}>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <View style={{ flexDirection: 'row', alignItems:'center' }}>
                                    <Image source={ic_calendar} style={{ width: 15, height: 15, resizeMode: 'contain' }} />
                                    <Text style={{ color: 'white', marginStart: 20,fontFamily:"ProductSans-Regular" }}><Text style={{ fontWeight: 'bold',fontFamily:"ProductSans-Regular" }}>{Capitalize(travel.Day)}</Text> {travel.ShortDate.split('/')[0] + " " + travel.month}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 10 , alignItems:'center'}}>
                                    <Image source={ic_clock} style={{ width: 15, height: 15, resizeMode: 'contain',tintColor:'white' }} />
                                    <Text style={{ color: 'white', marginStart: 20 ,fontFamily:"ProductSans-Regular"}}>{travel.Time}</Text>
                                </View>
                            </View>
                            <View>
                                <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={ic_location} />
                                <Image style={{ width: 15, flex: 1, resizeMode: 'contain', tintColor: 'white' }} source={ic_vertical_line} />
                                <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={ic_location} />
                            </View>
                        </View>

                        <View style={{ alignSelf: 'stretch', backgroundColor: 'white', borderBottomLeftRadius: 5, borderBottomRightRadius: 5 }}>
                            <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: red }} source={ic_location} />
                                    <Text style={{ marginStart: 10,fontFamily:"ProductSans-Regular" }}><Text style={{ fontWeight: 'bold',fontFamily:"ProductSans-Regular" }}>Origen:</Text> {travel.BaseOrigen}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={{ width: 20, height: lineHeight, resizeMode: 'contain' }} source={ic_vertical_line} />
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_location} />
                                    <Text style={{ marginStart: 10 ,fontFamily:"ProductSans-Regular"}}><Text style={{ fontWeight: 'bold',fontFamily:"ProductSans-Regular" }}>Destino:</Text> {travel.BaseFinal}</Text>
                                </View>
                            </View>

                        </View>
                    </View>
                    <View>
                        <Text style={{ color: spaceBlack, fontSize: 18, fontWeight: 'bold', marginStart: 20, marginTop: 20 ,fontFamily:"ProductSans-Regular"}}>{'Motivo de cancelación'}</Text>
                        <TouchableOpacity style={{ height: 40, borderWidth: 1, borderColor: 'lightgray', borderRadius: 5, width: '100%', marginTop: 10, flexDirection: 'row' }}
                            onPress={() => this.setState({ visibleSelect: true })}
                        >
                            <Text style={{ textAlign: 'left', marginLeft: 10, marginTop: 10, color: 'gray', width: '90%' ,fontFamily:"ProductSans-Regular"}}>{this.state.titleSelect}</Text>
                            <Image style={{ width: 15, height: 15, resizeMode: 'contain', alignSelf: 'flex-end', marginBottom: 10 }} source={ic_arrow_down} />
                        </TouchableOpacity>
                    </View>
                    <View>
                        <Text style={{ color: spaceBlack, fontSize: 18, fontWeight: 'bold', marginStart: 20, marginTop: 20 }}>{'Observaciones'}</Text>
                        <TextInput
                            multiline={true}
                            numberOfLines={6}
                            maxLength={100}
                            style={{
                                padding: 20,
                                borderWidth: 1,
                                borderColor: 'lightgray',
                                height: 100,
                                paddingTop: 15,
                                justifyContent: "flex-start",
                                borderRadius: 5,
                                marginTop: 10,
                                fontFamily:"ProductSans-Regular"
                            }}
                            placeholder={"Escribe una breve observación"}
                            placeholderTextColor="#CCCCCC"
                            value={this.state.Notes}
                            onChangeText={(obs) => {
                                this.setState({ countLabel: obs.length.toString(), Notes: obs })
                            }} />
                        <Text style={{ textAlign: 'right', marginEnd: 10, marginTop: 5, fontWeight: '700', color: lightBlue ,fontFamily:"ProductSans-Regular"}}>{`${this.state.countLabel}/100`}</Text>
                    </View>

                    <View style={{ padding: 20, width: '100%' }}>

                        <View style={{ alignSelf: 'center', height: 30, width: 140, flexDirection: 'row' }}>
                            <TouchableOpacity
                                onPress={() => { this._onStartCancel() }}
                                style={{ backgroundColor: red, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Cancelar viaje</Text>
                            </TouchableOpacity>

                        </View>

                    </View>

                </ScrollView>
                {this.state.showStartJourneyAlert ?
                    <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' }}>
                        <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, padding: 20, flexDirection: "row" }}>
                            <Image
                                source={ic_triangle_warning}
                                style={{ tintColor: red, width: 20, height: 20, marginEnd: 20, resizeMode: 'contain' }} />
                            <View>
                                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20,fontFamily:"ProductSans-Regular" }}>¡Hola {this.props.user.FirstName}!</Text>
                                <Text style={{ color: 'white', marginTop: 5,fontFamily:"ProductSans-Regular" }}>Estás a punto de cancelar el viaje</Text>
                            </View>
                        </View>
                        <View style={{ alignSelf: 'stretch', padding: 20, backgroundColor: 'white' }}>
                            <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
                                <Image source={ic_exclamation_mark} style={{ width: 20, height: 20, marginEnd: 20 }} />
                                <Text>Todos los usuarios serán notificados</Text>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 30, marginTop: 20, flexDirection: 'row' }}>
                                <TouchableOpacity
                                    onPress={() => { this._cancelTravel() }}
                                    style={{ backgroundColor: lightBlue, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Confirmar</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => { this._onCancelAlert() }}
                                    style={{ backgroundColor: red, marginStart: 10, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Cancelar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    : null}
                <Loader isVisible={this.state.loading} />
                <DropDown isVisible={this.state.visibleSelect}
                    listReason={this.state.selectReason}
                    selectReason={this._selectReason.bind(this)}
                    idReason={this.state.idReason} 
                    title={'Seleccione una opción'} />
            </SafeAreaView>
        )
    }
}
const mapStateProps = ({ travel, logIn }) => {
    const { currentTravel } = travel
    const { token } = logIn.user
    const { user } = logIn
    return { travel: currentTravel, token, user }
}
export default connect(mapStateProps, { getMyServices, cancelTravel, listCancelTravelReason })(CancelTravelScreen);
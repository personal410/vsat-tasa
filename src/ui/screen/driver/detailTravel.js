import React, { Component,Fragment } from 'react'
import { Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View ,StatusBar} from 'react-native'
import { connect } from 'react-redux'
import moment from 'moment'

import { spaceBlack, lightBlue, red, green, spaceBlackTrans } from '../../../resources/color'
import { ic_arrow_left, ic_calendar, ic_clock, ic_location, ic_vertical_line, ic_buildings, ic_box, ic_user_white, ic_exclamation_mark } from '../../../resources/icon'
import Loader from '../../component/loader'
import { startService, activityList } from '../../../app/thunk/travel/actions'
import { showAlert,Capitalize } from '../../../app/util'

class DetailTravelScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: this.props.travel.StatusId == 4 ? "Viaje Programado" : "Viaje Finalizado",
            showStartJourneyAlert: false,
            loading: false
        }
        this._onBack.bind(this)
        this._onSummary.bind(this)
        this._onStartJourney.bind(this)
        this._onCancelTravel.bind(this)
    }
    componentDidMount(){
        console.log('Detail Travel')
        this.setState({ loading: true }, () => {
            this.props.activityList({
                RequestId: this.props.travel.Id.toString(), token: this.props.token, callback: (e) => {
                    this.setState({ loading: false })
                }
            })
        })
    }
    _onBack(){
        this.props.navigation.goBack()
    }
    _onSummary(){
        this.props.navigation.navigate('SummaryTravel')
    }
    _onStartJourney() {
        this.setState({ showStartJourneyAlert: true })
    }
    _onCancelTravel() {
        this.props.navigation.navigate('CancelTravel')
    }
    _onConfirmJourney() {
        this.setState({ loading: true })
        this.props.startService({
            RequestId: this.props.travel.Id, token: this.props.token, callback: (e) => {
                this.setState({ loading: false })
                if (e == null) {
                    this.props.navigation.navigate('Journey')
                } else {
                    showAlert({ msg: e, type: 2 })
                }
            }
        })
    }
    _onCancelAlert() {
        this.setState({ showStartJourneyAlert: false })
    }
    render() {
        const { travel } = this.props
        const { StatusId } = travel
        const isProgram = StatusId == 4 //TODO: 4
        const backColor = isProgram ? lightBlue : spaceBlack
        const lineHeight = isProgram ? 20 : 40
        const colorUser = isProgram ? green : red

        const a = (this.props.currentTasks.length > 0) ? moment(this.props.currentTasks[1].DateRegister) : 0;
        const b = (this.props.currentTasks.length > 0) ? moment(this.props.currentTasks[this.props.currentTasks.length - 1].DateRegister) : 0;
        const dateDiff = (this.props.currentTasks.length > 0) ? b.diff(a, 'hours') : 0;
        const minutosDiff = (this.props.currentTasks.length > 0) ? b.diff(a, 'minutes') - (dateDiff * 60) : 0;
        return (

            <Fragment >
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />

                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 44, alignItems: 'center', flexDirection: 'row' }}>
                        <TouchableOpacity
                            onPress={() => { this._onBack() }}>
                            <Image source={ic_arrow_left} style={{ width: 40, height: 20, marginStart: 20, resizeMode: 'contain' }} />
                        </TouchableOpacity>
                        <Text style={{ color: spaceBlack, fontSize: 22, fontWeight: 'bold', marginStart: 20 , fontFamily: "ProductSans-Regular"}}>{this.state.title}</Text>
                    </View>
                    <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray', marginTop: 10 }} />
                    <ScrollView style={{ flex: 1, margin: 10 }}>
                        <View style={{ borderColor: 'lightgray', borderRadius: 5, borderWidth: 1 }}>
                            <View style={{ flexDirection: 'row', alignSelf: 'stretch', padding: 20, height: 100, backgroundColor: backColor, borderTopLeftRadius: 5, borderTopRightRadius: 5 }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <View style={{ flexDirection: 'row', alignItems:'center' }}>
                                        <Image source={ic_calendar} style={{ width: 15, height: 15, resizeMode: 'contain' }} />
                                        <Text style={{ color: 'white', marginStart: 20 , fontSize:18, fontFamily: "ProductSans-Regular"}}><Text style={{ fontWeight: 'bold' , fontFamily: "ProductSans-Regular"}}>{Capitalize(travel.Day)}</Text> {travel.ShortDate.split('/')[0] + " " + Capitalize(travel.month)}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 10, alignItems:'center' }}>
                                        <Image source={ic_clock} style={{ width: 15, height: 15, resizeMode: 'contain',tintColor:'white'}} />
                                        <Text style={{ color: 'white', marginStart: 20 ,fontSize:18, fontFamily: "ProductSans-Regular"}}>{travel.Time}</Text>
                                    </View>
                                </View>
                                <View>
                                    <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={ic_location} />
                                    <Image style={{ width: 15, flex: 1, resizeMode: 'contain', tintColor: 'white' }} source={ic_vertical_line} />
                                    <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={ic_location} />
                                </View>
                            </View>
                            <View style={{ alignSelf: 'stretch', backgroundColor: 'white', borderBottomLeftRadius: 5, borderBottomRightRadius: 5 }}>
                                <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: red }} source={ic_location} />
                                        <Text style={{ marginStart: 10 ,fontSize:18, fontFamily: "ProductSans-Regular"}}><Text style={{ fontWeight: 'bold' , fontFamily: "ProductSans-Regular"}}>Origen:</Text> {travel.BaseOrigen}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={{ width: 20, height: lineHeight, resizeMode: 'contain' }} source={ic_vertical_line} />
                                        {isProgram ? null : <Text style={{ marginStart: 10, color: 'gray', fontWeight: 'bold',fontSize:18, fontFamily: "ProductSans-Regular" }}> {(this.props.currentTasks.length > 0) ? moment(this.props.currentTasks[1].DateRegister).format('h:mm a'):''}</Text>}
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_location} />
                                        <Text style={{ marginStart: 10,fontSize:18, fontFamily: "ProductSans-Regular" }}><Text style={{ fontWeight: 'bold', fontFamily: "ProductSans-Regular" }}>Destino:</Text> {travel.BaseFinal}</Text>
                                    </View>
                                    {isProgram ? null : <Text style={{ marginStart: 30, color: 'gray', fontWeight: 'bold',fontSize:18 , fontFamily: "ProductSans-Regular"}}>{  (this.props.currentTasks.length > 0) ? moment(this.props.currentTasks[this.props.currentTasks.length - 1].DateRegister).format('h:mm a'):''}</Text>}
                                </View>
                                <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                                {isProgram ? null :
                                    <View>
                                        <View style={{ alignSelf: 'stretch', flexDirection: 'row', padding: 20 }}>
                                            <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_clock} />
                                            <Text style={{ marginStart: 10 ,fontSize:18, fontFamily: "ProductSans-Regular"}}><Text style={{ fontWeight: 'bold' , fontFamily: "ProductSans-Regular"}}>Tiempo de servicio:</Text> {dateDiff} h {minutosDiff} m</Text>
                                        </View>
                                        <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                                    </View>
                                }
                                <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                    <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                                        <Image style={{ width: 20, height: 20, resizeMode: 'contain' }} source={ic_buildings} />
                                        <Text style={{ marginStart: 10,fontSize:18, fontFamily: "ProductSans-Regular" }}><Text style={{ fontWeight: 'bold', fontFamily: "ProductSans-Regular" }}>Empresa:</Text> {travel.TransportProvider}</Text>
                                    </View>
                                    <View style={{ alignSelf: 'stretch', flexDirection: 'row', marginTop: 10 }}>
                                        <Image style={{ width: 20, height: 20, resizeMode: 'contain' }} source={ic_box} />
                                        <Text style={{ marginStart: 10,fontSize:18, fontFamily: "ProductSans-Regular" }}><Text style={{ fontWeight: 'bold', fontFamily: "ProductSans-Regular" }}>Carga:</Text> {travel.ProductName}</Text>
                                    </View>
                                </View>
                                <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                                <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                    <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                                        <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: colorUser }} source={ic_user_white} />
                                        <Text style={{ marginStart: 10 ,fontSize:18, fontFamily: "ProductSans-Regular"}}><Text style={{ fontWeight: 'bold' , fontFamily: "ProductSans-Regular"}}>Solicitado por:</Text> {travel.FirstName + " " + travel.LastName}</Text>
                                    </View>

                                </View>
                                <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray' }} />
                                <View style={{ alignSelf: 'stretch', padding: 20 }}>
                                    {isProgram ?
                                        <View style={{ alignSelf: 'stretch', height: 30, flexDirection: 'row' }}>
                                            <TouchableOpacity
                                                onPress={() => { this._onStartJourney() }}
                                                style={{ backgroundColor: lightBlue, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={{ color: 'white', fontFamily: "ProductSans-Regular" }}>Iniciar viaje</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => { this._onCancelTravel() }}
                                                style={{ backgroundColor: red, marginStart: 10, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={{ color: 'white', fontFamily: "ProductSans-Regular" }}>Cancelar</Text>
                                            </TouchableOpacity>
                                        </View>
                                        :
                                        <TouchableOpacity
                                            onPress={() => { this._onSummary() }}
                                            style={{ alignSelf: 'center', height: 30, paddingStart: 20, paddingEnd: 20, backgroundColor: lightBlue, width: '50%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: 'white', fontFamily: "ProductSans-Regular" }}>Resumen</Text>
                                        </TouchableOpacity>
                                    }
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                    {this.state.showStartJourneyAlert ?
                        <SafeAreaView style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' }}>
                            <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, padding: 20, flexDirection: "row" }}>
                                <Image
                                    source={ic_user_white}
                                    style={{ tintColor: lightBlue, width: 20, height: 20, marginEnd: 20, resizeMode: 'contain' }} />
                                <View>
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20 , fontFamily: "ProductSans-Regular"}}>¡Hola {this.props.user.FirstName}!</Text>
                                    <Text style={{ color: 'white', marginTop: 5 , fontFamily: "ProductSans-Regular"}}>Estás a punto de iniciar un viaje</Text>
                                </View>
                            </View>
                            <View style={{ alignSelf: 'stretch', padding: 20, backgroundColor: 'white',paddingBottom:35 }}>
                                <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
                                    <Image source={ic_exclamation_mark} style={{ width: 20, height: 20, marginEnd: 20 }} />
                                    <Text style={{ fontFamily: "ProductSans-Regular"}}>Todos los usuarios serán notificados</Text>
                                </View>
                                <View style={{ alignSelf: 'stretch', height: 30, marginTop: 20, flexDirection: 'row' }}>
                                    <TouchableOpacity
                                        onPress={() => { this._onConfirmJourney() }}
                                        style={{ backgroundColor: lightBlue, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white', fontFamily: "ProductSans-Regular" }}>Confirmar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => { this._onCancelAlert() }}
                                        style={{ backgroundColor: red, marginStart: 10, flex: 1, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white', fontFamily: "ProductSans-Regular" }}>Cancelar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </SafeAreaView>
                        : null}

                    <Loader isVisible={this.state.loading} />
                </SafeAreaView>
            </Fragment>
        )
    }
}


const mapStateProps = ({ travel, logIn }) => {
    const { currentTravel, currentTasks } = travel
    const { token } = logIn.user
    const { user } = logIn
    return { travel: currentTravel, token, user, currentTasks }
}

export default connect(mapStateProps, { startService, activityList })(DetailTravelScreen);
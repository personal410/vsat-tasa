import React, { PureComponent } from 'react';
import { FlatList, Image, SafeAreaView, Text, TouchableOpacity, View, StatusBar } from 'react-native';
import { connect } from 'react-redux'

import { spaceBlack, lightBlue, lighterBlue } from '../../../resources/color'
import { ic_calendar, ic_location, ic_vertical_line, ic_sync } from '../../../resources/icon'
import { getMyServices, setCurrentTravel, resetInitialService } from '../../../app/thunk/travel/actions'
import { setCurrentPage } from '../../../app/thunk/auth/actions'
import { showAlert,Capitalize } from '../../../app/util'
import Loader from '../../component/loader'

class TravelsScreen extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            tabs: [{ key: "P", title: "Programados" }, { key: "F", title: "Finalizados" }],
            idxSelectedTab: "P",
            travels: [],
            finishedTravels: [],
            loading: false,
            finishedLoaded: false
        }
        this._onTabPressed.bind(this)
        this._onTravelPressed.bind(this)
        this._onReload.bind(this)
    }

    componentDidMount(){
        this.didFocusListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('dark-content')
            if(this.props.currentPage == 'journey'){
                this.props.navigation.navigate('Journey')
            }else if(this.props.currentPage == 'checklist'){
                this.props.navigation.navigate('CheckList')
            }else{
                this.props.setCurrentPage('travels')
            }
        })
        this.props.resetInitialService(1);
        this._loadTravels(4)//TODO: 4
    }
    componentWillUnmount(){
        this.didFocusListener.remove()
    }
    _onTabPressed(newKey) {
        if (newKey != this.state.idxSelectedTab) {
            this.setState({ idxSelectedTab: newKey })
            if (newKey == "F" && !this.state.finishedLoaded) {
                this._loadTravels(7)
            }
        }
    }
    _onTravelPressed(travel) {
        this.props.setCurrentTravel({
            travel, callback: () => {
                this.props.navigation.navigate('DetailTravel')
            }
        })
    }

    _loadTravels(status) {
        this.setState({ loading: true }, () => {
            this.props.getMyServices({
                token: this.props.token, status, callback: (error, data) => {
                    if (error != null) {
                        showAlert({ msg: error, type: 2 })
                        this.setState({ loading: false })
                    } else {
                        if (status == 4) {//TODO: 4
                            this.setState({ loading: false, travels: data })
                        } else {
                            this.setState({ loading: false, finishedTravels: data, finishedLoaded: true })
                        }

                    }
                }
            })
        })
    }

    _onReload() {
        this.props.resetInitialService(1);
        this._loadTravels(this.state.idxSelectedTab == "P" ? 4 : 7)
    }
    render(){
        if (this.props.token == null) {
            return null
        }
        if (this.props.refreshTravel) {
            this._onReload();
            this._loadTravels(4)
            this._loadTravels(7)
        }
        const finalTravels = this.state.idxSelectedTab == "P" ? this.state.travels : this.state.finishedTravels

        console.log('finalTravels',finalTravels)

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 44, justifyContent: 'center' }}>
                    <Text style={{ color: spaceBlack, fontSize: 18, fontWeight: 'bold', marginStart: 20, alignSelf: 'stretch', fontFamily: "ProductSans-Bold"}}>Mis viajes</Text>
                    <TouchableOpacity style={{ position: 'absolute', right: 20 }} onPress={() => { this._onReload() }}>
                        <Image
                            source={ic_sync}
                            style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} />
                    </TouchableOpacity>
                </View>
                <View style={{ alignSelf: 'stretch', height: 40 }}>
                    <FlatList
                        style={{ flex: 1 }}
                        data={this.state.tabs}
                        extraData={this.state.idxSelectedTab}
                        horizontal
                        renderItem={({ item }) => {
                            const isSelected = item.key == this.state.idxSelectedTab
                            const backgroundColor = isSelected ? lighterBlue : 'white'
                            const textColor = isSelected ? lightBlue : 'lightgray'
                            const borderColor = isSelected ? lighterBlue : 'lightgray'
                            const fontWeight = isSelected ? 'bold' : 'normal'
                            return (
                                <View style={{ justifyContent: 'center', marginStart: 20 }}>
                                    <TouchableOpacity style={{ alignContent: 'center', justifyContent: 'center', backgroundColor: backgroundColor, paddingStart: 15, paddingEnd: 15, height: 30, borderRadius: 15, borderWidth: 1, borderColor: borderColor }} onPress={() => { this._onTabPressed(item.key) }}>
                                        <Text style={{ color: textColor, fontWeight: fontWeight,fontFamily:"ProductSans-Regular"}}>{item.title}</Text>
                                    </TouchableOpacity>
                                </View>
                            )
                        }} />
                </View>
                <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray', marginTop: 10 }} />
                <FlatList
                    data={finalTravels}
                    removeClippedSubviews={false}
                    extraData={this.state.idxSelectedTab}
                    style={{ flex: 1, marginTop: 10 }}
                    renderItem={({ item }) => {
                        const backgroundLeftColor = this.state.idxSelectedTab == "P" ? lightBlue : spaceBlack
                        return (
                            <View style={{ padding: 5, flex: 1, height: 100 }}>
                                <TouchableOpacity
                                    onPress={() => { this._onTravelPressed(item) }}
                                    style={{ flex: 1, flexDirection: "row", borderWidth: 1, borderColor: 'lightgray', borderRadius: 5 }}>
                                    <View style={{ backgroundColor: backgroundLeftColor, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, paddingStart: 10, paddingEnd: 10, justifyContent: 'center' }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={{ color: 'white', flex: 1, fontWeight: 'bold',fontSize:16,fontFamily:"ProductSans-Regular" }}>
                                                {Capitalize(item.Day)}
                                            </Text>
                                            <Image
                                                source={ic_calendar}
                                                style={{ width: 15, height: 15, resizeMode: 'contain' }} />
                                        </View>
                                        <Text style={{ color: 'white', marginTop: 10 , fontFamily: "ProductSans-Regular"}}>{item.ShortDate + " - " + item.Time}</Text>
                                    </View>
                                    <View style={{ padding: 10, flex: 1, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5 }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: 'red' }} source={ic_location} />
                                            <Text style={{ marginStart: 10,fontFamily:"ProductSans-Regular" }}>{item.BaseOrigen}</Text>
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <Image style={{ width: 20, flex: 1, resizeMode: 'contain' }} source={ic_vertical_line} />
                                        </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Image style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue }} source={ic_location} />
                                            <Text style={{ marginStart: 10,fontFamily:"ProductSans-Regular" }}>{item.BaseFinal}</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )
                    }} />
                <Loader isVisible={this.state.loading} />
            </SafeAreaView>
        )
    }
}

const mapStateProps = ({ logIn, travel }) => {
    if (logIn.user == null) {
        return {}
    }
    const { user, currentPage } = logIn
    const { token } = user
    const { refreshTravel } = travel
    return { token, refreshTravel, currentPage }
}
export default connect(mapStateProps, { setCurrentPage, getMyServices, setCurrentTravel, resetInitialService })(TravelsScreen);
import React, { PureComponent } from 'react';
import { AppState, BackHandler, Image, PermissionsAndroid, Platform, SafeAreaView, Text, TouchableOpacity, View, Linking, StatusBar } from 'react-native';
import MapView, { Marker, Polyline, PROVIDER_GOOGLE } from 'react-native-maps';
import { connect } from 'react-redux';
import Geolocation from 'react-native-geolocation-service'

import { spaceBlack, spaceBlackTrans, red, lightBlue, green } from '../../resources/color'
import { ic_location, ic_triangle_warning, ic_phone, ic_flag, ic_truck_front, ic_origin_2, ic_user_white, ic_exclamation_mark, ic_flag_blue, ic_race_flag, tracking_truck, finish_flag } from '../../resources/icon'
import Loader from '../component/loader'
import { sendLocation, setWatcherId, setCurrentPosition, checkActivity, resetInitialService } from '../../app/thunk/travel/actions'
import { setCurrentPage } from '../../app/thunk/auth/actions'
import { showAlert, calculateDistance, getUTCDateText, getUTCDate0Text } from '../../app/util'
import Location from '../../../Location'

class JourneyScreen extends PureComponent {
    constructor(props) {
        super(props)
        let travelDest = { latitude: '', longitude: '' }
        const { BaseOrigenLatitude, BaseOrigenLongitude, BaseFinalLatitude, BaseFinalLongitude } = this.props.travel
        if (this.props.initialService < 3) {
            if (BaseOrigenLatitude != null && BaseOrigenLongitude != null) {
                travelDest = { latitude: parseFloat(BaseOrigenLatitude), longitude: parseFloat(BaseOrigenLongitude) }
            } else {
                travelDest = { latitude: -12.055378, longitude: -77.0755451 }
            }
        } else {
            if (BaseFinalLatitude != null && BaseFinalLongitude != null) {
                travelDest = { latitude: parseFloat(BaseFinalLatitude), longitude: parseFloat(BaseFinalLongitude) }
            } else {
                travelDest = { latitude: -12.055378, longitude: -77.0755451 }
            }
        }
        this.state = {
            loading: true,
            lastDate: null,
            polylineData: null,
            travelOrigin: null,
            travelDest,
            travelDistance: '0 km',
            travelDuration: '0 h',
            showArrivalAlert: false,
            showAlert: false,
            travelEstimate: '00',
            EstimateSeg: 0,
            region: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            speed: 0,
            altitude: '',
            showSOSAlert: false
        }
        this._onWarning.bind(this)
        this._onCall.bind(this)
        this._onConfirmArrival.bind(this)
        this.willBlurListener = this.props.navigation.addListener('willBlur', () => {
            if (Platform.OS == 'android') {
                BackHandler.removeEventListener('hardwareBackPress', this._onBackButtonPressed)
                AppState.removeEventListener('change', this._onAppStateChanged)
            }
        })

    }


    componentDidMount() {

        this.props.setCurrentPage('journey')

            StatusBar.setBarStyle('light-content')
            this.setState({ lastDate: null })
            Geolocation.clearWatch(this.props.watcherId)
            
            if (this.props.initialService >= 3) {
                const { BaseFinalLatitude, BaseFinalLongitude } = this.props.travel
                if (BaseFinalLatitude != null && BaseFinalLongitude != null) {
                    this.setState({ travelDest: { latitude: parseFloat(BaseFinalLatitude), longitude: parseFloat(BaseFinalLongitude) } })

                } else {
                    this.setState({ travelDest: { latitude: -12.055378, longitude: -77.0755451 } })
                }
            }
            this._getLocation()
            if (Platform.OS == 'android') {
                BackHandler.addEventListener('hardwareBackPress', this._onBackButtonPressed)
                AppState.addEventListener('change', this._onAppStateChanged)
            }
            
        
    

        this._navListener = this.props.navigation.addListener('didFocus', () => {
            
            this.props.setCurrentPage('journey')

            StatusBar.setBarStyle('light-content')
            this.setState({ lastDate: null })
            Geolocation.clearWatch(this.props.watcherId)
            
            if (this.props.initialService >= 3) {
                const { BaseFinalLatitude, BaseFinalLongitude } = this.props.travel
                if (BaseFinalLatitude != null && BaseFinalLongitude != null) {
                    this.setState({ travelDest: { latitude: parseFloat(BaseFinalLatitude), longitude: parseFloat(BaseFinalLongitude) } })

                } else {
                    this.setState({ travelDest: { latitude: -12.055378, longitude: -77.0755451 } })
                }
            }
            this._getLocation()
            if (Platform.OS == 'android') {
                BackHandler.addEventListener('hardwareBackPress', this._onBackButtonPressed)
                AppState.addEventListener('change', this._onAppStateChanged)
            }
        })
    }

    add_minutes = function (dt, minutes) {
        return new Date(dt.getTime() + minutes * 60000);
    }
    componentWillUnmount() {

        //this.props.setCurrentPage('journey')

        this.willBlurListener.remove()
    }
    _onBackButtonPressed() {
        return true
    }
    _onAppStateChanged(newState) {
        if (newState == 'active') {
            Location.stopService()
        } else {
            Location.startService()
        }
    }
    _onConfirmSOS() {

        const date = new Date()
        const params = { latitude: this.state.region.latitude.toString(), longitude: this.state.region.longitude.toString(), speed: parseInt(this.state.speed).toString(), course: "0", altitude: this.state.altitude.toString(), date: getUTCDate0Text(date), event: '99', token: this.props.token }
        this.props.sendLocation(params)
        this.setState({ showSOSAlert: false })
    }
    _onCancelSOS() {
        this.setState({ showSOSAlert: false })
    }


    decode = (t, e) => { for (var n, o, u = 0, l = 0, r = 0, d = [], h = 0, i = 0, a = null, c = Math.pow(10, e || 5); u < t.length;) { a = null, h = 0, i = 0; do a = t.charCodeAt(u++) - 63, i |= (31 & a) << h, h += 5; while (a >= 32); n = 1 & i ? ~(i >> 1) : i >> 1, h = i = 0; do a = t.charCodeAt(u++) - 63, i |= (31 & a) << h, h += 5; while (a >= 32); o = 1 & i ? ~(i >> 1) : i >> 1, l += n, r += o, d.push([l / c, r / c]) } return d = d.map(function (t) { return { latitude: t[0], longitude: t[1] } }) }

    async _getLocation() {
        const hasPermission = await this._hasLocationPermission()
        if (!hasPermission) {
            return
        }
        const watchOptions = Platform.OS === 'ios' ? { enableHighAccuracy: true, distanceFilter: 100 } : { enableHighAccuracy: true, distanceFilter: 1, interval: 10000, fastestInterval: 10000 }
        const watcherId = Geolocation.watchPosition((position) => {
            const myregion = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: 0.01,
                longitudeDelta: 0.0011,
            }
            this.setState({ region: myregion });
            const { coords } = position
            const { latitude, longitude, speed, altitude } = coords
            this.setState({ speed: speed, altitude: altitude })
            this.props.setCurrentPosition({ latitude, longitude })
            const date = new Date()
            let firstLocation = false
            if (this.state.lastDate != null) {
                const diff = (date - this.state.lastDate) / 1000
                if (diff < this.props.IntervalLocation) {
                    return
                }
            } else {
                firstLocation = true
                const camera = { center: { latitude, longitude: position.coords.longitude }, zoom: 12 }
                this.map.animateCamera(camera)
                let firstTask
                if (this.props.initialService < 3) {
                    firstTask = this.props.currentTasks[0]
                } else {
                    let tasks = this.props.currentTasks.filter(e => e.InInitialService == 3)
                    firstTask = tasks[0]
                }
                this.props.checkActivity({
                    RequestId: this.props.travel.Id.toString(), ActivityId: firstTask.Id.toString(), Latitude: latitude.toString(), Longitude: longitude.toString(), InInitialService: firstTask.InInitialService, token: this.props.token, callback: (e) => {
                        if (firstTask.InInitialService == 0) {
                            this.props.navigation.navigate('CheckList')
                        }
                        if (e == null) {
                            const url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' + latitude.toString() + ',' + longitude.toString() + '&destination=' + this.state.travelDest.latitude + ',' + this.state.travelDest.longitude + '&key=AIzaSyCHkeqWpktceLDachthKaB8FwvcoA-QPAk'
                            fetch(url, { method: 'GET', headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' } }).then((res) => {

                                res.json().then(json => {
                                    let route = json.routes[0]
                                    if (route != null) {
                                        const { legs, overview_polyline } = route
                                        const leg = legs[0]
                                        let polyline = overview_polyline.points;
                                        let minutes_add = leg.duration.value / 60;
                                        let periodo = (this.add_minutes(new Date(), minutes_add).getHours() >= 12) ? 'P.M.' : 'A.M.'

                                        let new_time = this.add_minutes(new Date(), minutes_add).getHours() + ':' + this.add_minutes(new Date(), minutes_add).getMinutes() + ' ' + periodo

                                        this.setState({
                                            polylineData: this.decode(polyline),
                                            travelOrigin: { latitude, longitude },
                                            travelDistance: leg.distance.text,
                                            travelDuration: leg.duration.text,
                                            loading: false,
                                            travelEstimate: new_time,
                                            EstimateSeg: leg.duration.value
                                        })
                                    } else {
                                        this.setState({
                                            polylineData: [{ latitude, longitude }, this.state.travelDest],
                                            travelOrigin: { latitude, longitude },
                                            travelDistance: "XX KM",
                                            travelDuration: "XX H",
                                            loading: false
                                        })
                                    }
                                })
                            }).catch(e => {
                                this.setState({
                                    polylineData: [{ latitude, longitude }, this.state.travelDest],
                                    travelOrigin: { latitude, longitude },
                                    travelDistance: "XX KM",
                                    travelDuration: "XX H",
                                    loading: false
                                })
                            })
                        } else {
                            this.setState({
                                loading: false,
                                polylineData: [{ latitude, longitude }, this.state.travelDest],
                                travelOrigin: { latitude, longitude },
                                travelDistance: "XX KM",
                                travelDuration: "XX H",
                                loading: false
                            })
                            showAlert({ msg: e, type: 2 })
                        }
                    }
                })
            }


            this.setState({ lastDate: date })



            const url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' + latitude.toString() + ',' + longitude.toString() + '&destination=' + this.state.travelDest.latitude + ',' + this.state.travelDest.longitude + '&key=AIzaSyCHkeqWpktceLDachthKaB8FwvcoA-QPAk'//TODO: Eliminar ultima A
            fetch(url, { method: 'GET', headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' } }).then((res) => {
                res.json().then(json => {
                    let route = json.routes[0]
                    if (route != null) {
                        const { legs, overview_polyline } = route
                        const leg = legs[0]
                        let polyline = overview_polyline.points;
                        let minutes_add = leg.duration.value / 60;
                        let periodo = (this.add_minutes(new Date(), minutes_add).getHours() >= 12) ? 'P.M.' : 'A.M.'
                        let new_time = this.add_minutes(new Date(), minutes_add).getHours() + ':' + this.add_minutes(new Date(), minutes_add).getMinutes() + ' ' + periodo
                        this.setState({
                            polylineData: this.decode(polyline),
                            travelOrigin: { latitude, longitude },
                            travelDistance: leg.distance.text,
                            travelDuration: leg.duration.text,
                            loading: false,
                            travelEstimate: new_time,
                            EstimateSeg: leg.duration.value
                        }, () => {
                            const params = { latitude: latitude.toString(), longitude: longitude.toString(), speed: parseInt(this.state.speed).toString(), course: "0", altitude: altitude.toString(), date: getUTCDate0Text(date), event: '31', token: this.props.token, ETA: this.state.EstimateSeg, RequestId: this.props.travel.Id.toString() }
                            this.props.sendLocation(params)
                        })
                    } else {
                        this.setState({
                            polylineData: [{ latitude, longitude }, this.state.travelDest],
                            travelOrigin: { latitude, longitude },
                            travelDistance: "XX KM",
                            travelDuration: "XX H",
                            loading: false
                        }, () => {
                            const params = { latitude: latitude.toString(), longitude: longitude.toString(), speed: parseInt(this.state.speed).toString(), course: "0", altitude: altitude.toString(), date: getUTCDate0Text(date), event: '31', token: this.props.token, ETA: this.state.EstimateSeg, RequestId: this.props.travel.Id.toString() }
                            this.props.sendLocation(params)
                        })
                    }

                })


            }).catch(e => {

                this.setState({
                    polylineData: [{ latitude, longitude }, this.state.travelDest],
                    travelOrigin: { latitude, longitude },
                    travelDistance: "XX KM",
                    travelDuration: "XX H",
                    loading: false
                }, () => {
                    const params = { latitude: latitude.toString(), longitude: longitude.toString(), speed: parseInt(this.state.speed).toString(), course: "0", altitude: altitude.toString(), date: getUTCDate0Text(date), event: '31', token: this.props.token, ETA: this.state.EstimateSeg, RequestId: this.props.travel.Id.toString() }
                    this.props.sendLocation(params)
                })
            })




            if (!firstLocation) {
                let dist = calculateDistance({ latitude, longitude }, this.state.travelDest)
                if (dist < 50) {
                    this.setState({ showArrivalAlert: true })
                }
            }



        }, () => {
            showAlert({ msg: 'Hubo un error al obtener su ubicación', type: 2 })
        }, watchOptions)
        this.props.setWatcherId(watcherId)
    }
    async _hasLocationPermission() {
        if (Platform.OS == 'ios' || (Platform.OS == 'android' && Platform.Version < 23)) {
            return true
        }
        const hasPermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        if (hasPermission) {
            return true
        }
        const status = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if (status == PermissionsAndroid.RESULTS.GRANTED) {
            return true
        }
        showAlert({ msg: 'No se tiene permiso para obtener su ubicación.', type: 2 })
        return false;
    }
    _onWarning() {
        //this.setState({ showArrivalAlert: true })
        this.setState({ showSOSAlert: true })

    }
    _onArrival() {
        this.setState({ showArrivalAlert: true })
    }
    _onCall() {

        this.setState({ showAlert: true })

    }
    _onConfirmCall() {
        Linking.openURL(`tel:${this.props.travel.Phone}`)
        this.setState({ showAlert: false })
    }
    _onCancelCall() {
        this.setState({ showAlert: false })
    }

    _onCancelArrival() {
        this.setState({ showArrivalAlert: false })
    }


    _onConfirmArrival() {
        this.setState({ loading: true }, () => {

            let task
            if (this.props.initialService < 3) {
                task = this.props.currentTasks[1]
            } else {

                let tasks = this.props.currentTasks.filter(e => e.InInitialService == 4);

                task = tasks[0]
            }


            const { currentPosition, travel } = this.props
            this.props.checkActivity({
                RequestId: travel.Id.toString(), ActivityId: task.Id.toString(), Latitude: currentPosition.latitude.toString(), Longitude: currentPosition.longitude.toString(), InInitialService: task.InInitialService, token: this.props.token, callback: (e) => {
                    this.setState({ loading: false })
                    if (e == null) {
                        this.props.navigation.navigate('CheckList')
                    } else {
                        //showAlert({msg: e, type: 2})
                        this.props.navigation.navigate('CheckList')
                    }

                    this.setState({ showArrivalAlert: false })
                }
            })
        })
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: spaceBlack }}>
                <StatusBar barStyle="light-content" />
                <View style={{ position: 'absolute', width: 50, height: 200, zIndex: 999, right: 0, top: 80 }}>

                    <TouchableOpacity
                        style={{ alignItems: 'center', width: 45, height: 45, justifyContent: 'center', backgroundColor: green, borderRadius: 70, position: 'relative', top: 8, right: 10 }}
                        onPress={() => { this._onCall() }}>
                        <Image
                            source={ic_phone}
                            style={{ width: 25, height: 25, resizeMode: 'contain' }} />
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{ alignItems: 'center', width: 45, height: 45, justifyContent: 'center', backgroundColor: red, borderRadius: 50, position: 'relative', top: 15, right: 10 }}
                        onPress={() => { this._onWarning() }}>
                        <Image
                            source={ic_triangle_warning}
                            style={{ width: 25, height: 25, resizeMode: 'contain' }} />
                    </TouchableOpacity>


                </View>
                <View style={{ alignSelf: 'stretch', flexDirection: "row", backgroundColor: spaceBlack, height: 60, alignItems: 'center' }}>
                    <Image source={ic_location} style={{ tintColor: red, width: 20, height: 20, marginStart: 20, resizeMode: 'contain' }} />
                    <Text style={{ color: 'white', fontWeight: 'bold', flex: 1, marginStart: 20, fontSize: 20 ,fontFamily:"ProductSans-Regular"}}>{(this.props.initialService < 3) ? this.props.travel.BaseOrigen : this.props.travel.BaseFinal}</Text>
                    <TouchableOpacity
                        style={{ alignItems: 'center', width: 45, height: 45, justifyContent: 'center', backgroundColor: lightBlue, borderRadius: 25, marginRight: 15 }}
                        onPress={() => { this._onArrival() }}>
                        <Image
                            source={ic_flag}
                            style={{ width: 25, height: 25, resizeMode: 'contain', tintColor: 'white' }} />
                    </TouchableOpacity>


                </View>
                <MapView
                    ref={m => { this.map = m }}
                    style={{ flex: 1 }}
                    region={this.state.region}
                    provider={PROVIDER_GOOGLE}
                    showsUserLocation={true}
                    showsMyLocationButton={true}
                    followsUserLocation={true}>
                    {this.state.polylineData == null ? null :
                        <Polyline coordinates={this.state.polylineData} strokeWidth={4} strokeColor={lightBlue} />
                    }
                    {this.state.travelOrigin == null ? null :
                        <Marker coordinate={this.state.travelOrigin} title={'Origen'} image={ic_origin_2}  />
                    }
                    {this.state.travelDest == null ? null :
                        <Marker coordinate={this.state.travelDest} title={'Dest'} image={ic_flag_blue} />
                    }
                </MapView>
                <View style={{ alignSelf: 'stretch', flexDirection: "row", backgroundColor: spaceBlack, height: 100, padding: 20, alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            source={ic_truck_front}
                            style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                        <View style={{ marginStart: 20 }}>
                            <Text style={{ color: lightBlue, fontSize: 25, fontWeight: 'bold' ,fontFamily:"ProductSans-Regular"}}>{this.state.travelDuration.replace("hours", "h").replace("mins","m")}</Text>
                            <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>{this.state.travelDistance}</Text>
                        </View>
                    </View>
                    <View style={{ width: 1, alignSelf: 'stretch', backgroundColor: 'white', marginStart: 20 }} />
                    <View style={{ flexDirection: 'row', flex: 1, marginStart: 20, alignItems: 'center' }}>
                        <Image
                            source={ic_flag}
                            style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: red }} />
                        <View style={{ marginStart: 20 }}>
                            <Text style={{ color: 'white', fontWeight: 'bold',fontFamily:"ProductSans-Regular" }}>Llegada estimada</Text>
                            <Text style={{ color: 'white', marginTop: 5 ,fontFamily:"ProductSans-Regular"}}>{this.state.travelEstimate}</Text>
                        </View>
                    </View>
                </View>

                {!this.state.showArrivalAlert ? null :
                    <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' }}>
                        <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, padding: 20, flexDirection: "row" }}>
                            <Image
                                source={ic_user_white}
                                style={{ tintColor: lightBlue, width: 20, height: 20, marginEnd: 20, resizeMode: 'contain' }} />
                            <View style={{ flex: 1 }}>
                                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20,fontFamily:"ProductSans-Regular" }}>¡Hola {this.props.user.FirstName}!</Text>
                                <Text style={{ color: 'white', marginTop: 10 ,fontFamily:"ProductSans-Regular"}}>¡Has llegado al punto de {(this.props.initialService < 3) ? 'Origen' : 'Destino'}!</Text>
                                <View style={{ alignSelf: 'stretch', flexDirection: "row", marginTop: 10 }}>
                                    <Image
                                        source={ic_location}
                                        style={{ tintColor: red, width: 20, height: 20, marginEnd: 10, resizeMode: 'contain' }} />
                                    <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}><Text style={{ fontWeight: 'bold' ,fontFamily:"ProductSans-Regular"}}>{(this.props.initialService < 3) ? 'Origen' : 'Destino'}:</Text> {(this.props.initialService < 3) ? this.props.travel.BaseOrigen : this.props.travel.BaseFinal}</Text>
                                </View>
                            </View>
                            {/* <TouchableOpacity
                                style={{ alignItems: 'center', width: 36, height: 36, justifyContent: 'center', backgroundColor: green, borderRadius: 20, marginStart: 20 }}
                                onPress={() => { this._onCall() }}>
                                <Image
                                    source={ic_phone}
                                    style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                            </TouchableOpacity> */}
                        </View>
                        <View style={{ alignSelf: 'stretch', padding: 20, backgroundColor: 'white' }}>
                            <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
                                <Image source={ic_exclamation_mark} style={{ width: 20, height: 20, marginEnd: 20 }} />
                                <Text style={{fontFamily:"ProductSans-Regular"}}>Confirmar llegada</Text>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 30, marginTop: 20, flexDirection: 'row', justifyContent: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => { this._onConfirmArrival() }}
                                    style={{ backgroundColor: lightBlue, width: '45%', borderRadius: 15, alignItems: 'center', justifyContent: 'center', marginRight: '5%' }}>
                                    <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Confirmar</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => { this._onCancelArrival() }}
                                    style={{ backgroundColor: red, width: '45%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Cancelar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                }

                {!this.state.showAlert ? null :
                    <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end', width: '100%' }}>
                        <View style={{ padding: 20, backgroundColor: 'white', width: '100%', flexDirection: 'row', paddingBottom: 40 }}>
                            <View style={{ flexDirection: "row", alignSelf: "center", width: '20%', justifyContent: 'center' }}>
                                <TouchableOpacity
                                    style={{ alignItems: 'center', width: 50, height: 50, marginEnd: 20, justifyContent: 'center', backgroundColor: green, borderRadius: 20, marginStart: 20 }}
                                    onPress={() => { null }}>
                                    <Image
                                        source={ic_phone}
                                        style={{ width: 25, height: 25, resizeMode: 'contain' }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: 30, marginTop: 20, width: '80%', justifyContent: 'space-around' }}>
                                <Text style={{ alignSelf: 'center', fontWeight: 'bold', paddingBottom: 40 , fontSize: 18,fontFamily:"ProductSans-Regular"}}>Abrir aplicación de llamadas</Text>
                                <View style={{ height: 30, marginTop: 20, width: '100%', flexDirection: 'row', justifyContent: 'space-around' }}>
                                    <TouchableOpacity
                                        onPress={() => { this._onConfirmCall() }}
                                        style={{ backgroundColor: lightBlue, width: '40%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Abrir</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => { this._onCancelCall() }}
                                        style={{ backgroundColor: red, width: '40%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Cancelar</Text>
                                    </TouchableOpacity>

                                </View>

                            </View>

                        </View>
                    </View>
                }

                {!this.state.showSOSAlert ? null :
                    <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end', width: '100%' }}>
                        <View style={{ padding: 20, backgroundColor: 'white', width: '100%', flexDirection: 'row', paddingBottom: 40 }}>
                            <View style={{ flexDirection: "row", alignSelf: "center", width: '20%', justifyContent: 'center' }}>
                                <TouchableOpacity
                                    style={{ alignItems: 'center', width: 50, height: 50, marginEnd: 20, justifyContent: 'center', backgroundColor: red, borderRadius: 30, marginStart: 20 }}
                                    onPress={() => { null }}>
                                    <Image
                                        source={ic_triangle_warning}
                                        style={{ width: 25, height: 25, resizeMode: 'contain' }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: 30, marginTop: 20, width: '80%', justifyContent: 'space-around' }}>
                                <Text style={{ alignSelf: 'center', fontWeight: 'bold', paddingBottom: 40, fontSize: 18,fontFamily:"ProductSans-Regular" }}>Enviar alerta de emergencia</Text>
                                <View style={{ height: 30, marginTop: 20, width: '100%', flexDirection: 'row', justifyContent: 'space-around' }}>
                                    <TouchableOpacity
                                        onPress={() => { this._onConfirmSOS() }}
                                        style={{ backgroundColor: lightBlue, width: '40%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Confirmar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => { this._onCancelSOS() }}
                                        style={{ backgroundColor: red, width: '40%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Cancelar</Text>
                                    </TouchableOpacity>

                                </View>

                            </View>

                        </View>
                    </View>
                }

                <Loader isVisible={this.state.loading} />
            </SafeAreaView>
        )
    }
}

const mapStateProps = ({ logIn, travel }) => {
    const { token, IntervalLocation } = logIn.user
    const { user } = logIn
    const { currentPosition, currentTravel, currentTasks, initialService, watcherId } = travel
    return { token, IntervalLocation, currentPosition, currentTasks, travel: currentTravel, user, initialService, watcherId }
}

export default connect(mapStateProps, { sendLocation, setWatcherId, setCurrentPosition, checkActivity, setCurrentPage })(JourneyScreen);
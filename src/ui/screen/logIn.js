import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Image, PermissionsAndroid, Platform, Keyboard, SafeAreaView, Text, TextInput, TouchableOpacity, TouchableWithoutFeedback, View, StatusBar } from 'react-native';
import Geolocation from 'react-native-geolocation-service'
import firebase from 'react-native-firebase'

import Loader from '../component/loader'
import { spaceBlackTrans, spaceBlack, lightBlue } from '../../resources/color'
import { ic_user, ic_password ,logo} from '../../resources/icon'
import { showAlert } from '../../app/util';
import { logIn, sendToken } from '../../app/thunk/auth/actions'
import { DRIVER_ROLE_ID, GUARD_ROLE_ID, STORAGE_ROLE_ID, QUALITY_ROLE_ID, CERTIFIER_ROLE_ID, TRANSPORT_ROLE_ID } from '../../resources/consts'

class LogInScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: '',
            password: '',
            loading: false
        }
    }
    componentDidMount() {
        if (Platform.OS == 'ios') {
            Geolocation.setRNConfiguration({ authorizationLevel: 'always' })
            Geolocation.requestAuthorization()
        } else {
            this._hasLocationPermission()
        }
    }
    async _hasLocationPermission() {
        if (Platform.Version < 23) {
            return true
        }
        const hasPermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        if (hasPermission) {
            return true
        }
        const status = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if (status == PermissionsAndroid.RESULTS.GRANTED) {
            return true
        }
        showAlert({ msg: 'No se tiene permiso para obtener su ubicación.', type: 2 })
        return false;
    }
    async _checkPermissionNotifications() {
        const enabled = await firebase.messaging().hasPermission()
        if (enabled) {
            this._getToken()
        } else {
            firebase.messaging().requestPermission().then(() => {
                this._getToken()
            }).catch(e => {
                this.setState({ loading: false })
                const { RoleId } = this.props
                    if(RoleId == DRIVER_ROLE_ID || RoleId == GUARD_ROLE_ID){
                        this.props.navigation.navigate('Driver')
                    }else if(RoleId == STORAGE_ROLE_ID){
                        this.props.navigation.navigate('Storage')
                    }else if(RoleId == QUALITY_ROLE_ID){
                        this.props.navigation.navigate('Quality')
                    }else if(RoleId == CERTIFIER_ROLE_ID){
                        this.props.navigation.navigate('Certifier')
                    }else if (RoleId ==TRANSPORT_ROLE_ID){
                        this.props.navigation.navigate('Transport')
                    }else{
                        showAlert({ msg: "Perfil desconocido", type: 1 })
                    }
            })
        }
    }
    async _getToken() {
        const fcmToken = await firebase.messaging().getToken()
        this.props.sendToken({
            fcmToken, token: this.props.token, callback: r => {
                this.setState({ loading: false })
                if(r == null){
                    const { RoleId } = this.props
                    if(RoleId == DRIVER_ROLE_ID || RoleId == GUARD_ROLE_ID){
                        this.props.navigation.navigate('Driver')
                    }else if(RoleId == STORAGE_ROLE_ID){
                        this.props.navigation.navigate('Storage')
                    }else if(RoleId == QUALITY_ROLE_ID){
                        this.props.navigation.navigate('Quality')
                    }else if(RoleId == CERTIFIER_ROLE_ID){
                        this.props.navigation.navigate('Certifier')
                    }else if (RoleId ==TRANSPORT_ROLE_ID){
                        this.props.navigation.navigate('Transport')
                    }else{
                        showAlert({ msg: "Perfil desconocido", type: 1 })
                    }
                }else{
                    showAlert({ msg: r, type: 2 })
                }
            }
        })
    }
    _logIn() {
        Keyboard.dismiss()
        this.setState({ loading: true })
        this.props.logIn({
            Login: this.state.user, Password: this.state.password, callback: (error) => {
                if (error == null) {
                    this._checkPermissionNotifications()
                } else {
                    this.setState({ loading: false }, () => {
                        showAlert({ msg: error, type: 2 })
                    })
                }
            }
        })
    }
    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: 'white' }} />
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1, marginTop:50 }}>
                                <Image source={logo} style={{alignSelf: 'center',width: 150, height: 150 }} resizeMode={'contain'} />
                            </View>
                            <View style={{ flex: 0, backgroundColor: 'white', paddingStart: 30, paddingEnd: 30, paddingTop: 10, paddingBottom: 10 }}>

                                <Text style={{ color: spaceBlack, fontWeight: 'bold', fontSize: 20, alignSelf: 'stretch', textAlign: 'center' }}>Inicio de sesión</Text>
                                <View style={{ flexDirection: 'row', marginTop: 30 }}>
                                    <Image
                                        style={{ width: 30, height: 30 }}
                                        resizeMode={'contain'}
                                        source={ic_user} />
                                    <TextInput
                                        style={{ marginStart: 10, flex: 1, padding: 0, color: spaceBlack, fontSize: 15 }}
                                        placeholder="Usuario"
                                        value={this.state.user}
                                        onChangeText={(txt) => this.setState({ user: txt })} />
                                </View>
                                <View style={{ height: 1, marginTop: 5, alignSelf: 'stretch', backgroundColor: spaceBlackTrans }} />
                                <View style={{ flexDirection: 'row', marginTop: 30 }}>
                                    <Image
                                        style={{ width: 30, height: 30 }}
                                        resizeMode={'contain'}
                                        source={ic_password} />
                                    <TextInput
                                        style={{ marginStart: 10, flex: 1, padding: 0, color: spaceBlack }}
                                        placeholder="Contraseña"
                                        secureTextEntry
                                        value={this.state.password}
                                        onChangeText={(txt) => this.setState({ password: txt })} />
                                </View>
                                <View style={{ height: 1, marginTop: 5, alignSelf: 'stretch', backgroundColor: spaceBlackTrans }} />
                                <TouchableOpacity disabled={this.state.user.length == 0 || this.state.password.length == 0} style={{ height: 50, marginTop: 30, marginStart: 10, marginEnd: 10, borderRadius: 5, alignSelf: 'stretch', backgroundColor: (this.state.user.length == 0 || this.state.password.length == 0 ? spaceBlackTrans : lightBlue), justifyContent: 'center', alignItems: 'center' }} onPress={this._logIn.bind(this)}>
                                    <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}>INGRESAR</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => this.props.navigation.navigate('Recovery')}>
                                    <Text style={{ color: spaceBlack, marginTop: 10, textDecorationLine: 'underline', fontWeight: 'bold', fontSize: 15 }}>¿Olvidó su contraseña?</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1 }} />
                        </View>
                    </TouchableWithoutFeedback>
                    <Loader isVisible={this.state.loading} />
                </SafeAreaView>
            </Fragment>
        );
    }
}

const mapStateProps = ({ logIn }) => {

    if(!logIn.user){

        return {}
    }
    const { token, RoleId } = logIn.user
    return { token, RoleId }
}

export default connect(mapStateProps, { logIn, sendToken })(LogInScreen);
import React, { Component } from 'react';
import { Alert, AppState, BackHandler, Image, Platform, PermissionsAndroid, SafeAreaView, Text, TouchableOpacity, View, StatusBar } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service'
import { connect } from 'react-redux'
import firebase from 'react-native-firebase'

import { setCurrentPage, logOut } from '../../app/thunk/auth/actions'
import { showBadge } from '../../app/thunk/notifcications/actions'
import { getMyServices, startService, activityList, setCurrentTravel , refreshTavel} from '../../app/thunk/travel/actions'
import { spaceBlack } from '../../resources/color'
import { ic_get_out } from '../../resources/icon'
import { showAlert } from '../../app/util'
import Location from '../../../Location'

class MapScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            flex: 0,
            travels: []
        }
        this._getOut.bind(this)
        this._onBackButtonPressed.bind(this)
        this._goTravels.bind(this)
        this._manageNotification.bind(this)
    }

    _loadTravels(status) {
        this.props.getMyServices({
            token: this.props.user.token, status, callback: (error, data) => {
                if (status == 6) {
                    this.setState({ travels: data }, () => {
                        if (this.state.travels.length > 0) {
                            this.props.setCurrentTravel({
                                travel:this.state.travels[0], callback: () => {
                                    this.props.activityList({
                                        RequestId: this.state.travels[0].Id.toString(), token: this.props.user.token, callback: (e) => {
                                            this.props.startService({
                                                RequestId: this.state.travels[0].Id, token: this.props.user.token, callback: (e) => {
                                                    if (e == null) {
                                                        this.props.navigation.navigate('Journey')
                                                    } else {
                                                        showAlert({ msg: e, type: 2 })
                                                    }
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            }
        })
    }

    componentDidMount() {
        this.didFocusListener = this.props.navigation.addListener('didFocus', () => {
            this._loadTravels(6)


            StatusBar.setBarStyle('light-content')
            if (this.props.currentPage == 'journey' || this.props.currentPage == 'checklist') {

                this.props.navigation.navigate('Travels')
            } else {
                this.props.setCurrentPage('map')
            }
        })
        this._getLocation()
        setTimeout(() => {
            this.setState({ flex: 1 })
        }, 100)

        this.onNotificationListener = firebase.notifications().onNotification(async notification => {

            if (this.props.currentPage != 'notifications') {
                this.props.showBadge(true)
            }
            const { title, body, data } = notification
            Alert.alert(title, body, [{
                text: "Ok", onPress: () => {
                    const { action } = data
                    if (action != null && action != undefined) {
                        this._manageNotification(action)
                    }
                }
            }])
        })
        this.onNotificationOpenedListener = firebase.notifications().onNotificationOpened(notificationOpen => {

            const { action } = notificationOpen.notification.data
            if (action != null && action != undefined) {
                this._manageNotification(action)
            }
        })

        if (Platform.OS == 'android') {
            BackHandler.addEventListener('hardwareBackPress', this._onBackButtonPressed)
        }
        AppState.addEventListener('change', this._onAppStateChanged)
    }
    componentWillUnmount() {
        if (Platform.OS == 'android') {
            BackHandler.removeEventListener('hardwareBackPress', this._onBackButtonPressed)
        }
        this.onNotificationListener()
        this.onNotificationOpenedListener()
        this.didFocusListener.remove()
        AppState.removeEventListener('change', this._onAppStateChanged)
    }
    _onAppStateChanged(newState) {
        if (newState == 'active') {
            firebase.notifications().removeAllDeliveredNotifications()
        }
    }
    _manageNotification(action) {

        this.props.refreshTavel(true)

        if (action == "1" && (this.props.currentPage == 'map' || this.props.currentPage == 'notifications')) {

            this._goTravels()
        }
    }
    _goTravels() {
        this.props.navigation.navigate("Travels")
    }
    async _getLocation() {
        const hasPermission = await this._hasLocationPermission()
        if (!hasPermission) {
            return
        }
        const watchOptions = Platform.OS === 'ios' ? { enableHighAccuracy: true, distanceFilter: 10 } : { enableHighAccuracy: true, distanceFilter: 1, interval: 10, fastestInterval: 10 }
        Geolocation.getCurrentPosition((position) => {
            const camera = { center: { latitude: position.coords.latitude, longitude: position.coords.longitude }, zoom: 14 }
            this.map.animateCamera(camera)
        }, (error) => {
            showAlert({ msg: 'Hubo un error al obtener su ubicación', type: 2 })
        }, watchOptions)
    }
    async _hasLocationPermission() {
        if (Platform.OS == 'ios' || (Platform.OS == 'android' && Platform.Version < 23)) {
            return true
        }
        const hasPermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        if (hasPermission) {
            return true
        }
        const status = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if (status == PermissionsAndroid.RESULTS.GRANTED) {
            return true
        }
        showAlert({ msg: 'No se tiene permiso para obtener su ubicación.', type: 2 })
        return false;
    }
    _onBackButtonPressed = () => {
        this._getOut()
        return true
    }
    _getOut() {
        Alert.alert("Alerta", "¿Está seguro de cerrar sesión?", [{
            text: 'Si', onPress: () => {
                if (Platform.OS == 'android') {
                    Location.stopService()
                }
                this.props.logOut({ token: this.props.user.token })
                this.props.navigation.navigate('LogIn')
            }
        }, { text: 'No' }])
    }
    render() {
        if (this.props.user == null) {
            return null
        }
        return (
            <View style={{ flex: 1, backgroundColor: spaceBlack }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, height: 44, alignItems: 'center', justifyContent: 'center' }}>
                        <Image
                            source={{ uri: (this.props.user) ? this.props.user.PictureUrlLog : null }}
                            style={{ width: 150, height: 150, resizeMode: 'contain' }} />
                        <View style={{ position: 'absolute', right: 20, height: 44, justifyContent: 'center' }}>
                            <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => { this._getOut() }}>
                                <Image
                                    source={ic_get_out}
                                    style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                <Text style={{ color: 'white', fontSize: 12 }}>Salir</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <MapView
                        ref={m => { this.map = m }}
                        style={{ flex: this.state.flex }}
                        provider={PROVIDER_GOOGLE}
                        showsUserLocation={true}
                        showsMyLocationButton={true}
                        followsUserLocation={true} />
                </SafeAreaView>
            </View>
        )
    }
}

const mapStateProps = ({ logIn }) => {
    const { currentPage, user } = logIn
    return { currentPage, user }
}
export default connect(mapStateProps, { setCurrentPage, logOut, showBadge, getMyServices, startService, activityList, setCurrentTravel, refreshTavel })(MapScreen);
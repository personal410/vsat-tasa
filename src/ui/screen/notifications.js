import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image, SafeAreaView, Text, TouchableOpacity, View, StatusBar, FlatList, Alert } from 'react-native';
import moment from 'moment'

import Loader from '../component/loader'
import { spaceBlack, lightBlue, red, green, spaceBlackTrans } from '../../resources/color'
import { ic_get_out, ic_triangle_warning, ic_truck_front, ic_box, ic_green_check, ic_user_white, ic_location, ic_exclamation_mark, ic_calendar } from '../../resources/icon'
import { clearStateNotifications, initialDataNotifications, showBadge, goCheckNotification } from '../../app/thunk/notifcications/actions'
import { logOut } from '../../app/thunk/auth/actions'

import { setCurrentPage } from '../../app/thunk/auth/actions'

import { setCurrentSackLot } from '../../app/thunk/certifier/actions'
import { DRIVER_ROLE_ID, GUARD_ROLE_ID, STORAGE_ROLE_ID, QUALITY_ROLE_ID, CERTIFIER_ROLE_ID } from '../../resources/consts'

import Location from '../../../Location'

class NotificationsScreen extends Component {
    state = {
        showAlert: false,
        itemSelect: null
    }
    _getOut() {
        Alert.alert("Alerta", "¿Está seguro de cerrar sesión?", [{
            text: 'Si', onPress: () => {
                if (Platform.OS == 'android') {
                    Location.stopService()
                }
                this.props.logOut({ token: this.props.user.token })
                this.props.navigation.navigate('LogIn')
            }
        }, { text: 'No' }])
    }
    componentDidMount() {
        this.didFocusListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('light-content')
            this.props.clearStateNotifications({
                callback: () => {
                    this.props.initialDataNotifications({ token: this.props.token, RoleId: this.props.user.RoleId })
                }
            })
            this.props.setCurrentPage('notifications')
            this.props.showBadge(false)
        })
    }
    componentWillUnmount() {
        this.didFocusListener.remove()
    }

    pressQualityNotification = (item) => {
        this.props.navigation.navigate('Temperature', {
            id: item.LoteId,
            lote: item.Lote
        })
    }

    _onLotSelected(item) {
        const new_item = {Id:item.LoteId,Lote:item.Lote}
        this.props.setCurrentSackLot({
            currentLot: new_item, callback: () => {
                this.props.navigation.navigate('Sack')
            }
        })
    }

    _onConfirmNotification() {

        this.props.goCheckNotification({
            token: this.props.user.token, LoteId: this.state.itemSelect.LoteId, callback: () => {
                this.props.clearStateNotifications({
                    callback: () => {
                        this.props.initialDataNotifications({ token: this.props.token, RoleId: this.props.user.RoleId })
                        this.setState({ showAlert: false })

                    }
                })
            }
        })
    }
    render() {
        let notifications = this.props.notifications
        if (notifications == null || notifications.length == 0) {
            notifications = [{ Id: -1 }]
        }
        return (
            <View style={{ backgroundColor: spaceBlack, flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, height: 44, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                        <Image
                            source={{ uri: (this.props.user) ? this.props.user.PictureUrlLog : null }}
                            style={{ width: 150, height: 150, resizeMode: 'contain' }} />
                        <View style={{ position: 'absolute', right: 20, height: 44, justifyContent: 'center' }}>
                            <TouchableOpacity style={{ alignItems: 'center' }} onPress={this._getOut.bind(this)}>
                                <Image
                                    source={ic_get_out}
                                    style={{ width: 20, height: 20, resizeMode: 'contain' }} />

                                <Text style={{ color: 'white', fontSize: 12 }}>Salir</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ paddingStart: 30, paddingTop: 10, backgroundColor: 'white' }}>
                        <Text style={{ fontWeight: 'bold', color: '#C4C4C4' }}>Notificaciones</Text>
                    </View>
                    {
                        (this.props.user) ?
                            (this.props.user.RoleId != STORAGE_ROLE_ID) ?

                                (this.props.user.RoleId == QUALITY_ROLE_ID) ?

                                    <FlatList
                                        style={{ flex: 1, backgroundColor: 'white' }}
                                        data={notifications}
                                        keyExtractor={(item) => { return item.Id }}
                                        renderItem={({ item }) => {
                                            if (item.Id == -1) {
                                                return (
                                                    <Text style={{ fontWeight: 'bold', marginTop: 10, paddingStart: 30, fontSize: 20 }}>Sin notificaciones pendientes</Text>
                                                )
                                            }
                                            return (
                                                <TouchableOpacity style={{ padding: 5, height: 120 }}
                                                    onPress={() => this.pressQualityNotification(item)}
                                                >
                                                    <View style={{ flex: 1, flexDirection: "row", borderWidth: 1, borderColor: 'lightgray', borderRadius: 5 }}>
                                                        <View style={{ backgroundColor: (item.Type == 2) ? red : lightBlue, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, paddingStart: 10, paddingEnd: 10, justifyContent: 'center', width: 70 }}>
                                                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                                                <Text style={{ fontSize: 50, color: 'white', fontWeight: 'bold' }}>{item.FirstLetterAction}</Text>
                                                            </View>
                                                        </View>
                                                        <View style={{ flex: 1, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5 }}>
                                                            <View style={{ flexDirection: 'row', flex: 9, }}>
                                                                <View style={{ width: '70%', justifyContent: 'space-around', padding: 10 }}>
                                                                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'black' }}>{item.ActionName}</Text>
                                                                    <View style={{ flexDirection: 'row' }}>
                                                                        <Image source={ic_box}
                                                                            style={{ width: 15, height: 15, resizeMode: 'contain' }} />
                                                                        <Text style={{ fontSize: 12, color: 'black', marginLeft: 10 }}>Lote:</Text><Text style={{ fontSize: 14, color: 'black', marginLeft: 10, fontWeight: 'bold' }}>{item.Lote}</Text>
                                                                    </View>

                                                                    <View style={{ flexDirection: 'row' }}>
                                                                        <Image source={ic_calendar}
                                                                            style={{ width: 15, height: 15, resizeMode: 'contain',tintColor:lightBlue }} />
                                                                        <Text style={{ fontSize: 12, color: 'black', marginLeft: 10 }}>Fecha:</Text><Text style={{ fontSize: 14, fontWeight: 'bold', color: 'black', marginLeft: 10 }} >{moment(item.DateCheckPrev).format('DD/MM hh:mm A')}</Text>
                                                                    </View>
                                                                    


                                                                    <View style={{ flexDirection: 'row' }}>

                                                                        <Text style={{ fontSize: 12, color: 'black', marginRight: 20, marginLeft: 10 }}>{item.ZoneName}</Text>
                                                                        <Text style={{ fontSize: 12, color: 'black', marginLeft: 10 }}>{item.SectorName}</Text>

                                                                    </View>

                                                                </View>
                                                                <View style={{ alignItems: 'center', width: '30%', justifyContent: 'space-around', borderLeftWidth: 1, borderLeftColor: '#CCC', backgroundColor: green }}>
                                                                    <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'white' }} >Procesar</Text>

                                                                </View>
                                                            </View>

                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                            )
                                        }
                                        }
                                    />

                                    : (this.props.user.RoleId == CERTIFIER_ROLE_ID) ?
                                        <FlatList
                                            style={{ flex: 1, backgroundColor: 'white' }}
                                            data={notifications}
                                            keyExtractor={(item) => { return item.Id }}
                                            renderItem={({ item }) => {
                                                if (item.Id == -1) {
                                                    return (
                                                        <Text style={{ fontWeight: 'bold', marginTop: 10, paddingStart: 30, fontSize: 20 }}>Sin notificaciones pendientes</Text>
                                                    )
                                                }

                                                return (
                                                    <TouchableOpacity style={{ padding: 5, height: 120 }}
                                                        onPress={() => this._onLotSelected(item)}
                                                    >
                                                        <View style={{ flex: 1, flexDirection: "row", borderWidth: 1, borderColor: 'lightgray', borderRadius: 5 }}>
                                                            <View style={{ backgroundColor: (item.Type == 2) ? red : lightBlue, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, paddingStart: 10, paddingEnd: 10, justifyContent: 'center', width: 70 }}>
                                                                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                                                    <Text style={{ fontSize: 50, color: 'white', fontWeight: 'bold' }}>{item.FirstLetterAction}</Text>
                                                                </View>
                                                            </View>
                                                            <View style={{ flex: 1, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5 }}>
                                                                <View style={{ flexDirection: 'row', flex: 9, }}>
                                                                    <View style={{ width: '70%', justifyContent: 'space-around', padding: 10 }}>
                                                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'black' }}>{item.ActionName}</Text>
                                                                        <View style={{ flexDirection: 'row' }}>
                                                                            <Image source={ic_box}
                                                                                style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                                                            <Text style={{ fontSize: 14, color: 'black', marginLeft: 10 }}>Lote:</Text><Text style={{ fontSize: 16, color: 'black', marginLeft: 10, fontWeight: 'bold' }}>{item.Lote}</Text>
                                                                        </View>

                                                                        <View style={{ flexDirection: 'row' }}>

                                                                            <Text style={{ fontSize: 12, color: 'black', marginRight: 20, marginLeft: 10 }}>{item.ZoneName}</Text>
                                                                            <Text style={{ fontSize: 12, color: 'black', marginLeft: 10 }}>{item.SectorName}</Text>

                                                                        </View>

                                                                    </View>
                                                                    <View style={{ alignItems: 'center', width: '30%', justifyContent: 'space-around', borderLeftWidth: 1, borderLeftColor: '#CCC', backgroundColor: green }}>
                                                                        <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'white' }} >Procesar</Text>
                                                                    </View>
                                                                </View>

                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                )
                                            }
                                            }
                                        />
                                        :
                                        <FlatList
                                            style={{ flex: 1, backgroundColor: 'white' }}
                                            data={notifications}
                                            keyExtractor={(item) => { return item.Id }}
                                            renderItem={({ item }) => {
                                                if (item.Id == -1) {
                                                    return (
                                                        <Text style={{ fontWeight: 'bold', marginTop: 10, paddingStart: 30, fontSize: 20,fontFamily:"ProductSans-Regular" }}>Sin notificaciones pendientes</Text>
                                                    )
                                                }
                                                return (
                                                    <View style={{ padding: 5, height: 100, flexDirection: "row", }}>
                                                        <View style={{ flex: 1, flexDirection: "row", borderWidth: 1, borderColor: 'lightgray', borderRadius: 5 }}>
                                                            <View style={{ backgroundColor: (item.Type == 2) ? red : lightBlue, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, paddingStart: 10, paddingEnd: 10, justifyContent: 'center', width: 80 }}>
                                                                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                                                    <Image
                                                                        source={(item.Type == 2) ? ic_triangle_warning : ic_truck_front}
                                                                        style={{ width: 25, height: 25, resizeMode: 'contain' }} />
                                                                </View>
                                                            </View>

                                                            <View style={{ padding: 10, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5, justifyContent: 'space-around', flex: 1 }}>
                                                                <View style={{ flexDirection: 'row' }}>
                                                                    <Text style={{ marginStart: 30, fontWeight: 'bold' ,fontFamily:"ProductSans-Regular"}}>{item.Description}</Text>
                                                                </View>
                                                                <View style={{ flexDirection: 'row' }}>
                                                                    <Text style={{ marginStart: 30,fontFamily:"ProductSans-Regular" }}>{moment(item.Date).format('DD/MM hh:mm A')}</Text>
                                                                </View>
                                                            </View>
                                                        </View>
                                                    </View>
                                                )
                                            }
                                            }
                                        />
                                :
                                <FlatList
                                    style={{ flex: 1, backgroundColor: 'white' }}
                                    data={notifications}
                                    keyExtractor={(item) => { return item.Id }}
                                    renderItem={({ item }) => {
                                        if (item.Id == -1) {
                                            return (
                                                <Text style={{ fontWeight: 'bold', marginTop: 10, paddingStart: 30, fontSize: 20,fontFamily:"ProductSans-Regular" }}>Sin notificaciones pendientes</Text>
                                            )
                                        }
                                        return (
                                            <TouchableOpacity style={{ padding: 5, height: 120 }}
                                                onPress={() => this.setState({ itemSelect: item, showAlert: true })}
                                            >
                                                <View style={{ flex: 1, flexDirection: "row", borderWidth: 1, borderColor: 'lightgray', borderRadius: 5 }}>
                                                    <View style={{ backgroundColor: (item.Type == 2) ? red : lightBlue, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, paddingStart: 10, paddingEnd: 10, justifyContent: 'center', width: 70 }}>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                                            <Text style={{ fontSize: 50, color: 'white', fontWeight: 'bold',fontFamily:"ProductSans-Regular" }}>{item.FirstLetterAction}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ padding: 10, flex: 1, backgroundColor: 'white', borderTopRightRadius: 5, borderBottomRightRadius: 5 }}>
                                                        <View style={{ flexDirection: 'row', flex: 9 }}>
                                                            <View style={{ width: '65%', justifyContent: 'space-around' }}>
                                                                <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'black' ,fontFamily:"ProductSans-Regular"}}>{item.ActionName}</Text>
                                                                <View style={{ flexDirection: 'row' }}>
                                                                    <Image source={ic_box}
                                                                        style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                                                    <Text style={{ fontSize: 14, color: 'black', marginLeft: 10 ,fontFamily:"ProductSans-Regular"}}>{item.Lote}</Text>
                                                                </View>

                                                            </View>
                                                            <View style={{ alignItems: 'center', width: '35%', justifyContent: 'space-around', borderLeftWidth: 1, borderLeftColor: '#CCC' }}>
                                                                <Image source={ic_green_check}
                                                                    style={[{ width: 20, height: 20, resizeMode: 'contain' }, (item.StatusId == 3) ? null : { tintColor: '#CCC' }]}
                                                                />
                                                                <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'black',fontFamily:"ProductSans-Regular" }} >Pendiente</Text>
                                                            </View>
                                                        </View>
                                                        <View style={{ flexDirection: 'row' }}>

                                                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'black',fontFamily:"ProductSans-Regular" }}>Zona:</Text>
                                                            <Text style={{ fontSize: 12, color: 'black', marginRight: 20, marginLeft: 10,fontFamily:"ProductSans-Regular" }}>{item.ZoneName}</Text>
                                                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'black' ,fontFamily:"ProductSans-Regular"}}>Sector:</Text>
                                                            <Text style={{ fontSize: 12, color: 'black', marginLeft: 10,fontFamily:"ProductSans-Regular" }}>{item.SectorName}</Text>

                                                        </View>


                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    }}
                                />
                            : null
                    }

                    {!this.state.showAlert ? null :
                        <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' }}>
                            <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, padding: 20, flexDirection: "row" }}>
                                <Image
                                    source={ic_user_white}
                                    style={{ tintColor: lightBlue, width: 20, height: 20, marginEnd: 20, resizeMode: 'contain' }} />
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20,fontFamily:"ProductSans-Regular" }}>¡Hola {this.props.user.FirstName}!</Text>
                                    <Text style={{ color: 'white', marginTop: 10 ,fontFamily:"ProductSans-Regular"}}>¿Desea confirmar que realizó la tarea?</Text>

                                </View>

                            </View>
                            <View style={{ alignSelf: 'stretch', padding: 20, backgroundColor: 'white' }}>
                                <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
                                    <Image source={ic_exclamation_mark} style={{ width: 20, height: 20, marginEnd: 20 }} />
                                    <Text style={{fontFamily:"ProductSans-Regular"}}>Confirmar tarea</Text>
                                </View>
                                <View style={{ alignSelf: 'stretch', height: 30, marginTop: 20, flexDirection: 'row', justifyContent: 'center' }}>
                                    <TouchableOpacity
                                        onPress={() => { this._onConfirmNotification() }}
                                        style={{ backgroundColor: lightBlue, width: '45%', borderRadius: 15, alignItems: 'center', justifyContent: 'center', marginRight: '5%' }}>
                                        <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Confirmar</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => { this.setState({ showAlert: false, itemSelect: null }) }}
                                        style={{ backgroundColor: red, width: '45%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Cancelar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    }
                    <Loader isVisible={this.props.loading} />
                </SafeAreaView>
            </View>
        )
    }
}
const mapStateProps = (state) => {
    if (state.logIn.user == null) {
        return {}
    }
    const { loading, notifications } = state.notifications
    const { user } = state.logIn
    const { token } = user
    const { currentTravel } = state.travel
    return { loading, notifications, token, currentTravel, user }
}
export default connect(mapStateProps, { setCurrentPage, clearStateNotifications, initialDataNotifications, showBadge, logOut, goCheckNotification, setCurrentSackLot })(NotificationsScreen);
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image, Keyboard, SafeAreaView, Text, TextInput, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';

import { spaceBlackTrans, spaceBlack, lightBlue } from '../../resources/color'
import { logIn, sendToken, recoveryPassword } from '../../app/thunk/auth/actions'
import Loader from '../component/loader'
import { ic_user, ic_password, ic_arrow_left, ic_calendar, ic_clock, ic_location, ic_vertical_line, ic_buildings, ic_box, ic_user_white, ic_exclamation_mark, ic_envelope } from '../../resources/icon'



class RecoveryScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mail: '',
            loading: false,
            title: 'Recuperar Contraseña'
        }
        this._onBack.bind(this)
    }

    _onBack() {
        this.props.navigation.goBack()
    }
    componentDidMount() {

    }


    _recovery() {
        Keyboard.dismiss()

        this.props.recoveryPassword({
            Login: this.state.mail, callback: ()=> {
                this.props.navigation.goBack()
            }
        })

    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                    <View style={{ flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 44, alignItems: 'center', flexDirection: 'row' }}>
                                <TouchableOpacity
                                    onPress={() => { this._onBack() }}>
                                    <Image source={ic_arrow_left} style={{ width: 40, height: 20, marginStart: 20, resizeMode: 'contain' }} />
                                </TouchableOpacity>
                                <Text style={{ color: spaceBlack, fontSize: 18, fontWeight: 'bold', marginStart: 20 }}>{this.state.title}</Text>
                            </View>
                        </View>
                        <View style={{ flex: 0, backgroundColor: 'white', paddingStart: 30, paddingEnd: 30, paddingTop: 0, paddingBottom: 10 }}>
                            <View style={{ flexDirection: 'row', marginTop: 0 }}>
                                <Image
                                    style={{ width: 30, height: 30, tintColor: '#CCC' }}
                                    resizeMode={'contain'}
                                    source={ic_envelope} />
                                <TextInput
                                    style={{ marginStart: 10, flex: 1, padding: 0, color: spaceBlack, fontSize: 15 }}
                                    placeholder="Ingresar correo"
                                    value={this.state.mail}
                                    onChangeText={(txt) => this.setState({ mail: txt })} />
                            </View>
                            <View style={{ height: 1, marginTop: 5, alignSelf: 'stretch', backgroundColor: spaceBlackTrans }} />

                            <TouchableOpacity disabled={this.state.mail.length == 0} style={{ height: 50, marginTop: 30, marginStart: 10, marginEnd: 10, borderRadius: 5, alignSelf: 'stretch', backgroundColor: (this.state.mail.length == 0 ? spaceBlackTrans : lightBlue), justifyContent: 'center', alignItems: 'center' }} onPress={this._recovery.bind(this)}>
                                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}>Enviar</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1 }}>

                        </View>
                    </View>
                </TouchableWithoutFeedback>
                <Loader isVisible={this.state.loading} />
            </SafeAreaView>
        );
    }
}

const mapStateProps = ({ logIn }) => {
    if (!logIn.user) {
        return {}
    }
    const { token } = logIn.user
    return { token }
}

export default connect(mapStateProps, { logIn, sendToken, recoveryPassword })(RecoveryScreen);
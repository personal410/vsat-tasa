import React, {Component} from 'react';
import { View ,Image} from 'react-native';
import { connect } from 'react-redux'

import { spaceBlack } from '../../resources/color'
import { DRIVER_ROLE_ID, GUARD_ROLE_ID, STORAGE_ROLE_ID, QUALITY_ROLE_ID, CERTIFIER_ROLE_ID,TRANSPORT_ROLE_ID } from '../../resources/consts'
import { splash } from '../../resources/icon';

class SplashScreen extends Component {
    componentDidMount(){
        setTimeout(() => {
            if(this.props.user == null){
                this.props.navigation.navigate('LogIn')
            }else{
                const { RoleId } = this.props.user
                if(RoleId == DRIVER_ROLE_ID || RoleId == GUARD_ROLE_ID){
                    this.props.navigation.navigate('Driver')
                }else if(RoleId == STORAGE_ROLE_ID){
                    this.props.navigation.navigate('Storage')
                }else if(RoleId == QUALITY_ROLE_ID){
                    this.props.navigation.navigate('Quality')
                }else if(RoleId == CERTIFIER_ROLE_ID){
                    this.props.navigation.navigate('Certifier')
                }else if (RoleId ==TRANSPORT_ROLE_ID){
                    this.props.navigation.navigate('Transport')
                }else{
                    this.props.navigation.navigate('LogIn')
                }
            }
        }, 1000)
    }
    render(){
        return (
            <View style={{ flex: 1, backgroundColor: spaceBlack, alignItems:'center',justifyContent:'center' }} >
                <Image 
                source={splash}
                style={{width:50,height:50,resizeMode:'contain',tintColor:'#53B8DE'}}
                />
            </View>
        )
    }
}

const mapStateProps = ({logIn}) => {
    const { user, currentPage } = logIn
    return { user, currentPage }
}

export default connect(mapStateProps, {})(SplashScreen);
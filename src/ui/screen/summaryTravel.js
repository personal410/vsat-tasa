import React, { Component } from 'react'
import { connect } from 'react-redux';
import { FlatList, Image, SafeAreaView, Text, TouchableOpacity, View } from 'react-native'
import moment from 'moment'

import { Capitalize } from '../../app/util'
import { spaceBlack, green, lightBlue } from '../../resources/color'
import { ic_arrow_left, ic_calendar, ic_clock, ic_location, ic_vertical_line, ic_paper_plane, ic_scale, ic_race_flag, ic_shield, ic_unboxing, ic_green_check } from '../../resources/icon'

class SummaryTravelScreen extends Component {
    constructor(props) {
        super(props)
        let count = 0
        for (let i = 0; i < this.props.currentTasks.length; i++) {
            if (this.props.currentTasks[i].StatusId == 2) {
                count += 1
            }
        }
        this.state = {
            showRegistrationSuccessfull: false,
            currentTask: count,
            showCurrentTaskAlert: false,
            showNoCurrentTaskAlert: false,
            loading: false,
            numberTasks: this.props.currentTasks.length,
            requiredTask: '',
            tasks: [{ id: 1, icon: 'ic_paper_plane', title: 'Inicio de Viaje', time: '9:30 AM' }, { id: 2, icon: 'ic_race_flag', title: 'Llegada al Puerto Callao', time: '10:02 AM' }, { id: 3, icon: 'ic_shield', title: 'Chequeo de Seguridad', time: '10:30 AM' }, { id: 4, icon: 'ic_scale', title: 'Hacer Pesaje Inicial', time: '11:00 AM' }, { id: 5, icon: 'ic_unboxing', title: 'Iniciar Carga', time: '11:30 AM' }]

        }

        this._onBack.bind(this)

    }
    _onBack() {
        this.props.navigation.goBack()
    }
    render() {
        const { currentTask, showRegistrationSuccessfull, showCurrentTaskAlert } = this.state
        let img = ic_location
        let text = ""
        if (showCurrentTaskAlert) {
            text = this.props.currentTasks[currentTask].ActivityName
        }

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ alignSelf: 'stretch', backgroundColor: 'white', height: 44, alignItems: 'center', flexDirection: 'row' }}>
                    <TouchableOpacity
                        onPress={() => { this._onBack() }}>
                        <Image source={ic_arrow_left} style={{ width: 40, height: 20, marginStart: 20, resizeMode: 'contain' }} />
                    </TouchableOpacity>
                    <Text style={{ color: spaceBlack, fontSize: 18, fontWeight: 'bold', marginStart: 20 }}>Resumen de Viaje</Text>
                </View>
                <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: 'lightgray', marginTop: 10 }} />
                <View style={{ flex: 1, margin: 10, borderColor: 'lightgray', borderRadius: 5, borderWidth: 1 }}>
                    <View>
                        <View style={{ flexDirection: 'row', alignSelf: 'stretch', padding: 20, height: 100, backgroundColor: spaceBlack, borderTopLeftRadius: 5, borderTopRightRadius: 5 }}>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={ic_calendar} style={{ width: 15, height: 15, resizeMode: 'contain' }} />
                                    <Text style={{ color: 'white', marginStart: 20, fontSize:16 }}><Text style={{ fontWeight: 'bold' }}>{Capitalize(this.props.currentTravel.Day)}</Text> {this.props.currentTravel.ShortDate.split('/')[0] + " " + Capitalize(this.props.currentTravel.month)}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <Image source={ic_clock} style={{ width: 15, height: 15, resizeMode: 'contain' }} />
                                    <Text style={{ color: 'white', marginStart: 20,fontSize:16 }}>{this.props.currentTravel.Time}</Text>
                                </View>
                            </View>
                            <View>
                                <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={ic_location} />
                                <Image style={{ width: 15, flex: 1, resizeMode: 'contain', tintColor: 'white' }} source={ic_vertical_line} />
                                <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={ic_location} />
                            </View>
                        </View>
                    </View>

                    <FlatList
                        style={{ flex: 1 }}
                        data={this.props.currentTasks}
                        keyExtractor={(item) => { return item.Id.toString() }}
                        renderItem={({ item, index }) => {
                            const { ActivityName, DateRegister } = item
                            const completed = index < currentTask
                            const { Icon } = item
                            let img = ic_location
                            switch (Icon) {
                                case '1':
                                    img = ic_paper_plane;
                                    break
                                case '2':
                                    img = ic_race_flag;
                                    break
                                case '3':
                                    img = ic_shield;
                                    break
                                case '4':
                                    img = ic_scale;
                                    break
                                case '5':
                                    img = ic_unboxing;
                                    break
                            }
                            const day_en = moment(DateRegister).format('d')
                            let day = ''
                            switch (day_en) {
                                case '1':
                                    day = 'Lunes';
                                    break
                                case '2':
                                    day = 'Martes';
                                    break
                                case '3':
                                    day = 'Miercoles';
                                    break
                                case '4':
                                    day = 'Jueves';
                                    break
                                case '5':
                                    day = 'Viernes';
                                    break
                                case '6':
                                    day = 'Sabado';
                                    break
                                case '7':
                                    day = 'Domingo';
                                    break
                            }
                            const showTopLine = index > 0
                            const showBottomLine = index < this.state.numberTasks - 1
                            const topLineColor = completed ? green : 'lightgray'
                            const bottomLineColor = index < currentTask - 1 ? green : 'lightgray'
                            const timeBackgroundColor = DateRegister == null ? 'white' : lightBlue
                            return (
                                <TouchableOpacity
                                    onPress={index >= currentTask ? () => null : null}
                                    disabled={index < currentTask}>
                                    <View style={{ alignSelf: 'stretch', height: 80, flexDirection: "row", paddingStart: 20, paddingEnd: 20, alignItems: 'center' }}>
                                        <Image source={img} style={{ width: 30, height: 30, resizeMode: 'contain', tintColor: (!completed) ? '#CCC' : lightBlue }} />
                                        <View style={{ marginStart: 10, flex: 1 }}>
                                            <Text style={{ fontWeight: 'bold' ,fontSize:16}}>{ActivityName}</Text>
                                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                <View style={{ backgroundColor: timeBackgroundColor, width: 80, height: 20, borderRadius: 10, alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={{ color: 'white' }}>{DateRegister == null ? "" : moment(DateRegister).format('h:mm a')}</Text>
                                                </View>
                                                <Text style={{ color: spaceBlack, marginStart: 10, textAlignVertical: 'center' }}>{DateRegister == null ? "" : day} {DateRegister == null ? "" : moment(DateRegister).format('D/M')}</Text>
                                            </View>
                                        </View>
                                        <View>
                                            <View style={{ width: 24, flex: 1 }}>
                                                {showTopLine ? <View style={{ backgroundColor: topLineColor, width: 1, flex: 1, alignSelf: 'center' }} /> : null}
                                            </View>
                                            <View>
                                                {completed ?
                                                    <Image source={ic_green_check} style={{ width: 24, height: 24, resizeMode: 'contain' }} />
                                                    :
                                                    <View style={{ width: 24, height: 24, backgroundColor: 'lightgray', borderRadius: 24 }} />
                                                }
                                            </View>
                                            <View style={{ width: 24, flex: 1 }}>
                                                {showBottomLine ? <View style={{ backgroundColor: bottomLineColor, width: 1, flex: 1, alignSelf: 'center' }} /> : null}
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            )
                        }} />
                    <View style={{ alignSelf: 'stretch', height: 50, backgroundColor: green, justifyContent: 'center', alignItems: 'center', borderBottomStartRadius: 5, borderBottomEndRadius: 5 }}>
                        <Text style={{ color: 'white' ,fontSize:16}}>Registros completados {this.props.currentTasks.filter(e => e.DateRegister != null).length}/{this.props.currentTasks.length}</Text>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const mapStateProps = ({ logIn, travel }) => {
    const { token } = logIn.user
    const { watcherId, currentPosition, currentTravel, currentTasks } = travel
    return { watcherId, currentTravel, currentTasks, currentPosition, travelId: currentTravel.Id.toString(), token }
}
export default connect(mapStateProps, {})(SummaryTravelScreen);
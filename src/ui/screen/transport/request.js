import React, { PureComponent } from 'react'

import { SafeAreaView, StatusBar, Text, View, Image, TextInput, FlatList, TouchableOpacity , Alert } from 'react-native'
import { connect } from 'react-redux'
import firebase from 'react-native-firebase'

import { spaceBlack, lightBlue, backgroundLeftColor, spaceBlackTrans, red } from '../../../resources/color'
import { ic_truck_front, ic_box, ic_arrow_left, ic_sync, ic_location, ic_calendar, ic_user_white, ic_exclamation_mark, ic_get_out, ic_clock } from '../../../resources/icon'
import SelectInput from 'react-native-select-input-ios'
import Loader from '../../component/loader'
import { getInitialData, filterProvider, setInputRequest, setNameBase, saveRequest } from '../../../app/thunk/request/actions'
import DropdownAlert from 'react-native-dropdownalert';
import DatePicker from 'react-native-datepicker'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import {logOut} from '../../../app/thunk/auth/actions'

import moment from 'moment'

class RequestScreen extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            headerColor: 'dark-content',
            idSelectOrigen:0,
            idSelectDestino:0,
            idSelectProduct:0,
            idSelectType:0,
            idSelectProvider:0,
            date: "2016-05-15",
            hour: "20:00",
            unit:'',
            observation:'',
            showAlert:false
        }
    }

    componentDidMount() {
        this.props.getInitialData({ token: this.props.user.token })

        this.onNotificationListener = firebase.notifications().onNotification(async notification => {
            const { title, body, data } = notification
            this.dropDownAlertRequest.alertWithType('info', title, body);
            this.props.navigation.navigate('BaseTravels')
        })

        this.onNotificationOpenedListener = firebase.notifications().onNotificationOpened(notificationOpen => {
            const { action } = notificationOpen.notification.data
            if (action != null && action != undefined) {
                this.props.navigation.navigate('BaseTravels')
            }
        })


    }
    componentWillUnmount() {
        //this.didFocusListener.remove()
    }
    _getOut() {
        Alert.alert("Alerta", "¿Está seguro de cerrar sesión?", [{
            text: 'Si', onPress: () => {
                this.props.logOut({ token: this.props.user.token })
                this.props.navigation.navigate('LogIn')
            }
        }, { text: 'No' }])
    }


    cancelRequest = () =>{

                
        this.props.setInputRequest({prop:'idSelectOrigen',value:0})
        this.props.setInputRequest({prop:'idSelectDestino',value:0})
        this.props.setInputRequest({prop:'idSelectProduct',value:0})
        this.props.setInputRequest({prop:'idSelectType',value:0})
        this.props.setInputRequest({prop:'idSelectProvider',value:0})
        this.props.setInputRequest({prop:'date',value:moment().format('YYYY-MM-DD')})
        this.props.setInputRequest({prop:'hour',value:moment().format('HH:mm')})
        this.props.setInputRequest({prop:'unit',value:''})
        this.props.setInputRequest({prop:'observation',value:''})
        this.props.setInputRequest({prop:'RegisterId',value:0})
        this.props.setInputRequest({prop:'OrigenName',value:''})
        this.props.setInputRequest({prop:'DestinoName',value:''})
        this.hideAlert();

    }

    saveRequest = () =>{

        this.props.saveRequest({
            idOrigen:this.props.idSelectOrigen,
            nameOrigen:this.props.OrigenName,
            idDestino:this.props.idSelectDestino,
            nameDestino:this.props.DestinoName,
            fecha:this.props.date,
            hora:this.props.hour,
            idProducto:this.props.idSelectProduct,
            idTipo:this.props.idSelectType,
            idProveedor:this.props.idSelectProvider,
            unidades:this.props.unit,
            observacion:this.props.observation,
            RegisterId:this.props.RegisterId,
            token:this.props.user.token,
            callback:()=>{
                this.cancelRequest()
                Alert.alert('¡Registro Exitoso!','Su servicio fue solicitado al proveedor')
            }
        })
 
    }

    showAlert=()=>{
        this.setState({showAlert:true})
    }
    hideAlert=()=>{
        this.setState({showAlert:false})
    }



    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
            <KeyboardAwareScrollView >
                <StatusBar barStyle={this.state.headerColor} />
                <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, height: 44, alignItems: 'center', justifyContent: 'center' }}>
                    <Image
                        source={{ uri: this.props.user.PictureUrlLog }}
                        style={{ width: 170, height: 170, resizeMode: 'contain' }} />

                    <View style={{ position: 'absolute', right: 20, height: 44, justifyContent: 'center' }}>
                        <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => { this._getOut() }}>
                            <Image
                                source={ic_get_out}
                                style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                            <Text style={{ color: 'white', fontSize: 12 }}>Salir</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 1 }}>
                    <View style={{ flexDirection: 'row' }}>
                        {/* COLUMN 1 */}
                        <View style={{ width: '50%', padding: 15 }}>
                            <View style={{ flexDirection: 'row', marginBottom: 5 }}><Image source={ic_location} style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue, marginRight: 8 }} /><Text style={{ fontFamily: "ProductSans-Regular" }}>Origen</Text></View>
                            <SelectInput
                                placeholder={'Número de Placa'}
                                placeholderTextColor={'#CCC'}
                                value={this.props.idSelectOrigen}
                                options={this.props.origen}
                                labelStyle={{}}
                                style={{ backgroundColor: 'white', borderWidth: 1, borderColor: '#CCC', borderRadius: 5, paddingLeft: 12, height: 40, justifyContent: 'center', marginBottom: 10 }}
                                onSubmitEditing={(v) => {
                                    this.props.setInputRequest({prop:'idSelectOrigen',value:v})
                                    this.props.setNameBase({prop:'OrigenName',base:this.props.origen,idBase:v})
                                    //this.setState({idSelectOrigen:v})
                                }}

                            />
                            <View style={{ flexDirection: 'row', marginBottom: 5 }}><Image source={ic_calendar} style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue, marginRight: 8 }} /><Text style={{ fontFamily: "ProductSans-Regular" }}>Fecha</Text></View>
                            <DatePicker
                                style={{ backgroundColor: 'white', borderRadius: 5, paddingLeft: 12, height: 40, justifyContent: 'center', marginBottom: 10 }}
                                date={this.props.date}
                                mode="date"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                onDateChange={(date) => { 
                                    this.props.setInputRequest({prop:'date',value:date})
                                    //this.setState({ date: date }) 
                                    }}
                            />

                            <View style={{ flexDirection: 'row', marginBottom: 5 }}><Text style={{ fontFamily: "ProductSans-Regular" }}>Producto</Text></View>
                            <SelectInput
                                placeholder={'Número de Placa'}
                                placeholderTextColor={'#CCC'}
                                value={this.props.idSelectProduct}
                                options={this.props.producto}
                                labelStyle={{}}
                                style={{ backgroundColor: 'white', borderWidth: 1, borderColor: '#CCC', borderRadius: 5, paddingLeft: 12, height: 40, justifyContent: 'center', marginBottom: 10 }}
                                onSubmitEditing={(v) => {
                                    this.props.setInputRequest({prop:'idSelectProduct',value:v})
                                    this.props.setInputRequest({prop:'idSelectProvider',value:0})
                                    this.setState({idSelectProduct:v,idSelectProvider:0},()=>{
                                        this.props.filterProvider({idProduct:this.props.idSelectProduct, providers:this.props.proveedor})
                                    })
                                }}

                            />

                            <View style={{ flexDirection: 'row', marginBottom: 5 }}><Text style={{ fontFamily: "ProductSans-Regular" }}>Proveedor</Text></View>
                            <SelectInput
                                placeholder={'Número de Placa'}
                                placeholderTextColor={'#CCC'}
                                value={this.props.idSelectProvider}
                                options={this.props.proveedorFilter}
                                labelStyle={{}}
                                style={{ backgroundColor: 'white', borderWidth: 1, borderColor: '#CCC', borderRadius: 5, paddingLeft: 12, height: 40, justifyContent: 'center', marginBottom: 10 }}
                                onSubmitEditing={(v) => {
                                    this.props.setInputRequest({prop:'idSelectProvider',value:v})
                                    //this.setState({idSelectProvider:v})
                                }}

                            />


                        </View>
                        {/* COLUMN 2 */}
                        <View style={{ width: '50%', padding: 15 }}>
                            <View style={{ flexDirection: 'row', marginBottom: 5 }}><Image source={ic_location} style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue, marginRight: 8 }} /><Text style={{ fontFamily: "ProductSans-Regular" }}>Destino</Text></View>
                            <SelectInput
                                placeholder={'Número de Placa'}
                                placeholderTextColor={'#CCC'}
                                value={this.props.idSelectDestino}
                                options={this.props.destino}
                                labelStyle={{}}
                                style={{ backgroundColor: 'white', borderWidth: 1, borderColor: '#CCC', borderRadius: 5, paddingLeft: 12, height: 40, justifyContent: 'center', marginBottom: 10 }}
                                onSubmitEditing={(v) => {
                                    this.props.setInputRequest({prop:'idSelectDestino',value:v})
                                    this.props.setNameBase({prop:'DestinoName',base:this.props.destino,idBase:v})

                                    //this.setState({idSelectDestino:v})
                                }}

                            />
                            <View style={{ flexDirection: 'row', marginBottom: 5 }}><Image source={ic_clock} style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: lightBlue, marginRight: 8 }} /><Text style={{ fontFamily: "ProductSans-Regular" }}>Hora</Text></View>

                            <DatePicker
                                style={{ backgroundColor: 'white', borderRadius: 5, paddingLeft: 12, height: 40, justifyContent: 'center', marginBottom: 10 }}
                                date={this.props.hour}
                                mode="time"
                                format="HH:mm"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                minuteInterval={10}
                                onDateChange={(time) => { 
                                    this.props.setInputRequest({prop:'hour',value:time})
                                    //this.setState({ hour: time }); 
                                    }}
                            />

                            <View style={{ flexDirection: 'row', marginBottom: 5 }}><Text style={{ fontFamily: "ProductSans-Regular" }}>Tipo</Text></View>
                            <SelectInput
                                placeholder={'Número de Placa'}
                                placeholderTextColor={'#CCC'}
                                value={this.props.idSelectType}
                                options={this.props.tipo}
                                labelStyle={{}}
                                style={{ backgroundColor: 'white', borderWidth: 1, borderColor: '#CCC', borderRadius: 5, paddingLeft: 12, height: 40, justifyContent: 'center', marginBottom: 10 }}
                                onSubmitEditing={(v) => {
                                    this.props.setInputRequest({prop:'idSelectType',value:v})
                                    //this.setState({idSelectType:v})
                                }}

                            />

                            <View style={{ flexDirection: 'row', marginBottom: 5 }}><Text style={{ fontFamily: "ProductSans-Regular" }}>Cant. Unidades</Text></View>
                            <TextInput
                                style={{ backgroundColor: 'white', borderWidth: 1, borderColor: '#CCC', borderRadius: 5, paddingLeft: 12, height: 40, justifyContent: 'center', marginBottom: 10 }}
                                keyboardType={'numeric'}
                                maxLength={5}
                                onChangeText={text => {
                                    this.props.setInputRequest({prop:'unit',value:text})
                                    //this.setState({unit:text})
                                }}
                                value={this.props.unit}

                            />

                        </View>

                    </View>

                    <View style={{width:'100%',paddingLeft:15,paddingRight:15}}>
                        <Text style={{ fontFamily: "ProductSans-Regular" }}>Observaciones</Text>
                        <TextInput
                                style={{ backgroundColor: 'white', borderWidth: 1, borderColor: '#CCC', borderRadius: 5, paddingLeft: 12, height: 80, justifyContent: 'center', marginBottom: 10,marginTop:10 }}
                                keyboardType={'numeric'}
                                multiline={true}
                                onChangeText={text => {
                                    this.props.setInputRequest({prop:'observation',value:text})
                                    //this.setState({observation:text})
                                }}
                                value={this.props.observation}

                            />

                            <View style={{ alignSelf: 'stretch', height: 30, marginTop: 20, flexDirection: 'row', justifyContent: 'space-around' }}>
                                <TouchableOpacity
                                    onPress={() => { this.showAlert() }}
                                    style={{ backgroundColor: '#52B6DC', width: '40%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', fontWeight: 'bold' }}>Solicitar</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => { this.cancelRequest() }}
                                    style={{ backgroundColor: '#EB6560', width: '40%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', fontWeight: 'bold' }}>Cancelar</Text>
                                </TouchableOpacity>
                            </View>
                    </View>

                </View>
                </KeyboardAwareScrollView>
                <Loader isVisible={this.props.loading} />
                {!this.state.showAlert ? null :
                    <View style={{ backgroundColor: spaceBlackTrans, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, justifyContent: 'flex-end' }}>
                        <View style={{ alignSelf: 'stretch', backgroundColor: spaceBlack, padding: 20, flexDirection: "row" }}>
                            <Image
                                source={ic_user_white}
                                style={{ tintColor: lightBlue, width: 20, height: 20, marginEnd: 20, resizeMode: 'contain' }} />
                            <View style={{ flex: 1 }}>
                                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20,fontFamily:"ProductSans-Regular" }}>¡Hola {this.props.user.FirstName}!</Text>
                                <Text style={{ color: 'white', marginTop: 10 ,fontFamily:"ProductSans-Regular"}}>¿Está seguro de enviar la solicitud?</Text>
                            </View>

                        </View>
                        <View style={{ alignSelf: 'stretch', padding: 20, backgroundColor: 'white' }}>
                            <View style={{ flexDirection: "row", alignSelf: "stretch" }}>
                                <Image source={ic_exclamation_mark} style={{ width: 20, height: 20, marginEnd: 20 }} />
                                <Text style={{fontFamily:"ProductSans-Regular"}}>Confirmar envío</Text>
                            </View>
                            <View style={{ alignSelf: 'stretch', height: 30, marginTop: 20, flexDirection: 'row', justifyContent: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => { this.saveRequest() }}
                                    style={{ backgroundColor: '#52B6DC', width: '45%', borderRadius: 15, alignItems: 'center', justifyContent: 'center', marginRight: '5%' }}>
                                    <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Confirmar</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => { this.hideAlert() }}
                                    style={{ backgroundColor: '#EB6560', width: '45%', borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white',fontFamily:"ProductSans-Regular" }}>Cancelar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                }
                <DropdownAlert ref={ref => this.dropDownAlertRequest = ref} />
            </SafeAreaView>
        )
    }
}

const mapStateProps = (state) => {
    const { 
        origen, 
        destino, 
        producto, 
        tipo, 
        proveedor, 
        proveedorFilter,
        loading,
        idSelectOrigen,
        idSelectDestino,
        idSelectProduct,
        idSelectType,
        idSelectProvider,
        date,
        hour,
        unit,
        observation,
        RegisterId,
        OrigenName,
        DestinoName
    } = state.request
    const { user } = state.logIn
    console.log('state.request', state.request)

    return { user, origen, destino, producto, tipo, proveedor, proveedorFilter ,loading ,idSelectOrigen,
        idSelectDestino,
        idSelectProduct,
        idSelectType,
        idSelectProvider,
        date,
        hour,
        unit,
        observation,
        RegisterId,
        OrigenName,
        DestinoName
    }
}
export default connect(mapStateProps, { getInitialData , filterProvider, setInputRequest, setNameBase , saveRequest , logOut})(RequestScreen);